<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="collisionHappenedIn" />
        <signal name="diamondCountUpIn" />
        <signal name="diamondLevelUpIn" />
        <signal name="btn0In" />
        <signal name="btn1In" />
        <signal name="btn2In" />
        <signal name="fourSecSigIn" />
        <signal name="levelsMatchIn" />
        <signal name="slugIncSizeOut" />
        <signal name="slugAnimationOnOffOut" />
        <signal name="leftRightSlugOut" />
        <signal name="slugFlashAnimOnOut" />
        <signal name="diamondAnimOnOffOut" />
        <signal name="incScoreOut" />
        <signal name="incLevelOut" />
        <signal name="resetFourSecTimerOut" />
        <signal name="gameWinSigOut" />
        <signal name="slugMagentaOut" />
        <signal name="gameOverOut" />
        <signal name="clkin" />
        <signal name="qLevelUp" />
        <signal name="dAnimation" />
        <signal name="qAnimation" />
        <signal name="dResetTimer" />
        <signal name="qResetTimer" />
        <signal name="dGameOver" />
        <signal name="qGameOver" />
        <signal name="dWin" />
        <signal name="qWin" />
        <signal name="dLevelUp" />
        <signal name="dSwitchSlug" />
        <signal name="qSwitchSlug" />
        <signal name="dCountUp" />
        <signal name="qCountUp" />
        <signal name="dIncSize" />
        <signal name="qIncSize" />
        <signal name="qWin,qGameOver,qResetTimer,qAnimation,qLevelUp,qIncSize,qCountUp,qSwitchSlug,qGamePlayStart,qGameInitial" />
        <signal name="dWin,dGameOver,dResetTimer,dAnimation,dLevelUp,dIncSize,dCountUp,dSwitchSlug,dGamePlayStart,dGameInitial" />
        <signal name="resetScoreOut" />
        <signal name="qGameInitial" />
        <signal name="dGameInitial" />
        <signal name="XLXN_9" />
        <signal name="XLXN_11" />
        <signal name="dGamePlayStart" />
        <signal name="qGamePlayStart" />
        <port polarity="Input" name="collisionHappenedIn" />
        <port polarity="Input" name="diamondCountUpIn" />
        <port polarity="Input" name="diamondLevelUpIn" />
        <port polarity="Input" name="btn0In" />
        <port polarity="Input" name="btn1In" />
        <port polarity="Input" name="btn2In" />
        <port polarity="Input" name="fourSecSigIn" />
        <port polarity="Input" name="levelsMatchIn" />
        <port polarity="Output" name="slugIncSizeOut" />
        <port polarity="Output" name="slugAnimationOnOffOut" />
        <port polarity="Output" name="leftRightSlugOut" />
        <port polarity="Output" name="slugFlashAnimOnOut" />
        <port polarity="Output" name="diamondAnimOnOffOut" />
        <port polarity="Output" name="incScoreOut" />
        <port polarity="Output" name="incLevelOut" />
        <port polarity="Output" name="resetFourSecTimerOut" />
        <port polarity="Output" name="gameWinSigOut" />
        <port polarity="Output" name="slugMagentaOut" />
        <port polarity="Output" name="gameOverOut" />
        <port polarity="Input" name="clkin" />
        <port polarity="Output" name="resetScoreOut" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="controlLogicGameStateMachine">
            <timestamp>2016-5-24T3:51:11</timestamp>
            <line x2="608" y1="32" y2="32" x1="544" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <line x2="0" y1="-656" y2="-656" x1="64" />
            <line x2="0" y1="-576" y2="-576" x1="64" />
            <line x2="0" y1="-496" y2="-496" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-336" y2="-336" x1="64" />
            <line x2="0" y1="-256" y2="-256" x1="64" />
            <line x2="0" y1="-176" y2="-176" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="608" y1="-736" y2="-736" x1="544" />
            <line x2="608" y1="-672" y2="-672" x1="544" />
            <line x2="608" y1="-608" y2="-608" x1="544" />
            <line x2="608" y1="-544" y2="-544" x1="544" />
            <line x2="608" y1="-480" y2="-480" x1="544" />
            <line x2="608" y1="-416" y2="-416" x1="544" />
            <line x2="608" y1="-352" y2="-352" x1="544" />
            <line x2="608" y1="-288" y2="-288" x1="544" />
            <line x2="608" y1="-224" y2="-224" x1="544" />
            <line x2="608" y1="-160" y2="-160" x1="544" />
            <line x2="608" y1="-96" y2="-96" x1="544" />
            <rect width="64" x="544" y="-44" height="24" />
            <line x2="608" y1="-32" y2="-32" x1="544" />
            <rect width="480" x="64" y="-768" height="832" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="fd" name="XLXI_7">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="dLevelUp" name="D" />
            <blockpin signalname="qLevelUp" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_8">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="dAnimation" name="D" />
            <blockpin signalname="qAnimation" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_9">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="dResetTimer" name="D" />
            <blockpin signalname="qResetTimer" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_10">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="dGameOver" name="D" />
            <blockpin signalname="qGameOver" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_11">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="dWin" name="D" />
            <blockpin signalname="qWin" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_2">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="XLXN_9" name="D" />
            <blockpin signalname="XLXN_11" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_4">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="dSwitchSlug" name="D" />
            <blockpin signalname="qSwitchSlug" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_5">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="dCountUp" name="D" />
            <blockpin signalname="qCountUp" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_6">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="dIncSize" name="D" />
            <blockpin signalname="qIncSize" name="Q" />
        </block>
        <block symbolname="controlLogicGameStateMachine" name="XLXI_12">
            <blockpin signalname="btn0In" name="btn0In" />
            <blockpin signalname="btn1In" name="btn1In" />
            <blockpin signalname="btn2In" name="btn2In" />
            <blockpin signalname="fourSecSigIn" name="fourSecSigIn" />
            <blockpin signalname="collisionHappenedIn" name="collisionHappenedIn" />
            <blockpin signalname="diamondCountUpIn" name="diamondCountUpIn" />
            <blockpin signalname="diamondLevelUpIn" name="diamondLevelUpIn" />
            <blockpin signalname="levelsMatchIn" name="levelsMatchIn" />
            <blockpin signalname="qWin,qGameOver,qResetTimer,qAnimation,qLevelUp,qIncSize,qCountUp,qSwitchSlug,qGamePlayStart,qGameInitial" name="PS(9:0)" />
            <blockpin signalname="slugIncSizeOut" name="slugIncSizeOut" />
            <blockpin signalname="slugAnimationOnOffOut" name="slugAnimationOnOffOut" />
            <blockpin signalname="leftRightSlugOut" name="leftRightSlugOut" />
            <blockpin signalname="slugFlashAnimOnOut" name="slugFlashAnimOnOut" />
            <blockpin signalname="diamondAnimOnOffOut" name="diamondAnimOnOffOut" />
            <blockpin signalname="incScoreOut" name="incScoreOut" />
            <blockpin signalname="incLevelOut" name="incLevelOut" />
            <blockpin signalname="resetFourSecTimerOut" name="resetFourSecondTimerOut" />
            <blockpin signalname="gameWinSigOut" name="gameWinSigOut" />
            <blockpin signalname="slugMagentaOut" name="slugMagentaOut" />
            <blockpin signalname="gameOverOut" name="gameOverOut" />
            <blockpin signalname="dWin,dGameOver,dResetTimer,dAnimation,dLevelUp,dIncSize,dCountUp,dSwitchSlug,dGamePlayStart,dGameInitial" name="NS(9:0)" />
            <blockpin signalname="resetScoreOut" name="resetScoreOut" />
        </block>
        <block symbolname="inv" name="XLXI_13">
            <blockpin signalname="dGameInitial" name="I" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_14">
            <blockpin signalname="XLXN_11" name="I" />
            <blockpin signalname="qGameInitial" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_3">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="dGamePlayStart" name="D" />
            <blockpin signalname="qGamePlayStart" name="Q" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="collisionHappenedIn">
            <wire x2="656" y1="384" y2="384" x1="320" />
            <wire x2="656" y1="384" y2="416" x1="656" />
            <wire x2="1200" y1="416" y2="416" x1="656" />
        </branch>
        <branch name="diamondCountUpIn">
            <wire x2="640" y1="464" y2="464" x1="320" />
            <wire x2="640" y1="464" y2="496" x1="640" />
            <wire x2="1200" y1="496" y2="496" x1="640" />
        </branch>
        <branch name="diamondLevelUpIn">
            <wire x2="640" y1="544" y2="544" x1="304" />
            <wire x2="640" y1="544" y2="576" x1="640" />
            <wire x2="1200" y1="576" y2="576" x1="640" />
        </branch>
        <iomarker fontsize="28" x="320" y="384" name="collisionHappenedIn" orien="R180" />
        <iomarker fontsize="28" x="320" y="464" name="diamondCountUpIn" orien="R180" />
        <iomarker fontsize="28" x="304" y="544" name="diamondLevelUpIn" orien="R180" />
        <branch name="btn0In">
            <wire x2="624" y1="80" y2="80" x1="160" />
            <wire x2="624" y1="80" y2="96" x1="624" />
            <wire x2="1200" y1="96" y2="96" x1="624" />
        </branch>
        <branch name="btn1In">
            <wire x2="624" y1="144" y2="144" x1="160" />
            <wire x2="624" y1="144" y2="176" x1="624" />
            <wire x2="1200" y1="176" y2="176" x1="624" />
        </branch>
        <branch name="btn2In">
            <wire x2="640" y1="224" y2="224" x1="176" />
            <wire x2="640" y1="224" y2="256" x1="640" />
            <wire x2="1200" y1="256" y2="256" x1="640" />
        </branch>
        <iomarker fontsize="28" x="160" y="80" name="btn0In" orien="R180" />
        <iomarker fontsize="28" x="160" y="144" name="btn1In" orien="R180" />
        <iomarker fontsize="28" x="176" y="224" name="btn2In" orien="R180" />
        <branch name="fourSecSigIn">
            <wire x2="656" y1="304" y2="304" x1="240" />
            <wire x2="656" y1="304" y2="336" x1="656" />
            <wire x2="1200" y1="336" y2="336" x1="656" />
        </branch>
        <iomarker fontsize="28" x="240" y="304" name="fourSecSigIn" orien="R180" />
        <branch name="levelsMatchIn">
            <wire x2="624" y1="624" y2="624" x1="256" />
            <wire x2="624" y1="624" y2="656" x1="624" />
            <wire x2="1200" y1="656" y2="656" x1="624" />
        </branch>
        <iomarker fontsize="28" x="256" y="624" name="levelsMatchIn" orien="R180" />
        <branch name="slugIncSizeOut">
            <wire x2="2816" y1="96" y2="96" x1="1808" />
            <wire x2="3184" y1="48" y2="48" x1="2816" />
            <wire x2="2816" y1="48" y2="96" x1="2816" />
        </branch>
        <branch name="slugAnimationOnOffOut">
            <wire x2="2816" y1="160" y2="160" x1="1808" />
            <wire x2="3184" y1="112" y2="112" x1="2816" />
            <wire x2="2816" y1="112" y2="160" x1="2816" />
        </branch>
        <branch name="leftRightSlugOut">
            <wire x2="2816" y1="224" y2="224" x1="1808" />
            <wire x2="2816" y1="192" y2="224" x1="2816" />
            <wire x2="3184" y1="192" y2="192" x1="2816" />
        </branch>
        <branch name="slugFlashAnimOnOut">
            <wire x2="2816" y1="288" y2="288" x1="1808" />
            <wire x2="3184" y1="272" y2="272" x1="2816" />
            <wire x2="2816" y1="272" y2="288" x1="2816" />
        </branch>
        <branch name="diamondAnimOnOffOut">
            <wire x2="3184" y1="352" y2="352" x1="1808" />
        </branch>
        <branch name="incScoreOut">
            <wire x2="2816" y1="416" y2="416" x1="1808" />
            <wire x2="2816" y1="416" y2="432" x1="2816" />
            <wire x2="3184" y1="432" y2="432" x1="2816" />
        </branch>
        <branch name="incLevelOut">
            <wire x2="2816" y1="480" y2="480" x1="1808" />
            <wire x2="2816" y1="480" y2="496" x1="2816" />
            <wire x2="3184" y1="496" y2="496" x1="2816" />
        </branch>
        <branch name="resetFourSecTimerOut">
            <wire x2="2800" y1="544" y2="544" x1="1808" />
            <wire x2="2800" y1="544" y2="560" x1="2800" />
            <wire x2="3200" y1="560" y2="560" x1="2800" />
        </branch>
        <branch name="gameWinSigOut">
            <wire x2="2800" y1="608" y2="608" x1="1808" />
            <wire x2="2800" y1="608" y2="640" x1="2800" />
            <wire x2="3200" y1="640" y2="640" x1="2800" />
        </branch>
        <branch name="slugMagentaOut">
            <wire x2="2800" y1="672" y2="672" x1="1808" />
            <wire x2="2800" y1="672" y2="720" x1="2800" />
            <wire x2="3200" y1="720" y2="720" x1="2800" />
        </branch>
        <branch name="gameOverOut">
            <wire x2="2800" y1="736" y2="736" x1="1808" />
            <wire x2="2800" y1="736" y2="816" x1="2800" />
            <wire x2="3184" y1="816" y2="816" x1="2800" />
        </branch>
        <iomarker fontsize="28" x="3184" y="48" name="slugIncSizeOut" orien="R0" />
        <iomarker fontsize="28" x="3184" y="112" name="slugAnimationOnOffOut" orien="R0" />
        <iomarker fontsize="28" x="3184" y="192" name="leftRightSlugOut" orien="R0" />
        <iomarker fontsize="28" x="3184" y="272" name="slugFlashAnimOnOut" orien="R0" />
        <iomarker fontsize="28" x="3184" y="352" name="diamondAnimOnOffOut" orien="R0" />
        <iomarker fontsize="28" x="3184" y="432" name="incScoreOut" orien="R0" />
        <iomarker fontsize="28" x="3184" y="496" name="incLevelOut" orien="R0" />
        <iomarker fontsize="28" x="3200" y="560" name="resetFourSecTimerOut" orien="R0" />
        <iomarker fontsize="28" x="3200" y="640" name="gameWinSigOut" orien="R0" />
        <iomarker fontsize="28" x="3200" y="720" name="slugMagentaOut" orien="R0" />
        <iomarker fontsize="28" x="3184" y="816" name="gameOverOut" orien="R0" />
        <instance x="1104" y="1216" name="XLXI_7" orien="R0" />
        <instance x="1104" y="1536" name="XLXI_8" orien="R0" />
        <instance x="1104" y="1856" name="XLXI_9" orien="R0" />
        <instance x="1104" y="2176" name="XLXI_10" orien="R0" />
        <instance x="1104" y="2496" name="XLXI_11" orien="R0" />
        <text style="fontsize:28;fontname:Arial" x="1276" y="920">5</text>
        <text style="fontsize:28;fontname:Arial" x="1280" y="1244">6</text>
        <text style="fontsize:28;fontname:Arial" x="1288" y="1580">7</text>
        <text style="fontsize:28;fontname:Arial" x="1284" y="1892">8</text>
        <text style="fontsize:28;fontname:Arial" x="1284" y="2216">9</text>
        <branch name="clkin">
            <wire x2="624" y1="688" y2="688" x1="272" />
        </branch>
        <iomarker fontsize="28" x="272" y="688" name="clkin" orien="R180" />
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1088" type="branch" />
            <wire x2="1104" y1="1088" y2="1088" x1="1072" />
        </branch>
        <branch name="dLevelUp">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="960" type="branch" />
            <wire x2="1104" y1="960" y2="960" x1="1072" />
        </branch>
        <branch name="qLevelUp">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="960" type="branch" />
            <wire x2="1520" y1="960" y2="960" x1="1488" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1408" type="branch" />
            <wire x2="1104" y1="1408" y2="1408" x1="1072" />
        </branch>
        <branch name="dAnimation">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1280" type="branch" />
            <wire x2="1104" y1="1280" y2="1280" x1="1072" />
        </branch>
        <branch name="qAnimation">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="1280" type="branch" />
            <wire x2="1520" y1="1280" y2="1280" x1="1488" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1728" type="branch" />
            <wire x2="1104" y1="1728" y2="1728" x1="1072" />
        </branch>
        <branch name="dResetTimer">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1600" type="branch" />
            <wire x2="1104" y1="1600" y2="1600" x1="1072" />
        </branch>
        <branch name="qResetTimer">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="1600" type="branch" />
            <wire x2="1520" y1="1600" y2="1600" x1="1488" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="2048" type="branch" />
            <wire x2="1104" y1="2048" y2="2048" x1="1072" />
        </branch>
        <branch name="dGameOver">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1920" type="branch" />
            <wire x2="1104" y1="1920" y2="1920" x1="1072" />
        </branch>
        <branch name="qGameOver">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="1920" type="branch" />
            <wire x2="1520" y1="1920" y2="1920" x1="1488" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="2368" type="branch" />
            <wire x2="1104" y1="2368" y2="2368" x1="1072" />
        </branch>
        <branch name="dWin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="2240" type="branch" />
            <wire x2="1104" y1="2240" y2="2240" x1="1072" />
        </branch>
        <branch name="qWin">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="2240" type="branch" />
            <wire x2="1520" y1="2240" y2="2240" x1="1488" />
        </branch>
        <instance x="272" y="1856" name="XLXI_4" orien="R0" />
        <instance x="272" y="2176" name="XLXI_5" orien="R0" />
        <instance x="272" y="2496" name="XLXI_6" orien="R0" />
        <text style="fontsize:28;fontname:Arial" x="452" y="928">0</text>
        <text style="fontsize:28;fontname:Arial" x="448" y="1568">2</text>
        <text style="fontsize:28;fontname:Arial" x="452" y="1888">3</text>
        <text style="fontsize:28;fontname:Arial" x="464" y="2208">4</text>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="1088" type="branch" />
            <wire x2="256" y1="1088" y2="1088" x1="240" />
            <wire x2="432" y1="1072" y2="1072" x1="256" />
            <wire x2="256" y1="1072" y2="1088" x1="256" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="1728" type="branch" />
            <wire x2="272" y1="1728" y2="1728" x1="240" />
        </branch>
        <branch name="dSwitchSlug">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="1600" type="branch" />
            <wire x2="272" y1="1600" y2="1600" x1="240" />
        </branch>
        <branch name="qSwitchSlug">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="688" y="1600" type="branch" />
            <wire x2="688" y1="1600" y2="1600" x1="656" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="2048" type="branch" />
            <wire x2="272" y1="2048" y2="2048" x1="240" />
        </branch>
        <branch name="dCountUp">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="1920" type="branch" />
            <wire x2="272" y1="1920" y2="1920" x1="240" />
        </branch>
        <branch name="qCountUp">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="688" y="1920" type="branch" />
            <wire x2="688" y1="1920" y2="1920" x1="656" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="2368" type="branch" />
            <wire x2="272" y1="2368" y2="2368" x1="240" />
        </branch>
        <branch name="dIncSize">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="2240" type="branch" />
            <wire x2="272" y1="2240" y2="2240" x1="240" />
        </branch>
        <branch name="qIncSize">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="688" y="2240" type="branch" />
            <wire x2="688" y1="2240" y2="2240" x1="656" />
        </branch>
        <branch name="qWin,qGameOver,qResetTimer,qAnimation,qLevelUp,qIncSize,qCountUp,qSwitchSlug,qGamePlayStart,qGameInitial">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1152" y="736" type="branch" />
            <wire x2="1200" y1="736" y2="736" x1="1152" />
        </branch>
        <branch name="dWin,dGameOver,dResetTimer,dAnimation,dLevelUp,dIncSize,dCountUp,dSwitchSlug,dGamePlayStart,dGameInitial">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1856" y="800" type="branch" />
            <wire x2="1856" y1="800" y2="800" x1="1808" />
        </branch>
        <instance x="1200" y="832" name="XLXI_12" orien="R0">
        </instance>
        <branch name="resetScoreOut">
            <wire x2="3168" y1="864" y2="864" x1="1808" />
            <wire x2="3168" y1="864" y2="912" x1="3168" />
            <wire x2="3184" y1="912" y2="912" x1="3168" />
        </branch>
        <iomarker fontsize="28" x="3184" y="912" name="resetScoreOut" orien="R0" />
        <instance x="432" y="1200" name="XLXI_2" orien="R0" />
        <branch name="XLXN_9">
            <wire x2="416" y1="944" y2="944" x1="400" />
            <wire x2="432" y1="944" y2="944" x1="416" />
        </branch>
        <instance x="176" y="976" name="XLXI_13" orien="R0" />
        <instance x="848" y="944" name="XLXI_14" orien="R90" />
        <branch name="XLXN_11">
            <wire x2="832" y1="944" y2="944" x1="816" />
            <wire x2="864" y1="944" y2="944" x1="832" />
            <wire x2="880" y1="944" y2="944" x1="864" />
        </branch>
        <branch name="qGameInitial">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="880" y="1184" type="branch" />
            <wire x2="880" y1="1168" y2="1184" x1="880" />
        </branch>
        <branch name="dGameInitial">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="160" y="944" type="branch" />
            <wire x2="176" y1="944" y2="944" x1="160" />
        </branch>
        <text style="fontsize:28;fontname:Arial" x="404" y="1240">1</text>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="192" y="1408" type="branch" />
            <wire x2="224" y1="1408" y2="1408" x1="192" />
        </branch>
        <branch name="dGamePlayStart">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="192" y="1280" type="branch" />
            <wire x2="224" y1="1280" y2="1280" x1="192" />
        </branch>
        <branch name="qGamePlayStart">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="640" y="1280" type="branch" />
            <wire x2="640" y1="1280" y2="1280" x1="608" />
        </branch>
        <instance x="224" y="1536" name="XLXI_3" orien="R0" />
        <text style="fontsize:64;fontname:Arial" x="2180" y="1080">Schematic 3 GameStateMachine</text>
    </sheet>
</drawing>