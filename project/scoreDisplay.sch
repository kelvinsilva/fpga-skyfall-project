<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="_gnd" />
        <signal name="outputA" />
        <signal name="outputB" />
        <signal name="outputC" />
        <signal name="outputD" />
        <signal name="outputF" />
        <signal name="outputE" />
        <signal name="outputG" />
        <signal name="_outA" />
        <signal name="_outB" />
        <signal name="_outC" />
        <signal name="_outD" />
        <signal name="_outE" />
        <signal name="_outF" />
        <signal name="_outG" />
        <signal name="_negA" />
        <signal name="_negG" />
        <signal name="_negF" />
        <signal name="_negE" />
        <signal name="_negD" />
        <signal name="_negC" />
        <signal name="_negB" />
        <signal name="an0" />
        <signal name="clkin" />
        <signal name="inc" />
        <signal name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,d7,d6,d5,d4,d3,d2,d1,d0" />
        <signal name="d0" />
        <signal name="d2" />
        <signal name="d3" />
        <signal name="d4" />
        <signal name="d6" />
        <signal name="d7" />
        <signal name="d1" />
        <signal name="d5" />
        <signal name="sOut(3:0)" />
        <signal name="_outencA" />
        <signal name="_outencB" />
        <signal name="digitalADV" />
        <signal name="an2" />
        <signal name="an3" />
        <signal name="an1" />
        <signal name="enableIn" />
        <signal name="outA0" />
        <signal name="outA1" />
        <signal name="outA2" />
        <signal name="outA3" />
        <signal name="outA4" />
        <signal name="outA5" />
        <signal name="outA6" />
        <signal name="increaseAux" />
        <signal name="_vcc" />
        <signal name="resetData" />
        <signal name="_Q(3:0)" />
        <signal name="XLXN_188" />
        <signal name="Q0" />
        <signal name="Q1" />
        <signal name="Q2" />
        <signal name="Q3" />
        <signal name="Q4" />
        <signal name="Q5" />
        <signal name="Q6" />
        <signal name="Q7" />
        <signal name="XLXN_144(7:0)" />
        <signal name="_Q(7:0)" />
        <signal name="XLXN_175" />
        <signal name="XLXN_176" />
        <signal name="XLXN_177" />
        <signal name="XLXN_178" />
        <signal name="XLXN_179" />
        <signal name="XLXN_180" />
        <signal name="XLXN_181" />
        <signal name="an3_Out" />
        <signal name="an2_Out" />
        <signal name="an0_Out" />
        <signal name="an1_Out" />
        <signal name="countOut(7:0)" />
        <signal name="countOutAux(3:0)" />
        <signal name="Q7,Q6,Q5,Q4,Q3,Q2,Q1,Q0" />
        <port polarity="Output" name="outputA" />
        <port polarity="Output" name="outputB" />
        <port polarity="Output" name="outputC" />
        <port polarity="Output" name="outputD" />
        <port polarity="Output" name="outputF" />
        <port polarity="Output" name="outputE" />
        <port polarity="Output" name="outputG" />
        <port polarity="Output" name="an0" />
        <port polarity="Input" name="clkin" />
        <port polarity="Input" name="inc" />
        <port polarity="Input" name="digitalADV" />
        <port polarity="Input" name="enableIn" />
        <port polarity="Input" name="increaseAux" />
        <port polarity="Input" name="resetData" />
        <port polarity="Output" name="an3_Out" />
        <port polarity="Output" name="an2_Out" />
        <port polarity="Output" name="an0_Out" />
        <port polarity="Output" name="an1_Out" />
        <port polarity="Output" name="countOut(7:0)" />
        <port polarity="Output" name="countOutAux(3:0)" />
        <blockdef name="cb8cled">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="64" x="0" y="-460" height="24" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="64" x="320" y="-460" height="24" />
            <rect width="256" x="64" y="-512" height="448" />
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="320" y1="-448" y2="-448" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="hex7seg">
            <timestamp>2016-5-24T3:18:55</timestamp>
            <rect width="256" x="64" y="-448" height="448" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="m4_1e">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-416" y2="-416" x1="0" />
            <line x2="96" y1="-352" y2="-352" x1="0" />
            <line x2="96" y1="-288" y2="-288" x1="0" />
            <line x2="96" y1="-224" y2="-224" x1="0" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
            <line x2="256" y1="-320" y2="-320" x1="320" />
            <line x2="96" y1="-160" y2="-160" x1="0" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
            <line x2="96" y1="-96" y2="-96" x1="176" />
            <line x2="176" y1="-208" y2="-96" x1="176" />
            <line x2="96" y1="-32" y2="-32" x1="224" />
            <line x2="224" y1="-216" y2="-32" x1="224" />
            <line x2="96" y1="-224" y2="-192" x1="256" />
            <line x2="256" y1="-416" y2="-224" x1="256" />
            <line x2="256" y1="-448" y2="-416" x1="96" />
            <line x2="96" y1="-192" y2="-448" x1="96" />
            <line x2="96" y1="-160" y2="-160" x1="128" />
            <line x2="128" y1="-200" y2="-160" x1="128" />
        </blockdef>
        <blockdef name="selector">
            <timestamp>2016-5-24T3:18:59</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <blockdef name="ringCounter">
            <timestamp>2016-5-24T3:19:3</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="fourOneHotToTwoBitNumber">
            <timestamp>2016-5-24T3:19:8</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="hex7SegNegativeDisplay">
            <timestamp>2016-5-24T3:20:22</timestamp>
            <rect width="256" x="64" y="-512" height="512" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="sgnChangerForHexDisp">
            <timestamp>2016-5-24T3:18:30</timestamp>
            <rect width="256" x="64" y="-576" height="576" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="constant">
            <timestamp>2006-1-1T10:10:10</timestamp>
            <rect width="112" x="0" y="0" height="64" />
            <line x2="112" y1="32" y2="32" x1="144" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="cd4re">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <rect width="256" x="64" y="-512" height="448" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="320" y1="-320" y2="-320" x1="384" />
            <line x2="320" y1="-384" y2="-384" x1="384" />
            <line x2="320" y1="-448" y2="-448" x1="384" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
        </blockdef>
        <block symbolname="hex7seg" name="XLXI_7">
            <blockpin signalname="sOut(3:0)" name="inNum(3:0)" />
            <blockpin signalname="_outA" name="outA" />
            <blockpin signalname="_outB" name="outB" />
            <blockpin signalname="_outC" name="outC" />
            <blockpin signalname="_outD" name="outD" />
            <blockpin signalname="_outE" name="outE" />
            <blockpin signalname="_outF" name="outF" />
            <blockpin signalname="_outG" name="outG" />
        </block>
        <block symbolname="m4_1e" name="XLXI_12">
            <blockpin signalname="_outA" name="D0" />
            <blockpin signalname="_outA" name="D1" />
            <blockpin signalname="_negA" name="D2" />
            <blockpin signalname="outA0" name="D3" />
            <blockpin signalname="enableIn" name="E" />
            <blockpin signalname="_outencB" name="S0" />
            <blockpin signalname="_outencA" name="S1" />
            <blockpin signalname="outputA" name="O" />
        </block>
        <block symbolname="m4_1e" name="XLXI_13">
            <blockpin signalname="_outB" name="D0" />
            <blockpin signalname="_outB" name="D1" />
            <blockpin signalname="_negB" name="D2" />
            <blockpin signalname="outA1" name="D3" />
            <blockpin signalname="enableIn" name="E" />
            <blockpin signalname="_outencB" name="S0" />
            <blockpin signalname="_outencA" name="S1" />
            <blockpin signalname="outputB" name="O" />
        </block>
        <block symbolname="m4_1e" name="XLXI_14">
            <blockpin signalname="_outC" name="D0" />
            <blockpin signalname="_outC" name="D1" />
            <blockpin signalname="_negC" name="D2" />
            <blockpin signalname="outA2" name="D3" />
            <blockpin signalname="enableIn" name="E" />
            <blockpin signalname="_outencB" name="S0" />
            <blockpin signalname="_outencA" name="S1" />
            <blockpin signalname="outputC" name="O" />
        </block>
        <block symbolname="m4_1e" name="XLXI_15">
            <blockpin signalname="_outD" name="D0" />
            <blockpin signalname="_outD" name="D1" />
            <blockpin signalname="_negD" name="D2" />
            <blockpin signalname="outA3" name="D3" />
            <blockpin signalname="enableIn" name="E" />
            <blockpin signalname="_outencB" name="S0" />
            <blockpin signalname="_outencA" name="S1" />
            <blockpin signalname="outputD" name="O" />
        </block>
        <block symbolname="m4_1e" name="XLXI_16">
            <blockpin signalname="_outE" name="D0" />
            <blockpin signalname="_outE" name="D1" />
            <blockpin signalname="_negE" name="D2" />
            <blockpin signalname="outA4" name="D3" />
            <blockpin signalname="enableIn" name="E" />
            <blockpin signalname="_outencB" name="S0" />
            <blockpin signalname="_outencA" name="S1" />
            <blockpin signalname="outputE" name="O" />
        </block>
        <block symbolname="m4_1e" name="XLXI_17">
            <blockpin signalname="_outF" name="D0" />
            <blockpin signalname="_outF" name="D1" />
            <blockpin signalname="_negF" name="D2" />
            <blockpin signalname="outA5" name="D3" />
            <blockpin signalname="enableIn" name="E" />
            <blockpin signalname="_outencB" name="S0" />
            <blockpin signalname="_outencA" name="S1" />
            <blockpin signalname="outputF" name="O" />
        </block>
        <block symbolname="m4_1e" name="XLXI_18">
            <blockpin signalname="_outG" name="D0" />
            <blockpin signalname="_outG" name="D1" />
            <blockpin signalname="_negG" name="D2" />
            <blockpin signalname="outA6" name="D3" />
            <blockpin signalname="enableIn" name="E" />
            <blockpin signalname="_outencB" name="S0" />
            <blockpin signalname="_outencA" name="S1" />
            <blockpin signalname="outputG" name="O" />
        </block>
        <block symbolname="selector" name="XLXI_19">
            <blockpin signalname="an0" name="s0" />
            <blockpin signalname="an1" name="s1" />
            <blockpin signalname="an2" name="s2" />
            <blockpin signalname="an3" name="s3" />
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,d7,d6,d5,d4,d3,d2,d1,d0" name="x(15:0)" />
            <blockpin signalname="sOut(3:0)" name="y(3:0)" />
        </block>
        <block symbolname="hex7SegNegativeDisplay" name="XLXI_27">
            <blockpin signalname="Q7" name="negative" />
            <blockpin signalname="_negB" name="B" />
            <blockpin signalname="_negC" name="C" />
            <blockpin signalname="_negD" name="D" />
            <blockpin signalname="_negE" name="E" />
            <blockpin signalname="_negF" name="F" />
            <blockpin signalname="_negG" name="G" />
            <blockpin name="_VCC" />
            <blockpin signalname="_negA" name="A" />
        </block>
        <block symbolname="sgnChangerForHexDisp" name="XLXI_30">
            <blockpin signalname="Q0" name="in0" />
            <blockpin signalname="Q1" name="in1" />
            <blockpin signalname="Q2" name="in2" />
            <blockpin signalname="Q3" name="in3" />
            <blockpin signalname="Q4" name="in4" />
            <blockpin signalname="Q5" name="in5" />
            <blockpin signalname="Q6" name="in6" />
            <blockpin signalname="Q7" name="in7" />
            <blockpin signalname="_gnd" name="sign" />
            <blockpin signalname="d0" name="d0" />
            <blockpin signalname="d2" name="d2" />
            <blockpin signalname="d3" name="d3" />
            <blockpin signalname="d4" name="d4" />
            <blockpin signalname="d5" name="d5" />
            <blockpin signalname="d6" name="d6" />
            <blockpin signalname="d7" name="d7" />
            <blockpin signalname="d1" name="d1" />
        </block>
        <block symbolname="gnd" name="XLXI_3">
            <blockpin signalname="_gnd" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_11">
            <blockpin signalname="_vcc" name="P" />
        </block>
        <block symbolname="cd4re" name="XLXI_63">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="inc" name="CE" />
            <blockpin signalname="resetData" name="R" />
            <blockpin signalname="XLXN_188" name="CEO" />
            <blockpin signalname="Q0" name="Q0" />
            <blockpin signalname="Q1" name="Q1" />
            <blockpin signalname="Q2" name="Q2" />
            <blockpin signalname="Q3" name="Q3" />
            <blockpin name="TC" />
        </block>
        <block symbolname="cd4re" name="XLXI_64">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="XLXN_188" name="CE" />
            <blockpin signalname="resetData" name="R" />
            <blockpin name="CEO" />
            <blockpin signalname="Q4" name="Q0" />
            <blockpin signalname="Q5" name="Q1" />
            <blockpin signalname="Q6" name="Q2" />
            <blockpin signalname="Q7" name="Q3" />
            <blockpin name="TC" />
        </block>
        <block symbolname="constant" name="XLXI_36">
            <attr value="01" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_144(7:0)" name="O" />
        </block>
        <block symbolname="cb8cled" name="XLXI_35">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="increaseAux" name="CE" />
            <blockpin signalname="_gnd" name="CLR" />
            <blockpin signalname="XLXN_144(7:0)" name="D(7:0)" />
            <blockpin signalname="resetData" name="L" />
            <blockpin signalname="_vcc" name="UP" />
            <blockpin name="CEO" />
            <blockpin signalname="_Q(7:0)" name="Q(7:0)" />
            <blockpin name="TC" />
        </block>
        <block symbolname="buf" name="XLXI_37">
            <blockpin signalname="XLXN_175" name="I" />
            <blockpin signalname="outA0" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_38">
            <blockpin signalname="XLXN_176" name="I" />
            <blockpin signalname="outA1" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_39">
            <blockpin signalname="XLXN_177" name="I" />
            <blockpin signalname="outA2" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_41">
            <blockpin signalname="XLXN_178" name="I" />
            <blockpin signalname="outA3" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_43">
            <blockpin signalname="XLXN_179" name="I" />
            <blockpin signalname="outA4" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_44">
            <blockpin signalname="XLXN_180" name="I" />
            <blockpin signalname="outA5" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_45">
            <blockpin signalname="XLXN_181" name="I" />
            <blockpin signalname="outA6" name="O" />
        </block>
        <block symbolname="hex7seg" name="XLXI_55">
            <blockpin signalname="_Q(3:0)" name="inNum(3:0)" />
            <blockpin signalname="XLXN_175" name="outA" />
            <blockpin signalname="XLXN_176" name="outB" />
            <blockpin signalname="XLXN_177" name="outC" />
            <blockpin signalname="XLXN_178" name="outD" />
            <blockpin signalname="XLXN_179" name="outE" />
            <blockpin signalname="XLXN_180" name="outF" />
            <blockpin signalname="XLXN_181" name="outG" />
        </block>
        <block symbolname="ringCounter" name="XLXI_20">
            <blockpin signalname="clkin" name="clkin" />
            <blockpin signalname="digitalADV" name="en" />
            <blockpin signalname="an1" name="an1" />
            <blockpin signalname="an3" name="an3" />
            <blockpin signalname="an2" name="an2" />
            <blockpin signalname="an0" name="an0" />
        </block>
        <block symbolname="inv" name="XLXI_26">
            <blockpin signalname="an0" name="I" />
            <blockpin signalname="an3_Out" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_25">
            <blockpin signalname="an1" name="I" />
            <blockpin signalname="an2_Out" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_24">
            <blockpin signalname="an2" name="I" />
            <blockpin signalname="an1_Out" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_23">
            <blockpin signalname="an3" name="I" />
            <blockpin signalname="an0_Out" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_58(7:0)">
            <blockpin signalname="Q7,Q6,Q5,Q4,Q3,Q2,Q1,Q0" name="I" />
            <blockpin signalname="countOut(7:0)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_59(3:0)">
            <blockpin signalname="_Q(3:0)" name="I" />
            <blockpin signalname="countOutAux(3:0)" name="O" />
        </block>
        <block symbolname="fourOneHotToTwoBitNumber" name="XLXI_21">
            <blockpin signalname="an0" name="s0" />
            <blockpin signalname="an1" name="s1" />
            <blockpin signalname="an2" name="s2" />
            <blockpin signalname="an3" name="s3" />
            <blockpin signalname="_outencA" name="A" />
            <blockpin signalname="_outencB" name="B" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="Q7">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1840" y="112" type="branch" />
            <wire x2="1856" y1="112" y2="112" x1="1840" />
        </branch>
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1232" y="752" type="branch" />
            <wire x2="1248" y1="752" y2="752" x1="1232" />
        </branch>
        <instance x="1856" y="1136" name="XLXI_7" orien="R0">
        </instance>
        <instance x="2976" y="480" name="XLXI_12" orien="R0" />
        <instance x="2960" y="912" name="XLXI_13" orien="R0" />
        <instance x="2976" y="1824" name="XLXI_15" orien="R0" />
        <instance x="2976" y="2272" name="XLXI_16" orien="R0" />
        <instance x="2992" y="2704" name="XLXI_17" orien="R0" />
        <instance x="2384" y="2688" name="XLXI_18" orien="R0" />
        <branch name="outputA">
            <wire x2="3328" y1="160" y2="160" x1="3296" />
        </branch>
        <branch name="outputB">
            <wire x2="3312" y1="592" y2="592" x1="3280" />
        </branch>
        <branch name="outputC">
            <wire x2="3296" y1="1056" y2="1056" x1="3264" />
        </branch>
        <branch name="outputD">
            <wire x2="3312" y1="1504" y2="1504" x1="3296" />
        </branch>
        <branch name="outputF">
            <wire x2="3344" y1="2384" y2="2384" x1="3312" />
        </branch>
        <branch name="outputE">
            <wire x2="3328" y1="1952" y2="1952" x1="3296" />
        </branch>
        <branch name="outputG">
            <wire x2="2736" y1="2368" y2="2368" x1="2704" />
        </branch>
        <branch name="_outG">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2352" y="2272" type="branch" />
            <wire x2="2384" y1="2272" y2="2272" x1="2352" />
        </branch>
        <branch name="_outG">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2352" y="2336" type="branch" />
            <wire x2="2384" y1="2336" y2="2336" x1="2352" />
        </branch>
        <branch name="_negG">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2352" y="2400" type="branch" />
            <wire x2="2384" y1="2400" y2="2400" x1="2352" />
        </branch>
        <branch name="outA6">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2352" y="2464" type="branch" />
            <wire x2="2384" y1="2464" y2="2464" x1="2352" />
        </branch>
        <branch name="_outencB">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2352" y="2528" type="branch" />
            <wire x2="2384" y1="2528" y2="2528" x1="2352" />
        </branch>
        <branch name="_outencA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2352" y="2592" type="branch" />
            <wire x2="2384" y1="2592" y2="2592" x1="2352" />
        </branch>
        <branch name="_outencA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="2608" type="branch" />
            <wire x2="2992" y1="2608" y2="2608" x1="2976" />
        </branch>
        <branch name="_outencB">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="2544" type="branch" />
            <wire x2="2992" y1="2544" y2="2544" x1="2976" />
        </branch>
        <branch name="outA5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="2480" type="branch" />
            <wire x2="2992" y1="2480" y2="2480" x1="2976" />
        </branch>
        <branch name="_negF">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="2416" type="branch" />
            <wire x2="2992" y1="2416" y2="2416" x1="2976" />
        </branch>
        <branch name="_outF">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="2352" type="branch" />
            <wire x2="2992" y1="2352" y2="2352" x1="2976" />
        </branch>
        <branch name="_outF">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="2288" type="branch" />
            <wire x2="2992" y1="2288" y2="2288" x1="2976" />
        </branch>
        <branch name="_outencA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="2176" type="branch" />
            <wire x2="2976" y1="2176" y2="2176" x1="2960" />
        </branch>
        <branch name="_outencB">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2944" y="2112" type="branch" />
            <wire x2="2976" y1="2112" y2="2112" x1="2944" />
        </branch>
        <branch name="outA4">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2944" y="2048" type="branch" />
            <wire x2="2976" y1="2048" y2="2048" x1="2944" />
        </branch>
        <branch name="_negE">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2928" y="1984" type="branch" />
            <wire x2="2976" y1="1984" y2="1984" x1="2928" />
        </branch>
        <branch name="_outE">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="1920" type="branch" />
            <wire x2="2976" y1="1920" y2="1920" x1="2960" />
        </branch>
        <branch name="_outE">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="1856" type="branch" />
            <wire x2="2976" y1="1856" y2="1856" x1="2960" />
        </branch>
        <branch name="_outencA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2928" y="1728" type="branch" />
            <wire x2="2976" y1="1728" y2="1728" x1="2928" />
        </branch>
        <branch name="_outencB">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2928" y="1664" type="branch" />
            <wire x2="2976" y1="1664" y2="1664" x1="2928" />
        </branch>
        <branch name="outA3">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2944" y="1600" type="branch" />
            <wire x2="2976" y1="1600" y2="1600" x1="2944" />
        </branch>
        <branch name="_negD">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2944" y="1536" type="branch" />
            <wire x2="2976" y1="1536" y2="1536" x1="2944" />
        </branch>
        <branch name="_outD">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="1472" type="branch" />
            <wire x2="2976" y1="1472" y2="1472" x1="2960" />
        </branch>
        <branch name="_outD">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="1408" type="branch" />
            <wire x2="2976" y1="1408" y2="1408" x1="2960" />
        </branch>
        <branch name="_outencA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2928" y="1280" type="branch" />
            <wire x2="2944" y1="1280" y2="1280" x1="2928" />
        </branch>
        <branch name="_outencB">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2928" y="1216" type="branch" />
            <wire x2="2944" y1="1216" y2="1216" x1="2928" />
        </branch>
        <branch name="outA2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2928" y="1152" type="branch" />
            <wire x2="2944" y1="1152" y2="1152" x1="2928" />
        </branch>
        <branch name="_outencA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2944" y="816" type="branch" />
            <wire x2="2960" y1="816" y2="816" x1="2944" />
        </branch>
        <branch name="_outencB">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2944" y="752" type="branch" />
            <wire x2="2960" y1="752" y2="752" x1="2944" />
        </branch>
        <branch name="outA1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2928" y="688" type="branch" />
            <wire x2="2960" y1="688" y2="688" x1="2928" />
        </branch>
        <branch name="_negB">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2928" y="624" type="branch" />
            <wire x2="2960" y1="624" y2="624" x1="2928" />
        </branch>
        <branch name="_outB">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2944" y="560" type="branch" />
            <wire x2="2960" y1="560" y2="560" x1="2944" />
        </branch>
        <branch name="_outB">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2944" y="496" type="branch" />
            <wire x2="2960" y1="496" y2="496" x1="2944" />
        </branch>
        <branch name="_outencA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="384" type="branch" />
            <wire x2="2976" y1="384" y2="384" x1="2960" />
        </branch>
        <branch name="_outencB">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="320" type="branch" />
            <wire x2="2976" y1="320" y2="320" x1="2960" />
        </branch>
        <branch name="outA0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="256" type="branch" />
            <wire x2="2976" y1="256" y2="256" x1="2960" />
        </branch>
        <branch name="_negA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="192" type="branch" />
            <wire x2="2976" y1="192" y2="192" x1="2960" />
        </branch>
        <branch name="_outA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="128" type="branch" />
            <wire x2="2976" y1="128" y2="128" x1="2960" />
        </branch>
        <branch name="_outA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="64" type="branch" />
            <wire x2="2976" y1="64" y2="64" x1="2960" />
        </branch>
        <branch name="_outA">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="720" type="branch" />
            <wire x2="2272" y1="720" y2="720" x1="2240" />
        </branch>
        <branch name="_outB">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="784" type="branch" />
            <wire x2="2272" y1="784" y2="784" x1="2240" />
        </branch>
        <branch name="_outC">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="848" type="branch" />
            <wire x2="2272" y1="848" y2="848" x1="2240" />
        </branch>
        <branch name="_outD">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="912" type="branch" />
            <wire x2="2272" y1="912" y2="912" x1="2240" />
        </branch>
        <branch name="_outE">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="976" type="branch" />
            <wire x2="2288" y1="976" y2="976" x1="2240" />
        </branch>
        <branch name="_outF">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="1040" type="branch" />
            <wire x2="2272" y1="1040" y2="1040" x1="2240" />
        </branch>
        <branch name="_outG">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="1104" type="branch" />
            <wire x2="2272" y1="1104" y2="1104" x1="2240" />
        </branch>
        <branch name="_negA">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="560" type="branch" />
            <wire x2="2272" y1="560" y2="560" x1="2240" />
        </branch>
        <branch name="_negG">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2256" y="432" type="branch" />
            <wire x2="2256" y1="432" y2="432" x1="2240" />
        </branch>
        <branch name="_negF">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2256" y="368" type="branch" />
            <wire x2="2256" y1="368" y2="368" x1="2240" />
        </branch>
        <branch name="_negE">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2256" y="304" type="branch" />
            <wire x2="2256" y1="304" y2="304" x1="2240" />
        </branch>
        <branch name="_negD">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2256" y="240" type="branch" />
            <wire x2="2256" y1="240" y2="240" x1="2240" />
        </branch>
        <branch name="_negC">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="176" type="branch" />
            <wire x2="2272" y1="176" y2="176" x1="2240" />
        </branch>
        <branch name="_negB">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2256" y="112" type="branch" />
            <wire x2="2256" y1="112" y2="112" x1="2240" />
        </branch>
        <instance x="1264" y="1600" name="XLXI_19" orien="R0">
        </instance>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="288" y="496" type="branch" />
            <wire x2="352" y1="496" y2="496" x1="288" />
        </branch>
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,d7,d6,d5,d4,d3,d2,d1,d0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1232" y="1568" type="branch" />
            <wire x2="1264" y1="1568" y2="1568" x1="1232" />
        </branch>
        <branch name="Q0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1232" y="240" type="branch" />
            <wire x2="1248" y1="240" y2="240" x1="1232" />
        </branch>
        <branch name="Q1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1232" y="304" type="branch" />
            <wire x2="1248" y1="304" y2="304" x1="1232" />
        </branch>
        <branch name="Q2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1232" y="368" type="branch" />
            <wire x2="1248" y1="368" y2="368" x1="1232" />
        </branch>
        <branch name="Q3">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1232" y="432" type="branch" />
            <wire x2="1248" y1="432" y2="432" x1="1232" />
        </branch>
        <branch name="Q4">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1232" y="496" type="branch" />
            <wire x2="1248" y1="496" y2="496" x1="1232" />
        </branch>
        <branch name="Q5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1216" y="560" type="branch" />
            <wire x2="1248" y1="560" y2="560" x1="1216" />
        </branch>
        <branch name="Q6">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1216" y="624" type="branch" />
            <wire x2="1248" y1="624" y2="624" x1="1216" />
        </branch>
        <branch name="Q7">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1232" y="688" type="branch" />
            <wire x2="1248" y1="688" y2="688" x1="1232" />
        </branch>
        <branch name="d0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1648" y="240" type="branch" />
            <wire x2="1648" y1="240" y2="240" x1="1632" />
        </branch>
        <branch name="d2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1648" y="304" type="branch" />
            <wire x2="1648" y1="304" y2="304" x1="1632" />
        </branch>
        <branch name="d3">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1648" y="368" type="branch" />
            <wire x2="1648" y1="368" y2="368" x1="1632" />
        </branch>
        <branch name="d4">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1648" y="432" type="branch" />
            <wire x2="1648" y1="432" y2="432" x1="1632" />
        </branch>
        <branch name="d5">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1648" y="496" type="branch" />
            <wire x2="1648" y1="496" y2="496" x1="1632" />
        </branch>
        <branch name="d6">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="560" type="branch" />
            <wire x2="1664" y1="560" y2="560" x1="1632" />
        </branch>
        <branch name="d7">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1648" y="624" type="branch" />
            <wire x2="1648" y1="624" y2="624" x1="1632" />
        </branch>
        <branch name="d1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1648" y="688" type="branch" />
            <wire x2="1648" y1="688" y2="688" x1="1632" />
        </branch>
        <branch name="sOut(3:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="1312" type="branch" />
            <wire x2="1664" y1="1312" y2="1312" x1="1648" />
        </branch>
        <branch name="sOut(3:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1840" y="720" type="branch" />
            <wire x2="1856" y1="720" y2="720" x1="1840" />
        </branch>
        <iomarker fontsize="28" x="3328" y="160" name="outputA" orien="R0" />
        <iomarker fontsize="28" x="3312" y="592" name="outputB" orien="R0" />
        <iomarker fontsize="28" x="3296" y="1056" name="outputC" orien="R0" />
        <iomarker fontsize="28" x="3312" y="1504" name="outputD" orien="R0" />
        <iomarker fontsize="28" x="3328" y="1952" name="outputE" orien="R0" />
        <iomarker fontsize="28" x="3344" y="2384" name="outputF" orien="R0" />
        <iomarker fontsize="28" x="2736" y="2368" name="outputG" orien="R0" />
        <branch name="an0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="1312" type="branch" />
            <wire x2="1264" y1="1312" y2="1312" x1="1248" />
        </branch>
        <branch name="an1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="1376" type="branch" />
            <wire x2="1264" y1="1376" y2="1376" x1="1248" />
        </branch>
        <branch name="an2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="1440" type="branch" />
            <wire x2="1264" y1="1440" y2="1440" x1="1248" />
        </branch>
        <branch name="an3">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="1504" type="branch" />
            <wire x2="1264" y1="1504" y2="1504" x1="1248" />
        </branch>
        <instance x="1856" y="592" name="XLXI_27" orien="R0">
        </instance>
        <branch name="_negC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2912" y="1088" type="branch" />
            <wire x2="2944" y1="1088" y2="1088" x1="2912" />
        </branch>
        <branch name="_outC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2896" y="1024" type="branch" />
            <wire x2="2944" y1="1024" y2="1024" x1="2896" />
        </branch>
        <branch name="_outC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2912" y="960" type="branch" />
            <wire x2="2944" y1="960" y2="960" x1="2912" />
        </branch>
        <instance x="2944" y="1376" name="XLXI_14" orien="R0" />
        <branch name="enableIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2784" y="448" type="branch" />
            <wire x2="2976" y1="448" y2="448" x1="2784" />
        </branch>
        <branch name="enableIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2784" y="880" type="branch" />
            <wire x2="2960" y1="880" y2="880" x1="2784" />
        </branch>
        <branch name="enableIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2784" y="1344" type="branch" />
            <wire x2="2944" y1="1344" y2="1344" x1="2784" />
        </branch>
        <branch name="enableIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2784" y="1792" type="branch" />
            <wire x2="2976" y1="1792" y2="1792" x1="2784" />
        </branch>
        <branch name="enableIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2800" y="2240" type="branch" />
            <wire x2="2976" y1="2240" y2="2240" x1="2800" />
        </branch>
        <branch name="enableIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2816" y="2672" type="branch" />
            <wire x2="2992" y1="2672" y2="2672" x1="2816" />
        </branch>
        <branch name="enableIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2208" y="2656" type="branch" />
            <wire x2="2384" y1="2656" y2="2656" x1="2208" />
        </branch>
        <instance x="1248" y="784" name="XLXI_30" orien="R0">
        </instance>
        <instance x="48" y="1568" name="XLXI_3" orien="R0" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="112" y="1408" type="branch" />
            <wire x2="112" y1="1408" y2="1440" x1="112" />
        </branch>
        <instance x="176" y="1408" name="XLXI_11" orien="R0" />
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="1488" type="branch" />
            <wire x2="240" y1="1408" y2="1488" x1="240" />
        </branch>
        <branch name="inc">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="64" y="400" type="branch" />
            <wire x2="336" y1="400" y2="400" x1="64" />
            <wire x2="336" y1="400" y2="432" x1="336" />
            <wire x2="352" y1="432" y2="432" x1="336" />
        </branch>
        <branch name="resetData">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="336" y="592" type="branch" />
            <wire x2="352" y1="592" y2="592" x1="336" />
        </branch>
        <instance x="352" y="624" name="XLXI_63" orien="R0" />
        <branch name="clkin">
            <wire x2="544" y1="1760" y2="1760" x1="240" />
        </branch>
        <branch name="inc">
            <wire x2="544" y1="1840" y2="1840" x1="224" />
        </branch>
        <branch name="digitalADV">
            <wire x2="544" y1="1968" y2="1968" x1="320" />
        </branch>
        <rect width="440" x="132" y="1640" height="464" />
        <branch name="enableIn">
            <wire x2="560" y1="2032" y2="2032" x1="304" />
        </branch>
        <branch name="increaseAux">
            <wire x2="544" y1="1664" y2="1664" x1="256" />
        </branch>
        <branch name="resetData">
            <wire x2="544" y1="2080" y2="2080" x1="320" />
        </branch>
        <iomarker fontsize="28" x="240" y="1760" name="clkin" orien="R180" />
        <iomarker fontsize="28" x="224" y="1840" name="inc" orien="R180" />
        <iomarker fontsize="28" x="320" y="1968" name="digitalADV" orien="R180" />
        <iomarker fontsize="28" x="304" y="2032" name="enableIn" orien="R180" />
        <iomarker fontsize="28" x="256" y="1664" name="increaseAux" orien="R180" />
        <iomarker fontsize="28" x="320" y="2080" name="resetData" orien="R180" />
        <instance x="352" y="1152" name="XLXI_64" orien="R0" />
        <branch name="XLXN_188">
            <wire x2="288" y1="608" y2="960" x1="288" />
            <wire x2="352" y1="960" y2="960" x1="288" />
            <wire x2="816" y1="608" y2="608" x1="288" />
            <wire x2="816" y1="432" y2="432" x1="736" />
            <wire x2="816" y1="432" y2="608" x1="816" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="336" y="1024" type="branch" />
            <wire x2="352" y1="1024" y2="1024" x1="336" />
        </branch>
        <branch name="resetData">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="320" y="1120" type="branch" />
            <wire x2="352" y1="1120" y2="1120" x1="320" />
        </branch>
        <branch name="Q0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="752" y="176" type="branch" />
            <wire x2="752" y1="176" y2="176" x1="736" />
        </branch>
        <branch name="Q1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="752" y="240" type="branch" />
            <wire x2="752" y1="240" y2="240" x1="736" />
        </branch>
        <branch name="Q2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="768" y="304" type="branch" />
            <wire x2="768" y1="304" y2="304" x1="736" />
        </branch>
        <branch name="Q3">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="768" y="368" type="branch" />
            <wire x2="768" y1="368" y2="368" x1="736" />
        </branch>
        <branch name="Q4">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="752" y="704" type="branch" />
            <wire x2="752" y1="704" y2="704" x1="736" />
        </branch>
        <branch name="Q5">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="768" y="768" type="branch" />
            <wire x2="768" y1="768" y2="768" x1="736" />
        </branch>
        <branch name="Q6">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="768" y="832" type="branch" />
            <wire x2="768" y1="832" y2="832" x1="736" />
        </branch>
        <branch name="Q7">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="752" y="896" type="branch" />
            <wire x2="752" y1="896" y2="896" x1="736" />
        </branch>
        <rect width="916" x="16" y="20" height="1156" />
        <text style="fontsize:40;fontname:Arial" x="136" y="44">BCD Counter for Score Keepingl</text>
        <rect width="1268" x="1140" y="8" height="1168" />
        <text style="fontsize:36;fontname:Arial" x="1264" y="924">Two Rightmost Digit Hex 7 Seg</text>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="112" y="2592" type="branch" />
            <wire x2="160" y1="2592" y2="2592" x1="112" />
            <wire x2="288" y1="2592" y2="2592" x1="160" />
        </branch>
        <branch name="increaseAux">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="256" y="2528" type="branch" />
            <wire x2="288" y1="2528" y2="2528" x1="256" />
        </branch>
        <branch name="resetData">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="272" y="2464" type="branch" />
            <wire x2="288" y1="2464" y2="2464" x1="272" />
        </branch>
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="272" y="2400" type="branch" />
            <wire x2="288" y1="2400" y2="2400" x1="272" />
        </branch>
        <branch name="XLXN_144(7:0)">
            <wire x2="288" y1="2272" y2="2272" x1="272" />
        </branch>
        <instance x="128" y="2240" name="XLXI_36" orien="R0">
        </instance>
        <branch name="_Q(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="720" y="2272" type="branch" />
            <wire x2="688" y1="2272" y2="2272" x1="672" />
            <wire x2="720" y1="2272" y2="2272" x1="688" />
        </branch>
        <instance x="288" y="2720" name="XLXI_35" orien="R0" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="272" y="2688" type="branch" />
            <wire x2="288" y1="2688" y2="2688" x1="272" />
        </branch>
        <rect width="824" x="48" y="2116" height="600" />
        <branch name="_Q(3:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1104" y="2224" type="branch" />
            <wire x2="1152" y1="2224" y2="2224" x1="1104" />
        </branch>
        <branch name="XLXN_175">
            <wire x2="1552" y1="2224" y2="2224" x1="1536" />
            <wire x2="1568" y1="2224" y2="2224" x1="1552" />
        </branch>
        <branch name="XLXN_176">
            <wire x2="1552" y1="2288" y2="2288" x1="1536" />
            <wire x2="1568" y1="2288" y2="2288" x1="1552" />
        </branch>
        <branch name="XLXN_177">
            <wire x2="1552" y1="2352" y2="2352" x1="1536" />
            <wire x2="1568" y1="2352" y2="2352" x1="1552" />
        </branch>
        <branch name="XLXN_178">
            <wire x2="1552" y1="2416" y2="2416" x1="1536" />
            <wire x2="1568" y1="2416" y2="2416" x1="1552" />
        </branch>
        <branch name="XLXN_179">
            <wire x2="1552" y1="2480" y2="2480" x1="1536" />
            <wire x2="1568" y1="2480" y2="2480" x1="1552" />
        </branch>
        <branch name="XLXN_180">
            <wire x2="1552" y1="2544" y2="2544" x1="1536" />
            <wire x2="1568" y1="2544" y2="2544" x1="1552" />
        </branch>
        <branch name="XLXN_181">
            <wire x2="1552" y1="2608" y2="2608" x1="1536" />
            <wire x2="1568" y1="2608" y2="2608" x1="1552" />
        </branch>
        <branch name="outA0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1824" y="2224" type="branch" />
            <wire x2="1824" y1="2224" y2="2224" x1="1792" />
        </branch>
        <branch name="outA1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1824" y="2288" type="branch" />
            <wire x2="1824" y1="2288" y2="2288" x1="1792" />
        </branch>
        <branch name="outA2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1824" y="2352" type="branch" />
            <wire x2="1824" y1="2352" y2="2352" x1="1792" />
        </branch>
        <branch name="outA3">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1824" y="2416" type="branch" />
            <wire x2="1808" y1="2416" y2="2416" x1="1792" />
            <wire x2="1824" y1="2416" y2="2416" x1="1808" />
        </branch>
        <branch name="outA4">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1824" y="2480" type="branch" />
            <wire x2="1824" y1="2480" y2="2480" x1="1792" />
        </branch>
        <branch name="outA5">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1824" y="2544" type="branch" />
            <wire x2="1824" y1="2544" y2="2544" x1="1792" />
        </branch>
        <branch name="outA6">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1824" y="2608" type="branch" />
            <wire x2="1824" y1="2608" y2="2608" x1="1792" />
        </branch>
        <instance x="1568" y="2256" name="XLXI_37" orien="R0" />
        <instance x="1568" y="2320" name="XLXI_38" orien="R0" />
        <instance x="1568" y="2384" name="XLXI_39" orien="R0" />
        <instance x="1568" y="2448" name="XLXI_41" orien="R0" />
        <instance x="1568" y="2512" name="XLXI_43" orien="R0" />
        <instance x="1568" y="2576" name="XLXI_44" orien="R0" />
        <instance x="1568" y="2640" name="XLXI_45" orien="R0" />
        <instance x="1152" y="2640" name="XLXI_55" orien="R0">
        </instance>
        <rect width="268" x="24" y="1244" height="360" />
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="672" y="1328" type="branch" />
            <wire x2="688" y1="1328" y2="1328" x1="672" />
        </branch>
        <branch name="an2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1104" y="1456" type="branch" />
            <wire x2="1104" y1="1456" y2="1456" x1="1072" />
        </branch>
        <branch name="an3">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1104" y="1392" type="branch" />
            <wire x2="1104" y1="1392" y2="1392" x1="1072" />
        </branch>
        <branch name="an1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1104" y="1328" type="branch" />
            <wire x2="1104" y1="1328" y2="1328" x1="1072" />
        </branch>
        <branch name="an0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1104" y="1520" type="branch" />
            <wire x2="1104" y1="1520" y2="1520" x1="1072" />
        </branch>
        <branch name="digitalADV">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="656" y="1520" type="branch" />
            <wire x2="688" y1="1520" y2="1520" x1="656" />
        </branch>
        <instance x="688" y="1552" name="XLXI_20" orien="R0">
        </instance>
        <branch name="an3_Out">
            <wire x2="2560" y1="2144" y2="2144" x1="2480" />
            <wire x2="2576" y1="2144" y2="2144" x1="2560" />
        </branch>
        <branch name="an2_Out">
            <wire x2="2544" y1="2048" y2="2048" x1="2496" />
            <wire x2="2560" y1="2048" y2="2048" x1="2544" />
        </branch>
        <branch name="an0_Out">
            <wire x2="2544" y1="1888" y2="1888" x1="2480" />
            <wire x2="2560" y1="1888" y2="1888" x1="2544" />
        </branch>
        <branch name="an1_Out">
            <wire x2="2544" y1="1968" y2="1968" x1="2480" />
            <wire x2="2560" y1="1968" y2="1968" x1="2544" />
        </branch>
        <branch name="an2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2240" y="1968" type="branch" />
            <wire x2="2256" y1="1968" y2="1968" x1="2240" />
        </branch>
        <branch name="an1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2240" y="2048" type="branch" />
            <wire x2="2272" y1="2048" y2="2048" x1="2240" />
        </branch>
        <branch name="an0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2240" y="2144" type="branch" />
            <wire x2="2256" y1="2144" y2="2144" x1="2240" />
        </branch>
        <branch name="an3">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2240" y="1888" type="branch" />
            <wire x2="2256" y1="1888" y2="1888" x1="2240" />
        </branch>
        <instance x="2256" y="2176" name="XLXI_26" orien="R0" />
        <instance x="2272" y="2080" name="XLXI_25" orien="R0" />
        <instance x="2256" y="2000" name="XLXI_24" orien="R0" />
        <instance x="2256" y="1920" name="XLXI_23" orien="R0" />
        <iomarker fontsize="28" x="2576" y="2144" name="an3_Out" orien="R0" />
        <iomarker fontsize="28" x="2560" y="2048" name="an2_Out" orien="R0" />
        <iomarker fontsize="28" x="2560" y="1968" name="an1_Out" orien="R0" />
        <iomarker fontsize="28" x="2560" y="1888" name="an0_Out" orien="R0" />
        <branch name="countOut(7:0)">
            <wire x2="1872" y1="1968" y2="1968" x1="1808" />
            <wire x2="1888" y1="1968" y2="1968" x1="1872" />
        </branch>
        <branch name="countOutAux(3:0)">
            <wire x2="1840" y1="2064" y2="2064" x1="1792" />
            <wire x2="1856" y1="2064" y2="2064" x1="1840" />
        </branch>
        <instance x="1584" y="2000" name="XLXI_58(7:0)" orien="R0" />
        <branch name="Q7,Q6,Q5,Q4,Q3,Q2,Q1,Q0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1552" y="1968" type="branch" />
            <wire x2="1584" y1="1968" y2="1968" x1="1552" />
        </branch>
        <branch name="_Q(3:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1552" y="2064" type="branch" />
            <wire x2="1568" y1="2064" y2="2064" x1="1552" />
        </branch>
        <instance x="1568" y="2096" name="XLXI_59(3:0)" orien="R0" />
        <iomarker fontsize="28" x="1888" y="1968" name="countOut(7:0)" orien="R0" />
        <iomarker fontsize="28" x="1856" y="2064" name="countOutAux(3:0)" orien="R0" />
        <branch name="_outencA">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="1328" type="branch" />
            <wire x2="2288" y1="1328" y2="1328" x1="2256" />
        </branch>
        <branch name="_outencB">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="1520" type="branch" />
            <wire x2="2288" y1="1520" y2="1520" x1="2256" />
        </branch>
        <branch name="an0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1856" y="1328" type="branch" />
            <wire x2="1872" y1="1328" y2="1328" x1="1856" />
        </branch>
        <branch name="an1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1856" y="1392" type="branch" />
            <wire x2="1872" y1="1392" y2="1392" x1="1856" />
        </branch>
        <branch name="an2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1856" y="1456" type="branch" />
            <wire x2="1872" y1="1456" y2="1456" x1="1856" />
        </branch>
        <branch name="an3">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1856" y="1520" type="branch" />
            <wire x2="1872" y1="1520" y2="1520" x1="1856" />
        </branch>
        <instance x="1872" y="1552" name="XLXI_21" orien="R0">
        </instance>
        <rect width="2196" x="392" y="1196" height="436" />
        <text style="fontsize:36;fontname:Arial" x="1792" y="1600">Hex 7 Seg Selector Module</text>
        <rect width="1120" x="932" y="2112" height="604" />
        <text style="fontsize:36;fontname:Arial" x="1260" y="2692">Hex 7 Seg Leftmost Single Digit</text>
        <text style="fontsize:36;fontname:Arial" x="636" y="2684">Level Count</text>
        <text style="fontsize:64;fontname:Arial" x="1268" y="1732">Schematic 4 Score Display</text>
    </sheet>
</drawing>