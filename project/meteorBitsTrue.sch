<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="xPos(9:0)" />
        <signal name="yPos(9:0)" />
        <signal name="_gnd" />
        <signal name="columnCountIn(9:0)" />
        <signal name="drawMeteorEdge" />
        <signal name="drawMeteorCenter" />
        <signal name="rowCountIn(9:0)" />
        <signal name="XLXN_120(15:0)" />
        <signal name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,eOut(9:0)" />
        <signal name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,dOut(9:0)" />
        <signal name="vMinusY(9)" />
        <signal name="eOut(9:0)" />
        <signal name="vMinusY(9:0)" />
        <signal name="uMinusX(9)" />
        <signal name="dOut(9:0)" />
        <signal name="uMinusX(9:0)" />
        <signal name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,yPos(9:0)" />
        <signal name="vMinusY(15:0)" />
        <signal name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,xPos(9:0)" />
        <signal name="uMinusX(15:0)" />
        <signal name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,columnCountIn(9:0)" />
        <signal name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,rowCountIn(9:0)" />
        <port polarity="Input" name="xPos(9:0)" />
        <port polarity="Input" name="yPos(9:0)" />
        <port polarity="Input" name="columnCountIn(9:0)" />
        <port polarity="Output" name="drawMeteorEdge" />
        <port polarity="Output" name="drawMeteorCenter" />
        <port polarity="Input" name="rowCountIn(9:0)" />
        <blockdef name="adsu16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="48" y1="-64" y2="-64" x1="128" />
            <line x2="128" y1="-96" y2="-64" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="sgnChanger">
            <timestamp>2016-5-22T0:4:57</timestamp>
            <rect width="64" x="0" y="84" height="24" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <rect width="64" x="320" y="20" height="24" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="256" x="64" y="-576" height="704" />
        </blockdef>
        <blockdef name="meteorEdgeComparators">
            <timestamp>2016-5-22T1:48:16</timestamp>
            <rect width="352" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="480" y1="-96" y2="-96" x1="416" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
        </blockdef>
        <block symbolname="gnd" name="XLXI_6">
            <blockpin signalname="_gnd" name="G" />
        </block>
        <block symbolname="meteorEdgeComparators" name="XLXI_29">
            <blockpin signalname="XLXN_120(15:0)" name="inputBits(15:0)" />
            <blockpin signalname="drawMeteorCenter" name="meteorCenterTrue" />
            <blockpin signalname="drawMeteorEdge" name="meteorEdgeTrue" />
        </block>
        <block symbolname="add16" name="XLXI_23">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,dOut(9:0)" name="A(15:0)" />
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,eOut(9:0)" name="B(15:0)" />
            <blockpin signalname="_gnd" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_120(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="sgnChanger" name="XLXI_26">
            <blockpin signalname="vMinusY(9)" name="sign" />
            <blockpin signalname="eOut(9:0)" name="d(9:0)" />
            <blockpin signalname="vMinusY(9:0)" name="inBit(9:0)" />
        </block>
        <block symbolname="sgnChanger" name="XLXI_25">
            <blockpin signalname="uMinusX(9)" name="sign" />
            <blockpin signalname="dOut(9:0)" name="d(9:0)" />
            <blockpin signalname="uMinusX(9:0)" name="inBit(9:0)" />
        </block>
        <block symbolname="adsu16" name="XLXI_4">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,rowCountIn(9:0)" name="A(15:0)" />
            <blockpin name="ADD" />
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,yPos(9:0)" name="B(15:0)" />
            <blockpin signalname="_gnd" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="vMinusY(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="adsu16" name="XLXI_3">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,columnCountIn(9:0)" name="A(15:0)" />
            <blockpin name="ADD" />
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,xPos(9:0)" name="B(15:0)" />
            <blockpin signalname="_gnd" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="uMinusX(15:0)" name="S(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="xPos(9:0)">
            <wire x2="240" y1="320" y2="320" x1="208" />
        </branch>
        <branch name="yPos(9:0)">
            <wire x2="240" y1="384" y2="384" x1="208" />
        </branch>
        <iomarker fontsize="28" x="208" y="320" name="xPos(9:0)" orien="R180" />
        <iomarker fontsize="28" x="208" y="384" name="yPos(9:0)" orien="R180" />
        <branch name="columnCountIn(9:0)">
            <wire x2="352" y1="624" y2="624" x1="320" />
        </branch>
        <iomarker fontsize="28" x="320" y="624" name="columnCountIn(9:0)" orien="R180" />
        <branch name="drawMeteorCenter">
            <wire x2="3120" y1="2032" y2="2032" x1="2512" />
            <wire x2="3120" y1="288" y2="2032" x1="3120" />
            <wire x2="3184" y1="288" y2="288" x1="3120" />
        </branch>
        <iomarker fontsize="28" x="3184" y="288" name="drawMeteorCenter" orien="R0" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="336" y="2000" type="branch" />
            <wire x2="336" y1="2000" y2="2000" x1="208" />
        </branch>
        <instance x="144" y="2128" name="XLXI_6" orien="R0" />
        <branch name="rowCountIn(9:0)">
            <wire x2="384" y1="704" y2="704" x1="272" />
        </branch>
        <iomarker fontsize="28" x="272" y="704" name="rowCountIn(9:0)" orien="R180" />
        <instance x="2032" y="2128" name="XLXI_29" orien="R0">
        </instance>
        <branch name="XLXN_120(15:0)">
            <wire x2="2032" y1="2032" y2="2032" x1="1600" />
        </branch>
        <instance x="1152" y="2288" name="XLXI_23" orien="R0" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1120" y="1840" type="branch" />
            <wire x2="1152" y1="1840" y2="1840" x1="1120" />
        </branch>
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,eOut(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1136" y="2096" type="branch" />
            <wire x2="1152" y1="2096" y2="2096" x1="1136" />
        </branch>
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,dOut(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1136" y="1968" type="branch" />
            <wire x2="1152" y1="1968" y2="1968" x1="1136" />
        </branch>
        <rect width="396" x="16" y="44" height="712" />
        <text style="fontsize:36;fontname:Arial" x="112" y="80">Module Inputs</text>
        <rect width="364" x="36" y="216" height="204" />
        <text style="fontsize:36;fontname:Arial" x="140" y="260">X and Y</text>
        <rect width="372" x="28" y="524" height="216" />
        <text style="fontsize:36;fontname:Arial" x="136" y="552">U and V</text>
        <text style="fontsize:36;fontname:Arial" x="64" y="120">&lt; 64</text>
        <branch name="vMinusY(9)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1936" y="1632" type="branch" />
            <wire x2="1968" y1="1632" y2="1632" x1="1936" />
        </branch>
        <branch name="eOut(9:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2368" y="1696" type="branch" />
            <wire x2="2368" y1="1696" y2="1696" x1="2352" />
        </branch>
        <branch name="vMinusY(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1904" y="1760" type="branch" />
            <wire x2="1968" y1="1760" y2="1760" x1="1904" />
        </branch>
        <instance x="1968" y="1664" name="XLXI_26" orien="R0">
        </instance>
        <branch name="uMinusX(9)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1936" y="688" type="branch" />
            <wire x2="1984" y1="688" y2="688" x1="1936" />
        </branch>
        <branch name="dOut(9:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2384" y="752" type="branch" />
            <wire x2="2384" y1="752" y2="752" x1="2368" />
        </branch>
        <instance x="1984" y="720" name="XLXI_25" orien="R0">
        </instance>
        <branch name="uMinusX(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1920" y="816" type="branch" />
            <wire x2="1984" y1="816" y2="816" x1="1920" />
        </branch>
        <rect width="780" x="1780" y="8" height="880" />
        <text style="fontsize:36;fontname:Arial" x="2008" y="44">aboslute value |u-x|</text>
        <text style="fontsize:36;fontname:Arial" x="2000" y="952">aboslute value |v-y|</text>
        <rect width="788" x="1780" y="912" height="908" />
        <rect width="1096" x="588" y="1604" height="692" />
        <text style="fontsize:36;fontname:Arial" x="1024" y="1640">add |u - x|, |v - y|</text>
        <rect width="1188" x="1792" y="1836" height="464" />
        <text style="fontsize:36;fontname:Arial" x="1824" y="1872">assert less than 56, and less than 64 for center, greater than 56 for edge</text>
        <iomarker fontsize="28" x="3168" y="208" name="drawMeteorEdge" orien="R0" />
        <branch name="drawMeteorEdge">
            <wire x2="3072" y1="2096" y2="2096" x1="2512" />
            <wire x2="3168" y1="208" y2="208" x1="3072" />
            <wire x2="3072" y1="208" y2="2096" x1="3072" />
        </branch>
        <rect width="820" x="2696" y="16" height="416" />
        <text style="fontsize:36;fontname:Arial" x="2964" y="64">Module Output</text>
        <rect width="428" x="60" y="1860" height="316" />
        <text style="fontsize:36;fontname:Arial" x="160" y="1908">System Ground</text>
        <rect width="1160" x="564" y="260" height="572" />
        <rect width="1172" x="560" y="1020" height="576" />
        <instance x="976" y="1600" name="XLXI_4" orien="R0" />
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,yPos(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="1408" type="branch" />
            <wire x2="976" y1="1408" y2="1408" x1="944" />
        </branch>
        <branch name="vMinusY(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1456" y="1344" type="branch" />
            <wire x2="1456" y1="1344" y2="1344" x1="1424" />
        </branch>
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="960" y="1152" type="branch" />
            <wire x2="976" y1="1152" y2="1152" x1="960" />
        </branch>
        <instance x="1008" y="864" name="XLXI_3" orien="R0" />
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,xPos(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="672" type="branch" />
            <wire x2="1008" y1="672" y2="672" x1="992" />
        </branch>
        <branch name="uMinusX(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1488" y="608" type="branch" />
            <wire x2="1488" y1="608" y2="608" x1="1456" />
        </branch>
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="416" type="branch" />
            <wire x2="1008" y1="416" y2="416" x1="992" />
        </branch>
        <text style="fontsize:36;fontname:Arial" x="1136" y="292">u - x</text>
        <text style="fontsize:36;fontname:Arial" x="1140" y="1044">v - y</text>
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,columnCountIn(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="544" type="branch" />
            <wire x2="1008" y1="544" y2="544" x1="976" />
        </branch>
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,rowCountIn(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="1280" type="branch" />
            <wire x2="976" y1="1280" y2="1280" x1="944" />
        </branch>
        <text style="fontsize:64;fontname:Arial" x="532" y="60">Schematic 8 Meteor Bits True Module</text>
    </sheet>
</drawing>