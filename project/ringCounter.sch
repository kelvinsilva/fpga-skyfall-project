<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="an1" />
        <signal name="clkin" />
        <signal name="en" />
        <signal name="an3" />
        <signal name="an2" />
        <signal name="XLXN_36" />
        <signal name="an0" />
        <signal name="XLXN_40" />
        <port polarity="Output" name="an1" />
        <port polarity="Input" name="clkin" />
        <port polarity="Input" name="en" />
        <port polarity="Output" name="an3" />
        <port polarity="Output" name="an2" />
        <port polarity="Output" name="an0" />
        <blockdef name="fde">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="fde" name="XLXI_2">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="en" name="CE" />
            <blockpin signalname="XLXN_36" name="D" />
            <blockpin signalname="XLXN_40" name="Q" />
        </block>
        <block symbolname="fde" name="XLXI_6">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="en" name="CE" />
            <blockpin signalname="an0" name="D" />
            <blockpin signalname="an1" name="Q" />
        </block>
        <block symbolname="fde" name="XLXI_7">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="en" name="CE" />
            <blockpin signalname="an1" name="D" />
            <blockpin signalname="an2" name="Q" />
        </block>
        <block symbolname="fde" name="XLXI_8">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="en" name="CE" />
            <blockpin signalname="an2" name="D" />
            <blockpin signalname="an3" name="Q" />
        </block>
        <block symbolname="inv" name="XLXI_16">
            <blockpin signalname="an3" name="I" />
            <blockpin signalname="XLXN_36" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_17">
            <blockpin signalname="XLXN_40" name="I" />
            <blockpin signalname="an0" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="656" y="544" name="XLXI_2" orien="R0" />
        <instance x="656" y="960" name="XLXI_6" orien="R0" />
        <instance x="656" y="1408" name="XLXI_7" orien="R0" />
        <instance x="672" y="1840" name="XLXI_8" orien="R0" />
        <branch name="an1">
            <wire x2="640" y1="992" y2="1152" x1="640" />
            <wire x2="656" y1="1152" y2="1152" x1="640" />
            <wire x2="1056" y1="992" y2="992" x1="640" />
            <wire x2="1056" y1="704" y2="704" x1="1040" />
            <wire x2="1056" y1="704" y2="992" x1="1056" />
            <wire x2="1312" y1="704" y2="704" x1="1056" />
        </branch>
        <branch name="clkin">
            <wire x2="592" y1="416" y2="416" x1="304" />
            <wire x2="656" y1="416" y2="416" x1="592" />
            <wire x2="592" y1="416" y2="832" x1="592" />
            <wire x2="656" y1="832" y2="832" x1="592" />
            <wire x2="592" y1="832" y2="1280" x1="592" />
            <wire x2="656" y1="1280" y2="1280" x1="592" />
            <wire x2="592" y1="1280" y2="1712" x1="592" />
            <wire x2="672" y1="1712" y2="1712" x1="592" />
        </branch>
        <iomarker fontsize="28" x="304" y="416" name="clkin" orien="R180" />
        <branch name="en">
            <wire x2="608" y1="352" y2="352" x1="288" />
            <wire x2="608" y1="352" y2="768" x1="608" />
            <wire x2="656" y1="768" y2="768" x1="608" />
            <wire x2="608" y1="768" y2="1216" x1="608" />
            <wire x2="656" y1="1216" y2="1216" x1="608" />
            <wire x2="608" y1="1216" y2="1648" x1="608" />
            <wire x2="672" y1="1648" y2="1648" x1="608" />
            <wire x2="656" y1="352" y2="352" x1="608" />
        </branch>
        <iomarker fontsize="28" x="288" y="352" name="en" orien="R180" />
        <branch name="an3">
            <wire x2="400" y1="288" y2="288" x1="384" />
            <wire x2="384" y1="288" y2="480" x1="384" />
            <wire x2="496" y1="480" y2="480" x1="384" />
            <wire x2="496" y1="480" y2="1936" x1="496" />
            <wire x2="1136" y1="1936" y2="1936" x1="496" />
            <wire x2="1136" y1="1584" y2="1584" x1="1056" />
            <wire x2="1360" y1="1584" y2="1584" x1="1136" />
            <wire x2="1136" y1="1584" y2="1936" x1="1136" />
        </branch>
        <iomarker fontsize="28" x="1312" y="704" name="an1" orien="R0" />
        <iomarker fontsize="28" x="1472" y="928" name="an2" orien="R0" />
        <iomarker fontsize="28" x="1360" y="1584" name="an3" orien="R0" />
        <branch name="an2">
            <wire x2="624" y1="1472" y2="1584" x1="624" />
            <wire x2="672" y1="1584" y2="1584" x1="624" />
            <wire x2="688" y1="1472" y2="1472" x1="624" />
            <wire x2="1120" y1="1456" y2="1456" x1="688" />
            <wire x2="688" y1="1456" y2="1472" x1="688" />
            <wire x2="1120" y1="1152" y2="1152" x1="1040" />
            <wire x2="1120" y1="1152" y2="1456" x1="1120" />
            <wire x2="1120" y1="928" y2="1152" x1="1120" />
            <wire x2="1472" y1="928" y2="928" x1="1120" />
        </branch>
        <branch name="XLXN_36">
            <wire x2="656" y1="288" y2="288" x1="624" />
        </branch>
        <instance x="400" y="320" name="XLXI_16" orien="R0" />
        <instance x="1056" y="352" name="XLXI_17" orien="R90" />
        <branch name="an0">
            <wire x2="656" y1="576" y2="576" x1="640" />
            <wire x2="656" y1="576" y2="592" x1="656" />
            <wire x2="1088" y1="592" y2="592" x1="656" />
            <wire x2="1408" y1="592" y2="592" x1="1088" />
            <wire x2="640" y1="576" y2="704" x1="640" />
            <wire x2="656" y1="704" y2="704" x1="640" />
            <wire x2="1088" y1="576" y2="592" x1="1088" />
            <wire x2="1408" y1="480" y2="592" x1="1408" />
        </branch>
        <branch name="XLXN_40">
            <wire x2="1088" y1="288" y2="288" x1="1040" />
            <wire x2="1088" y1="288" y2="352" x1="1088" />
        </branch>
        <iomarker fontsize="28" x="1408" y="480" name="an0" orien="R270" />
    </sheet>
</drawing>