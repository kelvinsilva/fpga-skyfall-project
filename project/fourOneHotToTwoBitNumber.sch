<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="s0" />
        <signal name="s1" />
        <signal name="s2" />
        <signal name="s3" />
        <signal name="s0inv" />
        <signal name="s2inv" />
        <signal name="s3inv" />
        <signal name="s1inv" />
        <signal name="XLXN_142" />
        <signal name="XLXN_143" />
        <signal name="XLXN_145" />
        <signal name="XLXN_146" />
        <signal name="GND_" />
        <signal name="ONE_" />
        <signal name="A" />
        <signal name="B" />
        <port polarity="Input" name="s0" />
        <port polarity="Input" name="s1" />
        <port polarity="Input" name="s2" />
        <port polarity="Input" name="s3" />
        <port polarity="Output" name="A" />
        <port polarity="Output" name="B" />
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="or2" name="XLXI_1">
            <blockpin signalname="XLXN_143" name="I0" />
            <blockpin signalname="XLXN_142" name="I1" />
            <blockpin signalname="A" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_2">
            <blockpin signalname="s3inv" name="I0" />
            <blockpin signalname="s2inv" name="I1" />
            <blockpin signalname="s1" name="I2" />
            <blockpin signalname="s0inv" name="I3" />
            <blockpin signalname="XLXN_142" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_3">
            <blockpin signalname="s3inv" name="I0" />
            <blockpin signalname="s2inv" name="I1" />
            <blockpin signalname="s1inv" name="I2" />
            <blockpin signalname="s0" name="I3" />
            <blockpin signalname="XLXN_143" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_17">
            <blockpin signalname="s0" name="I" />
            <blockpin signalname="s0inv" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_18">
            <blockpin signalname="s1" name="I" />
            <blockpin signalname="s1inv" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_19">
            <blockpin signalname="s2" name="I" />
            <blockpin signalname="s2inv" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_20">
            <blockpin signalname="s3" name="I" />
            <blockpin signalname="s3inv" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_8">
            <blockpin signalname="XLXN_146" name="I0" />
            <blockpin signalname="XLXN_145" name="I1" />
            <blockpin signalname="B" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_13">
            <blockpin signalname="s3inv" name="I0" />
            <blockpin signalname="s2" name="I1" />
            <blockpin signalname="s1inv" name="I2" />
            <blockpin signalname="s0inv" name="I3" />
            <blockpin signalname="XLXN_145" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_14">
            <blockpin signalname="s3inv" name="I0" />
            <blockpin signalname="s2inv" name="I1" />
            <blockpin signalname="s1inv" name="I2" />
            <blockpin signalname="s0" name="I3" />
            <blockpin signalname="XLXN_146" name="O" />
        </block>
        <block symbolname="gnd" name="XLXI_24">
            <blockpin signalname="GND_" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_25">
            <blockpin signalname="ONE_" name="P" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="s0">
            <wire x2="496" y1="208" y2="208" x1="304" />
        </branch>
        <branch name="s1">
            <wire x2="496" y1="304" y2="304" x1="304" />
        </branch>
        <branch name="s2">
            <wire x2="496" y1="384" y2="384" x1="304" />
        </branch>
        <branch name="s3">
            <wire x2="496" y1="464" y2="464" x1="304" />
        </branch>
        <instance x="1824" y="512" name="XLXI_1" orien="R0" />
        <branch name="s0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="752" y="224" type="branch" />
            <wire x2="960" y1="224" y2="224" x1="752" />
        </branch>
        <instance x="1536" y="448" name="XLXI_2" orien="R0" />
        <instance x="1536" y="720" name="XLXI_3" orien="R0" />
        <branch name="s0inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="192" type="branch" />
            <wire x2="1536" y1="192" y2="192" x1="1520" />
        </branch>
        <branch name="s2inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="320" type="branch" />
            <wire x2="1536" y1="320" y2="320" x1="1520" />
        </branch>
        <branch name="s3inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="384" type="branch" />
            <wire x2="1536" y1="384" y2="384" x1="1520" />
        </branch>
        <branch name="s0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="464" type="branch" />
            <wire x2="1536" y1="464" y2="464" x1="1520" />
        </branch>
        <branch name="s1inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="528" type="branch" />
            <wire x2="1536" y1="528" y2="528" x1="1520" />
        </branch>
        <branch name="s2inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="592" type="branch" />
            <wire x2="1536" y1="592" y2="592" x1="1520" />
        </branch>
        <branch name="s3inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="656" type="branch" />
            <wire x2="1536" y1="656" y2="656" x1="1520" />
        </branch>
        <instance x="960" y="256" name="XLXI_17" orien="R0" />
        <branch name="s1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="752" y="304" type="branch" />
            <wire x2="960" y1="304" y2="304" x1="752" />
        </branch>
        <instance x="960" y="336" name="XLXI_18" orien="R0" />
        <instance x="960" y="432" name="XLXI_19" orien="R0" />
        <branch name="s2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="768" y="400" type="branch" />
            <wire x2="960" y1="400" y2="400" x1="768" />
        </branch>
        <branch name="s3">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="768" y="528" type="branch" />
            <wire x2="784" y1="528" y2="528" x1="768" />
            <wire x2="976" y1="528" y2="528" x1="784" />
        </branch>
        <instance x="976" y="560" name="XLXI_20" orien="R0" />
        <branch name="s0inv">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="224" type="branch" />
            <wire x2="1200" y1="224" y2="224" x1="1184" />
        </branch>
        <branch name="s1inv">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="304" type="branch" />
            <wire x2="1200" y1="304" y2="304" x1="1184" />
        </branch>
        <branch name="s2inv">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1216" y="400" type="branch" />
            <wire x2="1216" y1="400" y2="400" x1="1184" />
        </branch>
        <branch name="s3inv">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1232" y="528" type="branch" />
            <wire x2="1232" y1="528" y2="528" x1="1200" />
        </branch>
        <rect width="612" x="676" y="140" height="564" />
        <instance x="3024" y="512" name="XLXI_8" orien="R0" />
        <instance x="2672" y="400" name="XLXI_13" orien="R0" />
        <instance x="2672" y="688" name="XLXI_14" orien="R0" />
        <branch name="s0inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2656" y="144" type="branch" />
            <wire x2="2672" y1="144" y2="144" x1="2656" />
        </branch>
        <branch name="s1inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2656" y="208" type="branch" />
            <wire x2="2672" y1="208" y2="208" x1="2656" />
        </branch>
        <branch name="s2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2656" y="272" type="branch" />
            <wire x2="2672" y1="272" y2="272" x1="2656" />
        </branch>
        <branch name="s3inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2656" y="336" type="branch" />
            <wire x2="2672" y1="336" y2="336" x1="2656" />
        </branch>
        <branch name="s0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2656" y="432" type="branch" />
            <wire x2="2672" y1="432" y2="432" x1="2656" />
        </branch>
        <branch name="s1inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2656" y="496" type="branch" />
            <wire x2="2672" y1="496" y2="496" x1="2656" />
        </branch>
        <branch name="s2inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2656" y="560" type="branch" />
            <wire x2="2672" y1="560" y2="560" x1="2656" />
        </branch>
        <branch name="s3inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2656" y="624" type="branch" />
            <wire x2="2672" y1="624" y2="624" x1="2656" />
        </branch>
        <branch name="XLXN_142">
            <wire x2="1808" y1="288" y2="288" x1="1792" />
            <wire x2="1808" y1="288" y2="384" x1="1808" />
            <wire x2="1824" y1="384" y2="384" x1="1808" />
        </branch>
        <branch name="XLXN_143">
            <wire x2="1808" y1="560" y2="560" x1="1792" />
            <wire x2="1808" y1="448" y2="560" x1="1808" />
            <wire x2="1824" y1="448" y2="448" x1="1808" />
        </branch>
        <branch name="XLXN_145">
            <wire x2="2976" y1="240" y2="240" x1="2928" />
            <wire x2="2976" y1="240" y2="384" x1="2976" />
            <wire x2="3024" y1="384" y2="384" x1="2976" />
        </branch>
        <branch name="XLXN_146">
            <wire x2="2976" y1="528" y2="528" x1="2928" />
            <wire x2="2976" y1="448" y2="528" x1="2976" />
            <wire x2="3024" y1="448" y2="448" x1="2976" />
        </branch>
        <rect width="772" x="1388" y="128" height="604" />
        <rect width="904" x="2460" y="116" height="608" />
        <rect width="452" x="152" y="168" height="648" />
        <branch name="GND_">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="560" y="672" type="branch" />
            <wire x2="560" y1="672" y2="672" x1="432" />
        </branch>
        <instance x="368" y="800" name="XLXI_24" orien="R0" />
        <instance x="208" y="672" name="XLXI_25" orien="R0" />
        <branch name="ONE_">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="272" y="752" type="branch" />
            <wire x2="272" y1="672" y2="752" x1="272" />
        </branch>
        <text style="fontsize:28;fontname:Arial" x="1784" y="192">encoder</text>
        <text style="fontsize:28;fontname:Arial" x="2976" y="160">encoder</text>
        <iomarker fontsize="28" x="304" y="208" name="s0" orien="R180" />
        <iomarker fontsize="28" x="304" y="304" name="s1" orien="R180" />
        <iomarker fontsize="28" x="304" y="384" name="s2" orien="R180" />
        <iomarker fontsize="28" x="304" y="464" name="s3" orien="R180" />
        <branch name="s1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="256" type="branch" />
            <wire x2="1536" y1="256" y2="256" x1="1520" />
        </branch>
        <branch name="A">
            <wire x2="2096" y1="416" y2="416" x1="2080" />
        </branch>
        <branch name="B">
            <wire x2="3312" y1="416" y2="416" x1="3280" />
        </branch>
        <iomarker fontsize="28" x="2096" y="416" name="A" orien="R0" />
        <iomarker fontsize="28" x="3312" y="416" name="B" orien="R0" />
    </sheet>
</drawing>