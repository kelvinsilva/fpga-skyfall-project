// VERILOG 3 TESTBENCH VERILOG CODE FOR GAME FSM
// Verilog test fixture created from schematic C:\Users\kelvi\OneDrive\Documents\school files\spring 2016\ce 100\lab 7\workingDir\final1\lab7progress7\lab7progress5\gamStateMachine.sch - Tue May 31 20:07:51 2016

`timescale 1ns / 1ps

module gamStateMachine_gamStateMachine_sch_tb();

// Inputs
   reg collisionHappenedIn;
   reg diamondCountUpIn;
   reg diamondLevelUpIn;
   reg btn0In;
   reg btn1In;
   reg btn2In;
   reg fourSecSigIn;
   reg levelsMatchIn;
   reg clkin;

// Output
   wire slugIncSizeOut;
   wire slugAnimationOnOffOut;
   wire leftRightSlugOut;
   wire slugFlashAnimOnOut;
   wire diamondAnimOnOffOut;
   wire incScoreOut;
   wire incLevelOut;
   wire resetFourSecTimerOut;
   wire gameWinSigOut;
   wire slugMagentaOut;
   wire gameOverOut;
   wire resetScoreOut;

// Bidirs

// Instantiate the UUT
   gamStateMachine UUT (
		.collisionHappenedIn(collisionHappenedIn), 
		.diamondCountUpIn(diamondCountUpIn), 
		.diamondLevelUpIn(diamondLevelUpIn), 
		.btn0In(btn0In), 
		.btn1In(btn1In), 
		.btn2In(btn2In), 
		.fourSecSigIn(fourSecSigIn), 
		.levelsMatchIn(levelsMatchIn), 
		.slugIncSizeOut(slugIncSizeOut), 
		.slugAnimationOnOffOut(slugAnimationOnOffOut), 
		.leftRightSlugOut(leftRightSlugOut), 
		.slugFlashAnimOnOut(slugFlashAnimOnOut), 
		.diamondAnimOnOffOut(diamondAnimOnOffOut), 
		.incScoreOut(incScoreOut), 
		.incLevelOut(incLevelOut), 
		.resetFourSecTimerOut(resetFourSecTimerOut), 
		.gameWinSigOut(gameWinSigOut), 
		.slugMagentaOut(slugMagentaOut), 
		.gameOverOut(gameOverOut), 
		.clkin(clkin), 
		.resetScoreOut(resetScoreOut)
   );
// Initialize Inputs
   `ifdef auto_init
       initial begin
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
   `endif
	
	 parameter PERIOD = 100;
    parameter real DUTY_CYCLE = 0.5;
    parameter OFFSET = 20;

         // If your input labels are different
         // substitute your labels for eclk, pb0 and pb2 as required.

	initial    // Clock process for eclk
	begin

        #OFFSET
		clkin = 1'b1;
        forever
         begin
            #(PERIOD-(PERIOD*DUTY_CYCLE)) clkin = ~clkin;
         end
	end
	
	initial
	begin
         
	 // Add your stimuli here.
	 // To set signal foo to value 0 use
	 // foo = 1'b0;
	 // To set signal foo to value 1 use
	 // foo = 1'b1;
	 // Advance time by multiples of 100ns.
	 // To advance time by 100ns use the following line
	 // 100ns;
	 
	 
	 
		#100;	//GameInitial
	 	
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100;	//Go to GamePlayStart
	 
	 	collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 1;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //Maintain GamePlayStart
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //go to switchSlug
			
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 1;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //go back to GamePlayStart
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //go to countUp 
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 1;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100;	//go to gamePlayStart
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //go to level up
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 1;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //go to animation
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //stay in animation
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //go to reset timer
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 1;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //go to incSize
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //go to gamePlayStart
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 1;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //go to gameOver
		
		collisionHappenedIn = 1;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //from gameover to gameInitial
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 1;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;	

		#100; //from game initial to gamePlayStart
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 1;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		#100; //from gamePlayStart to win
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 0;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 1;
		clkin = 0;
		
		#100; //from win to gameInitial
		
		collisionHappenedIn = 0;
		diamondCountUpIn = 0;
		diamondLevelUpIn = 0;
		btn0In = 0;
		btn1In = 1;
		btn2In = 0;
		fourSecSigIn = 0;
		levelsMatchIn = 0;
		clkin = 0;
		
		//all states have been tested.
		
		
		

    end		
	
endmodule
