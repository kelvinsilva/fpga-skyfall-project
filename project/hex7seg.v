`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// VERILOG 1 HEX 7 SEG, SCORE DISPLAY DEPENDENCY
// Engineer: Kelvin Silva
// 
// Create Date:    23:22:09 04/25/2016 
// Design Name: 
// Module Name:    hex7seg 
// Project Name: lab 4
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module hex7seg(
    input [3:0] inNum,
    output outA,
    output outB,
    output outC,
    output outD,
    output outE,
    output outF,
    output outG
    );
	 
	 //specify 16 minterms
	 wire mt0;
	 assign mt0 = ((~inNum[0]) & (~inNum[1]) & (~inNum[2]) &(~inNum[3]));
	 
	 wire mt1;
	 assign mt1 = ((inNum[0]) & (~inNum[1]) & (~inNum[2]) & (~inNum[3]));
	 
	 wire mt2;
	 assign mt2 = ((~inNum[0]) & (inNum[1]) & (~inNum[2]) & (~inNum[3]));
	 
	 wire mt3;
	 assign mt3 = ((inNum[0]) & (inNum[1]) & (~inNum[2]) & (~inNum[3]));
	 
	 wire mt4;
	 assign mt4 = ((~inNum[0]) & (~inNum[1]) & (inNum[2]) & (~inNum[3]));
	 
	 wire mt5;
	 assign mt5 = ((inNum[0]) & (~inNum[1]) & (inNum[2]) & (~inNum[3]));
	 
	 wire mt6;
	 assign mt6 = ((~inNum[0]) & (inNum[1]) & (inNum[2]) & (~inNum[3]));

	 wire mt7;
	 assign mt7 = ((inNum[0]) & (inNum[1]) & (inNum[2]) & (~inNum[3]));
	 
	 wire mt8;
	 assign mt8 = ((~inNum[0]) & (~inNum[1]) & (~inNum[2]) & (inNum[3]));
	 
	 wire mt9;
	 assign mt9 = ((inNum[0]) & (~inNum[1]) & (~inNum[2]) & (inNum[3]));
	 
	 wire mt10;
	 assign mt10 = ((~inNum[0]) & (inNum[1]) & (~inNum[2]) & (inNum[3]));
	 
	 wire mt11;
	 assign mt11 = ((inNum[0]) & (inNum[1]) & (~inNum[2]) & (inNum[3]));
	 
	 wire mt12;
	 assign mt12 = ((~inNum[0]) & (~inNum[1]) & (inNum[2]) & (inNum[3]));
	 
	 wire mt13;
	 assign mt13 = ((inNum[0]) & (~inNum[1]) & (inNum[2]) & (inNum[3]));
	 
	 wire mt14;
	 assign mt14 = ((~inNum[0]) & (inNum[1]) & (inNum[2]) & (inNum[3]));
	 
	 wire mt15;
	 assign mt15 = ((inNum[0]) & (inNum[1]) & (inNum[2]) & (inNum[3]));
	 
	 assign outA = (mt1 | mt4 | mt11 | mt13);
	 assign outB = (mt5 | mt6 | mt11 | mt12 | mt14 | mt15);
	 assign outC = (mt2 | mt12 | mt14 | mt15);
	 assign outD = (mt1 | mt4 | mt7 | mt9 | mt10 | mt15);
	 assign outE = (mt1 | mt3 | mt4 | mt5 | mt7 | mt9);
	 assign outF = (mt1 | mt2 | mt3 | mt7 | mt13);
	 assign outG = (mt0 | mt1 | mt7 | mt12);
	 
	 
	 
	 
	 
endmodule
