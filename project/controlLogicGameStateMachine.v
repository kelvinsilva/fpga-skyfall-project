`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// VERILOG 4 FSM CONTROL LOGIC
// Engineer: 
// 
// Create Date:    18:56:29 05/23/2016 
// Design Name: 
// Module Name:    controlLogicGameStateMachine 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module controlLogicGameStateMachine(
    input btn0In,
    input btn1In,
    input btn2In,
    input fourSecSigIn,
    input collisionHappenedIn,
    input diamondCountUpIn,
    input diamondLevelUpIn,
    input levelsMatchIn,
	 
    output slugIncSizeOut,
    output slugAnimationOnOffOut,
    output leftRightSlugOut,
    output slugFlashAnimOnOut,
    output diamondAnimOnOffOut,
    output incScoreOut,
    output incLevelOut,
    output resetFourSecondTimerOut,
    output gameWinSigOut,
    output slugMagentaOut,
    output gameOverOut,
	 output resetScoreOut,
	 
    output [9:0] NS,
    input [9:0] PS
    );
	 
	 wire gameInitial, gamePlayStart, switchSlug, countUp, incSize, levelUp, animation, resetTimer, gameOver, win;
	 wire nextGameInitial, nextGamePlayStart, nextSwitchSlug, nextCountUp, nextIncSize, nextLevelUp, nextAnimation, nextResetTimer, nextGameOver, nextWin;
	 
	 assign gameInitial = PS[0];
	 assign gamePlayStart = PS[1];
	 assign switchSlug = PS[2];
	 assign countUp = PS[3];
	 assign incSize = PS[4];
	 assign levelUp = PS[5];
	 assign animation = PS[6];
	 assign resetTimer = PS[7];
	 assign gameOver = PS[8];
	 assign win = PS[9];
	 
	 assign NS[0] = nextGameInitial;
	 assign NS[1] = nextGamePlayStart;
	 assign NS[2] = nextSwitchSlug;
	 assign NS[3] = nextCountUp;
	 assign NS[4] = nextIncSize;
	 assign NS[5] = nextLevelUp;
	 assign NS[6] = nextAnimation;
	 assign NS[7] = nextResetTimer;
	 assign NS[8] = nextGameOver;
	 assign NS[9] = nextWin;
	 
	 assign nextGameInitial = (gameInitial & ~btn0In) | (win & btn1In) | (gameOver & btn1In);
	 assign nextGamePlayStart = (gameInitial & btn0In) | (gamePlayStart & ~btn2In & ~diamondCountUpIn & ~diamondLevelUpIn & ~collisionHappenedIn & ~levelsMatchIn) | (switchSlug) | (countUp) | (incSize & fourSecSigIn);
	 assign nextSwitchSlug = (gamePlayStart & btn2In) | (countUp & btn2In);
	 assign nextCountUp = (gamePlayStart & diamondCountUpIn);
	 assign nextLevelUp = (gamePlayStart & diamondLevelUpIn);
	 assign nextAnimation = (levelUp) | (animation & ~fourSecSigIn);
	 assign nextResetTimer = animation & fourSecSigIn;
	 assign nextIncSize = (resetTimer) | (incSize & ~fourSecSigIn);
	 assign nextGameOver = (gamePlayStart & collisionHappenedIn) | (gameOver & ~ btn1In);
	 assign nextWin = (gamePlayStart & levelsMatchIn) | (win & ~btn1In);
	 
	 assign slugIncSizeOut = resetTimer;
	 assign slugAnimationOnOffOut = (~win);
	 assign leftRightSlugOut = switchSlug;
	 assign slugFlashAnimOnOut = animation | resetTimer | incSize | gameOver;
	 assign diamondAnimOnOffOut = gamePlayStart | switchSlug | countUp;
	 assign incScoreOut = countUp;
	 assign incLevelOut = levelUp;
	 assign resetFourSecondTimerOut = levelUp | resetTimer;
	 assign gameWinSigOut = win;
	 assign slugMagentaOut = animation;
	 assign gameOverOut = gameOver;
	 assign resetScoreOut = gameInitial;
	 

endmodule
