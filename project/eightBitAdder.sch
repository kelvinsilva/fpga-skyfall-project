<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CIN" />
        <signal name="n0" />
        <signal name="n1" />
        <signal name="n2" />
        <signal name="n3" />
        <signal name="n4" />
        <signal name="n5" />
        <signal name="n6" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="XLXN_21" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="n8" />
        <signal name="b0" />
        <signal name="a0" />
        <signal name="b1" />
        <signal name="b2" />
        <signal name="a2" />
        <signal name="b3" />
        <signal name="a3" />
        <signal name="b4" />
        <signal name="a4" />
        <signal name="b5" />
        <signal name="a5" />
        <signal name="b6" />
        <signal name="a6" />
        <signal name="b7" />
        <signal name="a7" />
        <signal name="a1" />
        <signal name="n7" />
        <port polarity="Input" name="CIN" />
        <port polarity="Output" name="n0" />
        <port polarity="Output" name="n1" />
        <port polarity="Output" name="n2" />
        <port polarity="Output" name="n3" />
        <port polarity="Output" name="n4" />
        <port polarity="Output" name="n5" />
        <port polarity="Output" name="n6" />
        <port polarity="Output" name="n8" />
        <port polarity="Input" name="b0" />
        <port polarity="Input" name="a0" />
        <port polarity="Input" name="b1" />
        <port polarity="Input" name="b2" />
        <port polarity="Input" name="a2" />
        <port polarity="Input" name="b3" />
        <port polarity="Input" name="a3" />
        <port polarity="Input" name="b4" />
        <port polarity="Input" name="a4" />
        <port polarity="Input" name="b5" />
        <port polarity="Input" name="a5" />
        <port polarity="Input" name="b6" />
        <port polarity="Input" name="a6" />
        <port polarity="Input" name="b7" />
        <port polarity="Input" name="a7" />
        <port polarity="Input" name="a1" />
        <port polarity="Output" name="n7" />
        <blockdef name="fullAdder">
            <timestamp>2016-5-21T22:29:34</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="fullAdder" name="XLXI_1">
            <blockpin signalname="a0" name="a" />
            <blockpin signalname="b0" name="b" />
            <blockpin signalname="CIN" name="Cin" />
            <blockpin signalname="n0" name="S" />
            <blockpin signalname="XLXN_17" name="Cout" />
        </block>
        <block symbolname="fullAdder" name="XLXI_2">
            <blockpin signalname="a1" name="a" />
            <blockpin signalname="b1" name="b" />
            <blockpin signalname="XLXN_17" name="Cin" />
            <blockpin signalname="n1" name="S" />
            <blockpin signalname="XLXN_18" name="Cout" />
        </block>
        <block symbolname="fullAdder" name="XLXI_3">
            <blockpin signalname="a2" name="a" />
            <blockpin signalname="b2" name="b" />
            <blockpin signalname="XLXN_18" name="Cin" />
            <blockpin signalname="n2" name="S" />
            <blockpin signalname="XLXN_19" name="Cout" />
        </block>
        <block symbolname="fullAdder" name="XLXI_4">
            <blockpin signalname="a3" name="a" />
            <blockpin signalname="b3" name="b" />
            <blockpin signalname="XLXN_19" name="Cin" />
            <blockpin signalname="n3" name="S" />
            <blockpin signalname="XLXN_20" name="Cout" />
        </block>
        <block symbolname="fullAdder" name="XLXI_5">
            <blockpin signalname="a4" name="a" />
            <blockpin signalname="b4" name="b" />
            <blockpin signalname="XLXN_20" name="Cin" />
            <blockpin signalname="n4" name="S" />
            <blockpin signalname="XLXN_21" name="Cout" />
        </block>
        <block symbolname="fullAdder" name="XLXI_6">
            <blockpin signalname="a5" name="a" />
            <blockpin signalname="b5" name="b" />
            <blockpin signalname="XLXN_21" name="Cin" />
            <blockpin signalname="n5" name="S" />
            <blockpin signalname="XLXN_22" name="Cout" />
        </block>
        <block symbolname="fullAdder" name="XLXI_7">
            <blockpin signalname="a6" name="a" />
            <blockpin signalname="b6" name="b" />
            <blockpin signalname="XLXN_22" name="Cin" />
            <blockpin signalname="n6" name="S" />
            <blockpin signalname="XLXN_23" name="Cout" />
        </block>
        <block symbolname="fullAdder" name="XLXI_8">
            <blockpin signalname="a7" name="a" />
            <blockpin signalname="b7" name="b" />
            <blockpin signalname="XLXN_23" name="Cin" />
            <blockpin signalname="n7" name="S" />
            <blockpin signalname="n8" name="Cout" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="480" y="624" name="XLXI_1" orien="R90">
        </instance>
        <instance x="944" y="640" name="XLXI_2" orien="R90">
        </instance>
        <instance x="1360" y="656" name="XLXI_3" orien="R90">
        </instance>
        <instance x="1696" y="656" name="XLXI_4" orien="R90">
        </instance>
        <instance x="2112" y="672" name="XLXI_5" orien="R90">
        </instance>
        <instance x="2432" y="672" name="XLXI_6" orien="R90">
        </instance>
        <instance x="2784" y="672" name="XLXI_7" orien="R90">
        </instance>
        <instance x="3136" y="672" name="XLXI_8" orien="R90">
        </instance>
        <branch name="CIN">
            <wire x2="512" y1="592" y2="624" x1="512" />
        </branch>
        <iomarker fontsize="28" x="512" y="592" name="CIN" orien="R270" />
        <branch name="n0">
            <wire x2="640" y1="1024" y2="1024" x1="560" />
            <wire x2="560" y1="1024" y2="1104" x1="560" />
            <wire x2="640" y1="1104" y2="1104" x1="560" />
            <wire x2="640" y1="1008" y2="1024" x1="640" />
        </branch>
        <iomarker fontsize="28" x="640" y="1104" name="n0" orien="R0" />
        <branch name="n1">
            <wire x2="1104" y1="1024" y2="1056" x1="1104" />
        </branch>
        <iomarker fontsize="28" x="1104" y="1056" name="n1" orien="R90" />
        <branch name="n2">
            <wire x2="1520" y1="1040" y2="1072" x1="1520" />
        </branch>
        <iomarker fontsize="28" x="1520" y="1072" name="n2" orien="R90" />
        <branch name="n3">
            <wire x2="1856" y1="1040" y2="1072" x1="1856" />
        </branch>
        <iomarker fontsize="28" x="1856" y="1072" name="n3" orien="R90" />
        <branch name="n4">
            <wire x2="2272" y1="1056" y2="1088" x1="2272" />
        </branch>
        <iomarker fontsize="28" x="2272" y="1088" name="n4" orien="R90" />
        <branch name="n5">
            <wire x2="2592" y1="1056" y2="1088" x1="2592" />
        </branch>
        <iomarker fontsize="28" x="2592" y="1088" name="n5" orien="R90" />
        <branch name="n6">
            <wire x2="2944" y1="1056" y2="1072" x1="2944" />
            <wire x2="3024" y1="1072" y2="1072" x1="2944" />
            <wire x2="3024" y1="1072" y2="1136" x1="3024" />
            <wire x2="3024" y1="1136" y2="1136" x1="2944" />
        </branch>
        <iomarker fontsize="28" x="2944" y="1136" name="n6" orien="R180" />
        <branch name="XLXN_17">
            <wire x2="416" y1="432" y2="1072" x1="416" />
            <wire x2="512" y1="1072" y2="1072" x1="416" />
            <wire x2="976" y1="432" y2="432" x1="416" />
            <wire x2="976" y1="432" y2="640" x1="976" />
            <wire x2="512" y1="1008" y2="1072" x1="512" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="976" y1="1024" y2="1040" x1="976" />
            <wire x2="1200" y1="1040" y2="1040" x1="976" />
            <wire x2="1200" y1="592" y2="1040" x1="1200" />
            <wire x2="1392" y1="592" y2="592" x1="1200" />
            <wire x2="1392" y1="592" y2="656" x1="1392" />
        </branch>
        <branch name="XLXN_19">
            <wire x2="1296" y1="608" y2="1120" x1="1296" />
            <wire x2="1392" y1="1120" y2="1120" x1="1296" />
            <wire x2="1728" y1="608" y2="608" x1="1296" />
            <wire x2="1728" y1="608" y2="656" x1="1728" />
            <wire x2="1392" y1="1040" y2="1120" x1="1392" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="1632" y1="576" y2="1104" x1="1632" />
            <wire x2="1728" y1="1104" y2="1104" x1="1632" />
            <wire x2="2144" y1="576" y2="576" x1="1632" />
            <wire x2="2144" y1="576" y2="672" x1="2144" />
            <wire x2="1728" y1="1040" y2="1104" x1="1728" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="2144" y1="1056" y2="1072" x1="2144" />
            <wire x2="2384" y1="1072" y2="1072" x1="2144" />
            <wire x2="2384" y1="608" y2="1072" x1="2384" />
            <wire x2="2464" y1="608" y2="608" x1="2384" />
            <wire x2="2464" y1="608" y2="672" x1="2464" />
        </branch>
        <branch name="XLXN_22">
            <wire x2="2464" y1="1056" y2="1072" x1="2464" />
            <wire x2="2688" y1="1072" y2="1072" x1="2464" />
            <wire x2="2688" y1="608" y2="1072" x1="2688" />
            <wire x2="2816" y1="608" y2="608" x1="2688" />
            <wire x2="2816" y1="608" y2="672" x1="2816" />
        </branch>
        <branch name="XLXN_23">
            <wire x2="2720" y1="624" y2="1072" x1="2720" />
            <wire x2="2816" y1="1072" y2="1072" x1="2720" />
            <wire x2="3168" y1="624" y2="624" x1="2720" />
            <wire x2="3168" y1="624" y2="672" x1="3168" />
            <wire x2="2816" y1="1056" y2="1072" x1="2816" />
        </branch>
        <branch name="n8">
            <wire x2="3168" y1="1056" y2="1088" x1="3168" />
        </branch>
        <branch name="n7">
            <wire x2="3296" y1="1056" y2="1088" x1="3296" />
        </branch>
        <branch name="b0">
            <wire x2="576" y1="592" y2="624" x1="576" />
        </branch>
        <iomarker fontsize="28" x="576" y="592" name="b0" orien="R270" />
        <branch name="a0">
            <wire x2="640" y1="592" y2="624" x1="640" />
        </branch>
        <iomarker fontsize="28" x="640" y="592" name="a0" orien="R270" />
        <branch name="b1">
            <wire x2="1040" y1="608" y2="640" x1="1040" />
        </branch>
        <iomarker fontsize="28" x="1040" y="608" name="b1" orien="R270" />
        <branch name="a1">
            <wire x2="1104" y1="608" y2="640" x1="1104" />
        </branch>
        <iomarker fontsize="28" x="1104" y="608" name="a1" orien="R270" />
        <branch name="b2">
            <wire x2="1456" y1="624" y2="656" x1="1456" />
        </branch>
        <iomarker fontsize="28" x="1456" y="624" name="b2" orien="R270" />
        <branch name="a2">
            <wire x2="1520" y1="624" y2="656" x1="1520" />
        </branch>
        <iomarker fontsize="28" x="1520" y="624" name="a2" orien="R270" />
        <branch name="b3">
            <wire x2="1792" y1="624" y2="656" x1="1792" />
        </branch>
        <iomarker fontsize="28" x="1792" y="624" name="b3" orien="R270" />
        <branch name="a3">
            <wire x2="1856" y1="624" y2="656" x1="1856" />
        </branch>
        <iomarker fontsize="28" x="1856" y="624" name="a3" orien="R270" />
        <branch name="b4">
            <wire x2="2208" y1="640" y2="672" x1="2208" />
        </branch>
        <iomarker fontsize="28" x="2208" y="640" name="b4" orien="R270" />
        <branch name="a4">
            <wire x2="2272" y1="640" y2="672" x1="2272" />
        </branch>
        <iomarker fontsize="28" x="2272" y="640" name="a4" orien="R270" />
        <branch name="b5">
            <wire x2="2528" y1="640" y2="672" x1="2528" />
        </branch>
        <iomarker fontsize="28" x="2528" y="640" name="b5" orien="R270" />
        <branch name="a5">
            <wire x2="2592" y1="640" y2="672" x1="2592" />
        </branch>
        <iomarker fontsize="28" x="2592" y="640" name="a5" orien="R270" />
        <branch name="b6">
            <wire x2="2880" y1="640" y2="672" x1="2880" />
        </branch>
        <iomarker fontsize="28" x="2880" y="640" name="b6" orien="R270" />
        <branch name="a6">
            <wire x2="2944" y1="640" y2="672" x1="2944" />
        </branch>
        <iomarker fontsize="28" x="2944" y="640" name="a6" orien="R270" />
        <branch name="b7">
            <wire x2="3232" y1="640" y2="672" x1="3232" />
        </branch>
        <iomarker fontsize="28" x="3232" y="640" name="b7" orien="R270" />
        <branch name="a7">
            <wire x2="3296" y1="640" y2="672" x1="3296" />
        </branch>
        <iomarker fontsize="28" x="3296" y="640" name="a7" orien="R270" />
        <iomarker fontsize="28" x="3168" y="1088" name="n8" orien="R90" />
        <iomarker fontsize="28" x="3296" y="1088" name="n7" orien="R90" />
    </sheet>
</drawing>