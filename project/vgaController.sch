<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="hsOut" />
        <signal name="vsOut" />
        <signal name="l_H" />
        <signal name="g_H" />
        <signal name="_gnd" />
        <signal name="_vcc" />
        <signal name="hsCount(9:0)" />
        <signal name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,hsCount(9:0)" />
        <signal name="g_H1" />
        <signal name="l_H1" />
        <signal name="g_V" />
        <signal name="l_V" />
        <signal name="vsCount(9:0)" />
        <signal name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,vsCount(9:0)" />
        <signal name="l_V2" />
        <signal name="g_V2" />
        <signal name="isActive" />
        <signal name="columnCount(9:0)" />
        <signal name="a_L" />
        <signal name="a_L0" />
        <signal name="rowCount(9:0)" />
        <signal name="clkin" />
        <signal name="XLXN_98(15:0)" />
        <signal name="XLXN_99(15:0)" />
        <signal name="outSigCompVS" />
        <signal name="XLXN_163" />
        <signal name="XLXN_164" />
        <signal name="XLXN_165" />
        <signal name="XLXN_166" />
        <signal name="XLXN_167" />
        <signal name="XLXN_168" />
        <signal name="XLXN_169" />
        <signal name="XLXN_63(15:0)" />
        <signal name="XLXN_65(15:0)" />
        <signal name="XLXN_55" />
        <signal name="XLXN_113" />
        <signal name="XLXN_142" />
        <signal name="XLXN_143" />
        <signal name="XLXN_148" />
        <signal name="XLXN_149" />
        <signal name="XLXN_150" />
        <signal name="XLXN_151" />
        <signal name="XLXN_3(15:0)" />
        <signal name="XLXN_5(15:0)" />
        <signal name="XLXN_256(15:0)" />
        <signal name="XLXN_257" />
        <signal name="XLXN_258" />
        <signal name="XLXN_259" />
        <signal name="XLXN_260" />
        <signal name="resetOn800" />
        <signal name="XLXN_264(15:0)" />
        <signal name="XLXN_265" />
        <signal name="XLXN_266" />
        <signal name="XLXN_267" />
        <signal name="XLXN_268" />
        <signal name="resetOn525" />
        <port polarity="Output" name="hsOut" />
        <port polarity="Output" name="vsOut" />
        <port polarity="Output" name="hsCount(9:0)" />
        <port polarity="Output" name="isActive" />
        <port polarity="Output" name="columnCount(9:0)" />
        <port polarity="Output" name="rowCount(9:0)" />
        <port polarity="Input" name="clkin" />
        <blockdef name="compm16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <rect width="256" x="64" y="-384" height="320" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="constant">
            <timestamp>2006-1-1T10:10:10</timestamp>
            <rect width="112" x="0" y="0" height="64" />
            <line x2="112" y1="32" y2="32" x1="144" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="kelvinsTenBitCounter">
            <timestamp>2016-5-15T23:13:32</timestamp>
            <rect width="64" x="320" y="20" height="24" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-192" height="256" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <block symbolname="gnd" name="XLXI_5">
            <blockpin signalname="_gnd" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_14">
            <blockpin signalname="_vcc" name="P" />
        </block>
        <block symbolname="and2" name="XLXI_61">
            <blockpin signalname="a_L0" name="I0" />
            <blockpin signalname="a_L" name="I1" />
            <blockpin signalname="isActive" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_62(9:0)">
            <blockpin signalname="vsCount(9:0)" name="I" />
            <blockpin signalname="rowCount(9:0)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_63(9:0)">
            <blockpin signalname="hsCount(9:0)" name="I" />
            <blockpin signalname="columnCount(9:0)" name="O" />
        </block>
        <block symbolname="compm16" name="XLXI_57">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,hsCount(9:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_98(15:0)" name="B(15:0)" />
            <blockpin name="GT" />
            <blockpin signalname="a_L" name="LT" />
        </block>
        <block symbolname="compm16" name="XLXI_58">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,vsCount(9:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_99(15:0)" name="B(15:0)" />
            <blockpin name="GT" />
            <blockpin signalname="a_L0" name="LT" />
        </block>
        <block symbolname="constant" name="XLXI_59">
            <attr value="27F" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_98(15:0)" name="O" />
        </block>
        <block symbolname="constant" name="XLXI_60">
            <attr value="1DF" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_99(15:0)" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_93">
            <blockpin signalname="XLXN_165" name="I0" />
            <blockpin signalname="XLXN_164" name="I1" />
            <blockpin signalname="XLXN_163" name="I2" />
            <blockpin signalname="outSigCompVS" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_94">
            <blockpin signalname="XLXN_167" name="I0" />
            <blockpin signalname="XLXN_166" name="I1" />
            <blockpin signalname="XLXN_163" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_95">
            <blockpin signalname="XLXN_169" name="I0" />
            <blockpin signalname="XLXN_168" name="I1" />
            <blockpin signalname="XLXN_164" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_96">
            <blockpin signalname="l_V" name="I0" />
            <blockpin signalname="g_V" name="I1" />
            <blockpin signalname="XLXN_165" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_101">
            <blockpin signalname="g_V" name="I" />
            <blockpin signalname="XLXN_166" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_102">
            <blockpin signalname="l_V2" name="I" />
            <blockpin signalname="XLXN_167" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_103">
            <blockpin signalname="g_V2" name="I" />
            <blockpin signalname="XLXN_168" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_104">
            <blockpin signalname="l_V" name="I" />
            <blockpin signalname="XLXN_169" name="O" />
        </block>
        <block symbolname="compm16" name="XLXI_41">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,vsCount(9:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_63(15:0)" name="B(15:0)" />
            <blockpin signalname="g_V" name="GT" />
            <blockpin signalname="l_V2" name="LT" />
        </block>
        <block symbolname="constant" name="XLXI_44">
            <attr value="1E9" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_63(15:0)" name="O" />
        </block>
        <block symbolname="compm16" name="XLXI_42">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,vsCount(9:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_65(15:0)" name="B(15:0)" />
            <blockpin signalname="g_V2" name="GT" />
            <blockpin signalname="l_V" name="LT" />
        </block>
        <block symbolname="constant" name="XLXI_45">
            <attr value="1EA" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_65(15:0)" name="O" />
        </block>
        <block symbolname="kelvinsTenBitCounter" name="XLXI_65">
            <blockpin signalname="clkin" name="CLK" />
            <blockpin signalname="resetOn525" name="R" />
            <blockpin signalname="resetOn800" name="CE" />
            <blockpin name="TC" />
            <blockpin signalname="vsCount(9:0)" name="outCount(9:0)" />
        </block>
        <block symbolname="inv" name="XLXI_38">
            <blockpin signalname="XLXN_55" name="I" />
            <blockpin signalname="hsOut" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_51">
            <blockpin signalname="outSigCompVS" name="I" />
            <blockpin signalname="vsOut" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_67">
            <blockpin signalname="l_H" name="I0" />
            <blockpin signalname="g_H" name="I1" />
            <blockpin signalname="XLXN_113" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_80">
            <blockpin signalname="XLXN_113" name="I0" />
            <blockpin signalname="XLXN_143" name="I1" />
            <blockpin signalname="XLXN_142" name="I2" />
            <blockpin signalname="XLXN_55" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_83">
            <blockpin signalname="XLXN_149" name="I0" />
            <blockpin signalname="XLXN_148" name="I1" />
            <blockpin signalname="XLXN_142" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_84">
            <blockpin signalname="XLXN_151" name="I0" />
            <blockpin signalname="XLXN_150" name="I1" />
            <blockpin signalname="XLXN_143" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_85">
            <blockpin signalname="g_H1" name="I" />
            <blockpin signalname="XLXN_148" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_86">
            <blockpin signalname="l_H" name="I" />
            <blockpin signalname="XLXN_149" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_87">
            <blockpin signalname="g_H" name="I" />
            <blockpin signalname="XLXN_150" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_88">
            <blockpin signalname="l_H1" name="I" />
            <blockpin signalname="XLXN_151" name="O" />
        </block>
        <block symbolname="compm16" name="XLXI_2">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,hsCount(9:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_3(15:0)" name="B(15:0)" />
            <blockpin signalname="g_H1" name="GT" />
            <blockpin signalname="l_H" name="LT" />
        </block>
        <block symbolname="constant" name="XLXI_15">
            <attr value="2EE" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_3(15:0)" name="O" />
        </block>
        <block symbolname="compm16" name="XLXI_4">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,hsCount(9:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_5(15:0)" name="B(15:0)" />
            <blockpin signalname="g_H" name="GT" />
            <blockpin signalname="l_H1" name="LT" />
        </block>
        <block symbolname="constant" name="XLXI_16">
            <attr value="0000028F" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_5(15:0)" name="O" />
        </block>
        <block symbolname="kelvinsTenBitCounter" name="XLXI_64">
            <blockpin signalname="clkin" name="CLK" />
            <blockpin signalname="resetOn800" name="R" />
            <blockpin signalname="_vcc" name="CE" />
            <blockpin name="TC" />
            <blockpin signalname="hsCount(9:0)" name="outCount(9:0)" />
        </block>
        <block symbolname="compm16" name="XLXI_141">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,hsCount(9:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_256(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_257" name="GT" />
            <blockpin signalname="XLXN_258" name="LT" />
        </block>
        <block symbolname="constant" name="XLXI_142">
            <attr value="320" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_256(15:0)" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_143">
            <blockpin signalname="XLXN_257" name="I" />
            <blockpin signalname="XLXN_260" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_144">
            <blockpin signalname="XLXN_258" name="I" />
            <blockpin signalname="XLXN_259" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_145">
            <blockpin signalname="XLXN_259" name="I0" />
            <blockpin signalname="XLXN_260" name="I1" />
            <blockpin signalname="resetOn800" name="O" />
        </block>
        <block symbolname="compm16" name="XLXI_146">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,vsCount(9:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_264(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_265" name="GT" />
            <blockpin signalname="XLXN_266" name="LT" />
        </block>
        <block symbolname="constant" name="XLXI_147">
            <attr value="20D" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_264(15:0)" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_148">
            <blockpin signalname="XLXN_265" name="I" />
            <blockpin signalname="XLXN_268" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_149">
            <blockpin signalname="XLXN_266" name="I" />
            <blockpin signalname="XLXN_267" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_150">
            <blockpin signalname="XLXN_267" name="I0" />
            <blockpin signalname="XLXN_268" name="I1" />
            <blockpin signalname="resetOn525" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="vsOut">
            <wire x2="3296" y1="560" y2="560" x1="3168" />
            <wire x2="3312" y1="432" y2="432" x1="3296" />
            <wire x2="3296" y1="432" y2="560" x1="3296" />
        </branch>
        <iomarker fontsize="28" x="3312" y="352" name="hsOut" orien="R0" />
        <iomarker fontsize="28" x="3312" y="432" name="vsOut" orien="R0" />
        <instance x="32" y="1376" name="XLXI_5" orien="R0" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="144" y="1248" type="branch" />
            <wire x2="96" y1="1232" y2="1248" x1="96" />
            <wire x2="144" y1="1232" y2="1232" x1="96" />
            <wire x2="144" y1="1232" y2="1248" x1="144" />
        </branch>
        <instance x="32" y="1520" name="XLXI_14" orien="R0" />
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="160" y="1520" type="branch" />
            <wire x2="96" y1="1520" y2="1536" x1="96" />
            <wire x2="160" y1="1536" y2="1536" x1="96" />
            <wire x2="160" y1="1520" y2="1536" x1="160" />
        </branch>
        <branch name="hsOut">
            <wire x2="3296" y1="352" y2="352" x1="3184" />
            <wire x2="3312" y1="352" y2="352" x1="3296" />
        </branch>
        <branch name="isActive">
            <wire x2="3136" y1="896" y2="896" x1="2592" />
        </branch>
        <branch name="rowCount(9:0)">
            <wire x2="3136" y1="1072" y2="1072" x1="2608" />
        </branch>
        <branch name="columnCount(9:0)">
            <wire x2="3136" y1="1184" y2="1184" x1="2608" />
        </branch>
        <iomarker fontsize="28" x="3136" y="896" name="isActive" orien="R0" />
        <iomarker fontsize="28" x="3136" y="1072" name="rowCount(9:0)" orien="R0" />
        <iomarker fontsize="28" x="3136" y="1184" name="columnCount(9:0)" orien="R0" />
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,hsCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1920" y="1344" type="branch" />
            <wire x2="1936" y1="1344" y2="1344" x1="1920" />
            <wire x2="1936" y1="1344" y2="1360" x1="1936" />
            <wire x2="2240" y1="1360" y2="1360" x1="1936" />
        </branch>
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,vsCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1888" y="1792" type="branch" />
            <wire x2="1904" y1="1792" y2="1792" x1="1888" />
            <wire x2="1904" y1="1792" y2="1824" x1="1904" />
            <wire x2="2224" y1="1824" y2="1824" x1="1904" />
        </branch>
        <instance x="2336" y="992" name="XLXI_61" orien="R0" />
        <branch name="a_L">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="864" type="branch" />
            <wire x2="2336" y1="864" y2="864" x1="2288" />
        </branch>
        <branch name="a_L0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="928" type="branch" />
            <wire x2="2336" y1="928" y2="928" x1="2288" />
        </branch>
        <instance x="2384" y="1104" name="XLXI_62(9:0)" orien="R0" />
        <instance x="2384" y="1216" name="XLXI_63(9:0)" orien="R0" />
        <branch name="vsCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2368" y="1072" type="branch" />
            <wire x2="2384" y1="1072" y2="1072" x1="2368" />
        </branch>
        <branch name="hsCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2368" y="1184" type="branch" />
            <wire x2="2384" y1="1184" y2="1184" x1="2368" />
        </branch>
        <iomarker fontsize="28" x="96" y="752" name="clkin" orien="R180" />
        <branch name="clkin">
            <wire x2="240" y1="752" y2="752" x1="96" />
            <wire x2="240" y1="752" y2="2064" x1="240" />
            <wire x2="480" y1="2064" y2="2064" x1="240" />
            <wire x2="928" y1="624" y2="624" x1="240" />
            <wire x2="240" y1="624" y2="752" x1="240" />
        </branch>
        <instance x="2240" y="1680" name="XLXI_57" orien="R0" />
        <instance x="2224" y="2144" name="XLXI_58" orien="R0" />
        <branch name="XLXN_98(15:0)">
            <wire x2="2208" y1="1552" y2="1552" x1="2160" />
            <wire x2="2240" y1="1552" y2="1552" x1="2208" />
        </branch>
        <branch name="XLXN_99(15:0)">
            <wire x2="2192" y1="2016" y2="2016" x1="2144" />
            <wire x2="2224" y1="2016" y2="2016" x1="2192" />
        </branch>
        <instance x="2016" y="1520" name="XLXI_59" orien="R0">
        </instance>
        <instance x="2000" y="1984" name="XLXI_60" orien="R0">
        </instance>
        <branch name="a_L">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2672" y="1488" type="branch" />
            <wire x2="2672" y1="1488" y2="1488" x1="2624" />
        </branch>
        <branch name="a_L0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2656" y="1952" type="branch" />
            <wire x2="2656" y1="1952" y2="1952" x1="2608" />
        </branch>
        <instance x="2928" y="2496" name="XLXI_93" orien="R0" />
        <branch name="outSigCompVS">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3248" y="2368" type="branch" />
            <wire x2="3216" y1="2368" y2="2368" x1="3184" />
            <wire x2="3248" y1="2368" y2="2368" x1="3216" />
        </branch>
        <branch name="XLXN_163">
            <wire x2="2928" y1="2240" y2="2240" x1="2816" />
            <wire x2="2928" y1="2240" y2="2304" x1="2928" />
        </branch>
        <branch name="XLXN_164">
            <wire x2="2928" y1="2368" y2="2368" x1="2832" />
        </branch>
        <branch name="XLXN_165">
            <wire x2="2928" y1="2560" y2="2560" x1="2816" />
            <wire x2="2928" y1="2432" y2="2560" x1="2928" />
        </branch>
        <instance x="2560" y="2336" name="XLXI_94" orien="R0" />
        <instance x="2576" y="2464" name="XLXI_95" orien="R0" />
        <instance x="2560" y="2656" name="XLXI_96" orien="R0" />
        <branch name="XLXN_166">
            <wire x2="2560" y1="2208" y2="2208" x1="2544" />
        </branch>
        <branch name="XLXN_167">
            <wire x2="2560" y1="2272" y2="2272" x1="2544" />
        </branch>
        <branch name="XLXN_168">
            <wire x2="2576" y1="2336" y2="2336" x1="2560" />
        </branch>
        <branch name="XLXN_169">
            <wire x2="2576" y1="2400" y2="2400" x1="2544" />
        </branch>
        <branch name="g_V">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2544" y="2528" type="branch" />
            <wire x2="2560" y1="2528" y2="2528" x1="2544" />
        </branch>
        <branch name="l_V">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2544" y="2592" type="branch" />
            <wire x2="2560" y1="2592" y2="2592" x1="2544" />
        </branch>
        <instance x="2320" y="2240" name="XLXI_101" orien="R0" />
        <instance x="2320" y="2304" name="XLXI_102" orien="R0" />
        <branch name="g_V">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="2208" type="branch" />
            <wire x2="2288" y1="2208" y2="2208" x1="2272" />
            <wire x2="2320" y1="2208" y2="2208" x1="2288" />
        </branch>
        <branch name="l_V2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="2272" type="branch" />
            <wire x2="2320" y1="2272" y2="2272" x1="2272" />
        </branch>
        <instance x="2336" y="2368" name="XLXI_103" orien="R0" />
        <instance x="2320" y="2432" name="XLXI_104" orien="R0" />
        <branch name="g_V2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="2336" type="branch" />
            <wire x2="2336" y1="2336" y2="2336" x1="2288" />
        </branch>
        <branch name="l_V">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="2400" type="branch" />
            <wire x2="2320" y1="2400" y2="2400" x1="2288" />
        </branch>
        <branch name="vsCount(9:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="2064" type="branch" />
            <wire x2="912" y1="2064" y2="2064" x1="864" />
            <wire x2="864" y1="2064" y2="2128" x1="864" />
            <wire x2="928" y1="2128" y2="2128" x1="864" />
            <wire x2="928" y1="2128" y2="2256" x1="928" />
            <wire x2="928" y1="2256" y2="2256" x1="864" />
        </branch>
        <instance x="1008" y="2048" name="XLXI_41" orien="R0" />
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,vsCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1728" type="branch" />
            <wire x2="1008" y1="1728" y2="1728" x1="976" />
        </branch>
        <branch name="XLXN_63(15:0)">
            <wire x2="1008" y1="1920" y2="1920" x1="896" />
        </branch>
        <branch name="g_V">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1424" y="1792" type="branch" />
            <wire x2="1424" y1="1792" y2="1792" x1="1392" />
        </branch>
        <branch name="l_V2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1424" y="1856" type="branch" />
            <wire x2="1424" y1="1856" y2="1856" x1="1392" />
        </branch>
        <instance x="752" y="1888" name="XLXI_44" orien="R0">
        </instance>
        <instance x="1024" y="2640" name="XLXI_42" orien="R0" />
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,vsCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="2320" type="branch" />
            <wire x2="1024" y1="2320" y2="2320" x1="992" />
        </branch>
        <branch name="XLXN_65(15:0)">
            <wire x2="1024" y1="2512" y2="2512" x1="912" />
        </branch>
        <branch name="g_V2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1456" y="2384" type="branch" />
            <wire x2="1456" y1="2384" y2="2384" x1="1408" />
        </branch>
        <branch name="l_V">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1456" y="2448" type="branch" />
            <wire x2="1456" y1="2448" y2="2448" x1="1408" />
        </branch>
        <instance x="768" y="2480" name="XLXI_45" orien="R0">
        </instance>
        <text style="fontsize:32;fontname:Arial" x="780" y="1620">Row, VS</text>
        <rect width="1324" x="264" y="1232" height="1416" />
        <branch name="resetOn525">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="464" y="2128" type="branch" />
            <wire x2="480" y1="2128" y2="2128" x1="464" />
        </branch>
        <instance x="480" y="2224" name="XLXI_65" orien="R0">
        </instance>
        <branch name="XLXN_55">
            <wire x2="2928" y1="320" y2="352" x1="2928" />
            <wire x2="2960" y1="352" y2="352" x1="2928" />
        </branch>
        <instance x="2960" y="384" name="XLXI_38" orien="R0" />
        <branch name="outSigCompVS">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2912" y="560" type="branch" />
            <wire x2="2944" y1="560" y2="560" x1="2912" />
        </branch>
        <instance x="2944" y="592" name="XLXI_51" orien="R0" />
        <branch name="XLXN_113">
            <wire x2="2656" y1="384" y2="384" x1="2640" />
            <wire x2="2672" y1="384" y2="384" x1="2656" />
        </branch>
        <instance x="2384" y="480" name="XLXI_67" orien="R0" />
        <branch name="g_H">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2352" y="352" type="branch" />
            <wire x2="2368" y1="352" y2="352" x1="2352" />
            <wire x2="2384" y1="352" y2="352" x1="2368" />
        </branch>
        <branch name="l_H">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2352" y="416" type="branch" />
            <wire x2="2368" y1="416" y2="416" x1="2352" />
            <wire x2="2384" y1="416" y2="416" x1="2368" />
        </branch>
        <instance x="2672" y="448" name="XLXI_80" orien="R0" />
        <branch name="XLXN_142">
            <wire x2="2672" y1="160" y2="160" x1="2576" />
            <wire x2="2672" y1="160" y2="256" x1="2672" />
        </branch>
        <branch name="XLXN_143">
            <wire x2="2624" y1="256" y2="256" x1="2576" />
            <wire x2="2624" y1="256" y2="320" x1="2624" />
            <wire x2="2672" y1="320" y2="320" x1="2624" />
        </branch>
        <instance x="2320" y="256" name="XLXI_83" orien="R0" />
        <instance x="2320" y="352" name="XLXI_84" orien="R0" />
        <branch name="XLXN_148">
            <wire x2="2320" y1="128" y2="128" x1="2304" />
        </branch>
        <branch name="XLXN_149">
            <wire x2="2320" y1="192" y2="192" x1="2304" />
        </branch>
        <branch name="XLXN_150">
            <wire x2="2304" y1="272" y2="272" x1="2256" />
            <wire x2="2320" y1="224" y2="224" x1="2304" />
            <wire x2="2304" y1="224" y2="272" x1="2304" />
        </branch>
        <branch name="XLXN_151">
            <wire x2="2304" y1="336" y2="336" x1="2272" />
            <wire x2="2320" y1="288" y2="288" x1="2304" />
            <wire x2="2304" y1="288" y2="336" x1="2304" />
        </branch>
        <instance x="2080" y="160" name="XLXI_85" orien="R0" />
        <instance x="2080" y="224" name="XLXI_86" orien="R0" />
        <branch name="g_H1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="128" type="branch" />
            <wire x2="2080" y1="128" y2="128" x1="2064" />
        </branch>
        <branch name="l_H">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="192" type="branch" />
            <wire x2="2080" y1="192" y2="192" x1="2064" />
        </branch>
        <branch name="g_H">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2000" y="272" type="branch" />
            <wire x2="2032" y1="272" y2="272" x1="2000" />
        </branch>
        <branch name="l_H1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2000" y="336" type="branch" />
            <wire x2="2048" y1="336" y2="336" x1="2000" />
        </branch>
        <instance x="2032" y="304" name="XLXI_87" orien="R0" />
        <instance x="2048" y="368" name="XLXI_88" orien="R0" />
        <branch name="hsCount(9:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="624" type="branch" />
            <wire x2="1328" y1="816" y2="816" x1="1312" />
            <wire x2="1376" y1="624" y2="624" x1="1328" />
            <wire x2="1328" y1="624" y2="688" x1="1328" />
            <wire x2="1328" y1="688" y2="816" x1="1328" />
        </branch>
        <instance x="1408" y="592" name="XLXI_2" orien="R0" />
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,hsCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="272" type="branch" />
            <wire x2="1408" y1="272" y2="272" x1="1376" />
        </branch>
        <branch name="XLXN_3(15:0)">
            <wire x2="1408" y1="464" y2="464" x1="1328" />
        </branch>
        <instance x="1184" y="432" name="XLXI_15" orien="R0">
        </instance>
        <instance x="1424" y="1216" name="XLXI_4" orien="R0" />
        <branch name="XLXN_5(15:0)">
            <wire x2="1424" y1="1088" y2="1088" x1="1344" />
        </branch>
        <instance x="1200" y="1056" name="XLXI_16" orien="R0">
        </instance>
        <branch name="g_H1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1824" y="336" type="branch" />
            <wire x2="1824" y1="336" y2="336" x1="1792" />
        </branch>
        <branch name="l_H">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1824" y="400" type="branch" />
            <wire x2="1824" y1="400" y2="400" x1="1792" />
        </branch>
        <branch name="g_H">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1840" y="960" type="branch" />
            <wire x2="1840" y1="960" y2="960" x1="1808" />
        </branch>
        <branch name="l_H1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1840" y="1024" type="branch" />
            <wire x2="1840" y1="1024" y2="1024" x1="1808" />
        </branch>
        <text style="fontsize:32;fontname:Arial" x="1152" y="212">Col, HS</text>
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="752" type="branch" />
            <wire x2="928" y1="752" y2="752" x1="912" />
        </branch>
        <branch name="resetOn800">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="688" type="branch" />
            <wire x2="928" y1="688" y2="688" x1="912" />
        </branch>
        <instance x="928" y="784" name="XLXI_64" orien="R0">
        </instance>
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,hsCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1248" y="896" type="branch" />
            <wire x2="1424" y1="896" y2="896" x1="1248" />
        </branch>
        <instance x="240" y="624" name="XLXI_141" orien="R0" />
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,hsCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="192" y="304" type="branch" />
            <wire x2="240" y1="304" y2="304" x1="192" />
        </branch>
        <branch name="XLXN_256(15:0)">
            <wire x2="240" y1="496" y2="496" x1="208" />
        </branch>
        <instance x="64" y="464" name="XLXI_142" orien="R0">
        </instance>
        <branch name="XLXN_257">
            <wire x2="656" y1="368" y2="368" x1="624" />
        </branch>
        <instance x="656" y="400" name="XLXI_143" orien="R0" />
        <branch name="XLXN_258">
            <wire x2="656" y1="432" y2="432" x1="624" />
        </branch>
        <instance x="656" y="464" name="XLXI_144" orien="R0" />
        <branch name="XLXN_259">
            <wire x2="912" y1="432" y2="432" x1="880" />
        </branch>
        <branch name="XLXN_260">
            <wire x2="912" y1="368" y2="368" x1="880" />
        </branch>
        <instance x="912" y="496" name="XLXI_145" orien="R0" />
        <branch name="resetOn800">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="400" type="branch" />
            <wire x2="1200" y1="400" y2="400" x1="1168" />
        </branch>
        <instance x="448" y="1680" name="XLXI_146" orien="R0" />
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,vsCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="416" y="1360" type="branch" />
            <wire x2="448" y1="1360" y2="1360" x1="416" />
        </branch>
        <branch name="XLXN_264(15:0)">
            <wire x2="448" y1="1552" y2="1552" x1="432" />
        </branch>
        <instance x="288" y="1520" name="XLXI_147" orien="R0">
        </instance>
        <branch name="XLXN_265">
            <wire x2="864" y1="1424" y2="1424" x1="832" />
        </branch>
        <instance x="864" y="1456" name="XLXI_148" orien="R0" />
        <branch name="XLXN_266">
            <wire x2="864" y1="1488" y2="1488" x1="832" />
        </branch>
        <instance x="864" y="1520" name="XLXI_149" orien="R0" />
        <branch name="XLXN_267">
            <wire x2="1120" y1="1488" y2="1488" x1="1088" />
        </branch>
        <branch name="XLXN_268">
            <wire x2="1120" y1="1424" y2="1424" x1="1088" />
        </branch>
        <instance x="1120" y="1552" name="XLXI_150" orien="R0" />
        <branch name="resetOn525">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="1456" type="branch" />
            <wire x2="1392" y1="1456" y2="1456" x1="1376" />
        </branch>
        <branch name="resetOn800">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="464" y="2192" type="branch" />
            <wire x2="480" y1="2192" y2="2192" x1="464" />
        </branch>
        <rect width="1856" x="64" y="108" height="1096" />
        <text style="fontsize:64;fontname:Arial" x="184" y="60">Schematic 5 VGA Controller</text>
    </sheet>
</drawing>