// Verilog test fixture created from schematic C:\Users\kelvi\OneDrive\Documents\school files\spring 2016\ce 100\lab 7\workingDir\final1\lab7progress7\lab7progress5\vgaController.sch - Fri Jun 03 14:38:27 2016

`timescale 1ns / 1ps

module vgaController_vgaController_sch_tb();

// Inputs
   reg clkin;

// Output
   wire hsOut;
   wire vsOut;
   wire [9:0] hsCount;
   wire isActive;
   wire [9:0] columnCount;
   wire [9:0] rowCount;

// Bidirs

// Instantiate the UUT
   vgaController UUT (
		.hsOut(hsOut), 
		.vsOut(vsOut), 
		.hsCount(hsCount), 
		.isActive(isActive), 
		.columnCount(columnCount), 
		.rowCount(rowCount), 
		.clkin(clkin)
   );
// Initialize Inputs
   `ifdef auto_init
       initial begin
		clkin = 0;
   `endif
	
	    parameter PERIOD = 100;
    parameter real DUTY_CYCLE = 0.5;
    parameter OFFSET = 20;

         // If your input labels are different
         // substitute your labels for eclk, pb0 and pb2 as required.

	initial    // Clock process for eclk
	begin

        #OFFSET
		clkin = 1'b1;
        forever
         begin
            #(PERIOD-(PERIOD*DUTY_CYCLE)) clkin = ~clkin;
         end
	end
	
	initial
	begin
	 // Add your stimuli here.
	 // To set signal foo to value 0 use
	 // foo = 1'b0;
	 // To set signal foo to value 1 use
	 // foo = 1'b1;
	 // Advance time by multiples of 100ns.
	 // To advance time by 100ns use the following line
	 // 100ns;
	 
	 
      end
endmodule
