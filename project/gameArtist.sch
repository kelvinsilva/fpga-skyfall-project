<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="columnCountIn(9:0)" />
        <signal name="rowCountIn(9:0)" />
        <signal name="isActiveIn" />
        <signal name="frameTickIn" />
        <signal name="redOut(2:0)" />
        <signal name="greenOut(2:0)" />
        <signal name="blueOut(1:0)" />
        <signal name="clkin" />
        <signal name="_gnd" />
        <signal name="_vcc" />
        <signal name="meteorOneSigE" />
        <signal name="meteorOneSigC" />
        <signal name="meteorTwoSigE" />
        <signal name="meteorTwoSigC" />
        <signal name="slugVisibleOut" />
        <signal name="slugMagentaFlashSig" />
        <signal name="leftRightIn" />
        <signal name="slugResetSizeIn" />
        <signal name="slugIncSizeIn" />
        <signal name="slugAnimationOnOff" />
        <signal name="diamondAnimationOnOff" />
        <signal name="isEndTwo" />
        <signal name="slugCountOut(9:0)" />
        <signal name="slugFlashAnimOn" />
        <signal name="collisionHappened" />
        <signal name="slugMagentaColorIn" />
        <signal name="isEndOne" />
        <signal name="XLXN_396(2:0)" />
        <signal name="XLXN_397(2:0)" />
        <signal name="XLXN_398(1:0)" />
        <signal name="winSig,winSig,winSig" />
        <signal name="_gnd,_gnd,_gnd" />
        <signal name="_gnd,_gnd" />
        <signal name="endGameIn,endGameIn,endGameIn" />
        <signal name="endGameIn,endGameIn" />
        <signal name="endGameIn" />
        <signal name="startIn" />
        <signal name="XLXN_456" />
        <signal name="XLXN_459" />
        <signal name="XLXN_416(15:0)" />
        <signal name="winSig" />
        <signal name="redOut(2)" />
        <signal name="greenOut(2)" />
        <signal name="blueOut(1)" />
        <signal name="XLXN_341" />
        <signal name="XLXN_342" />
        <signal name="diamondTouchBottomScreen" />
        <signal name="XLXN_140" />
        <signal name="XLXN_417(15:0)" />
        <signal name="XLXN_251" />
        <signal name="slugSizeOut(15:0)" />
        <signal name="slugSizeOut(9:0)" />
        <port polarity="Input" name="columnCountIn(9:0)" />
        <port polarity="Input" name="rowCountIn(9:0)" />
        <port polarity="Input" name="isActiveIn" />
        <port polarity="Input" name="frameTickIn" />
        <port polarity="Output" name="redOut(2:0)" />
        <port polarity="Output" name="greenOut(2:0)" />
        <port polarity="Output" name="blueOut(1:0)" />
        <port polarity="Input" name="clkin" />
        <port polarity="Input" name="leftRightIn" />
        <port polarity="Input" name="slugResetSizeIn" />
        <port polarity="Input" name="slugIncSizeIn" />
        <port polarity="Input" name="slugAnimationOnOff" />
        <port polarity="Input" name="diamondAnimationOnOff" />
        <port polarity="Input" name="slugFlashAnimOn" />
        <port polarity="Output" name="collisionHappened" />
        <port polarity="Input" name="slugMagentaColorIn" />
        <port polarity="Input" name="endGameIn" />
        <port polarity="Input" name="startIn" />
        <port polarity="Output" name="diamondTouchBottomScreen" />
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="graphicsControl">
            <timestamp>2016-5-23T2:30:30</timestamp>
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="480" y1="-352" y2="-352" x1="416" />
            <rect width="64" x="416" y="-236" height="24" />
            <line x2="480" y1="-224" y2="-224" x1="416" />
            <rect width="64" x="416" y="-108" height="24" />
            <line x2="480" y1="-96" y2="-96" x1="416" />
            <rect width="64" x="416" y="20" height="24" />
            <line x2="480" y1="32" y2="32" x1="416" />
            <rect width="352" x="64" y="-384" height="968" />
            <rect width="64" x="0" y="4" height="24" />
            <line x2="0" y1="16" y2="16" x1="64" />
            <line x2="0" y1="64" y2="64" x1="64" />
            <line x2="0" y1="128" y2="128" x1="64" />
            <rect width="64" x="0" y="180" height="24" />
            <line x2="0" y1="192" y2="192" x1="64" />
            <rect width="64" x="0" y="292" height="24" />
            <line x2="0" y1="304" y2="304" x1="64" />
            <line x2="0" y1="352" y2="352" x1="64" />
            <line x2="0" y1="416" y2="416" x1="64" />
            <line x2="0" y1="480" y2="480" x1="64" />
            <line x2="0" y1="544" y2="544" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="slugHandler">
            <timestamp>2016-5-24T0:44:2</timestamp>
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="448" y="-492" height="24" />
            <line x2="512" y1="-480" y2="-480" x1="448" />
            <line x2="512" y1="-336" y2="-336" x1="448" />
            <line x2="512" y1="-192" y2="-192" x1="448" />
            <rect width="64" x="448" y="-60" height="24" />
            <line x2="512" y1="-48" y2="-48" x1="448" />
            <rect width="384" x="64" y="-512" height="576" />
        </blockdef>
        <blockdef name="m2_1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-64" y2="-192" x1="96" />
            <line x2="96" y1="-96" y2="-64" x1="256" />
            <line x2="256" y1="-160" y2="-96" x1="256" />
            <line x2="256" y1="-192" y2="-160" x1="96" />
            <line x2="96" y1="-32" y2="-32" x1="176" />
            <line x2="176" y1="-80" y2="-32" x1="176" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
            <line x2="256" y1="-128" y2="-128" x1="320" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
            <line x2="96" y1="-160" y2="-160" x1="0" />
        </blockdef>
        <blockdef name="diamondHandler">
            <timestamp>2016-5-24T21:12:8</timestamp>
            <rect width="64" x="0" y="20" height="24" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="544" y1="-288" y2="-288" x1="480" />
            <line x2="544" y1="-208" y2="-208" x1="480" />
            <line x2="544" y1="-128" y2="-128" x1="480" />
            <line x2="544" y1="-48" y2="-48" x1="480" />
            <rect width="416" x="64" y="-320" height="384" />
        </blockdef>
        <blockdef name="constant">
            <timestamp>2006-1-1T10:10:10</timestamp>
            <rect width="112" x="0" y="0" height="64" />
            <line x2="112" y1="32" y2="32" x1="144" />
        </blockdef>
        <block symbolname="m2_1" name="XLXI_104(2:0)">
            <blockpin signalname="XLXN_396(2:0)" name="D0" />
            <blockpin signalname="winSig,winSig,winSig" name="D1" />
            <blockpin signalname="endGameIn,endGameIn,endGameIn" name="S0" />
            <blockpin signalname="redOut(2:0)" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_105(2:0)">
            <blockpin signalname="XLXN_397(2:0)" name="D0" />
            <blockpin signalname="_gnd,_gnd,_gnd" name="D1" />
            <blockpin signalname="endGameIn,endGameIn,endGameIn" name="S0" />
            <blockpin signalname="greenOut(2:0)" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_106(1:0)">
            <blockpin signalname="XLXN_398(1:0)" name="D0" />
            <blockpin signalname="_gnd,_gnd" name="D1" />
            <blockpin signalname="endGameIn,endGameIn" name="S0" />
            <blockpin signalname="blueOut(1:0)" name="O" />
        </block>
        <block symbolname="gnd" name="XLXI_24">
            <blockpin signalname="_gnd" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_58">
            <blockpin signalname="_vcc" name="P" />
        </block>
        <block symbolname="constant" name="XLXI_122">
            <attr value="140" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_416(15:0)" name="O" />
        </block>
        <block symbolname="diamondHandler" name="XLXI_120">
            <blockpin signalname="clkin" name="clkin" />
            <blockpin signalname="diamondAnimationOnOff" name="startStop" />
            <blockpin signalname="slugResetSizeIn" name="resetToTop" />
            <blockpin signalname="rowCountIn(9:0)" name="monitorV_rowCount(9:0)" />
            <blockpin signalname="columnCountIn(9:0)" name="monitorH_colCount(9:0)" />
            <blockpin signalname="meteorOneSigE" name="edgeBitsTrue" />
            <blockpin signalname="meteorOneSigC" name="centerBitsTrue" />
            <blockpin name="isHalfway" />
            <blockpin signalname="isEndOne" name="isEnd" />
            <blockpin signalname="XLXN_416(15:0)" name="resetPosIn(15:0)" />
        </block>
        <block symbolname="m2_1" name="XLXI_128">
            <blockpin signalname="XLXN_459" name="D0" />
            <blockpin signalname="_gnd" name="D1" />
            <blockpin signalname="slugResetSizeIn" name="S0" />
            <blockpin signalname="meteorTwoSigE" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_129">
            <blockpin signalname="XLXN_456" name="D0" />
            <blockpin signalname="_gnd" name="D1" />
            <blockpin signalname="slugResetSizeIn" name="S0" />
            <blockpin signalname="meteorTwoSigC" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_116">
            <blockpin signalname="isActiveIn" name="I0" />
            <blockpin signalname="_vcc" name="I1" />
            <blockpin signalname="winSig" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_87">
            <blockpin signalname="blueOut(1)" name="I0" />
            <blockpin signalname="redOut(2)" name="I1" />
            <blockpin signalname="XLXN_341" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_88">
            <blockpin signalname="blueOut(1)" name="I0" />
            <blockpin signalname="greenOut(2)" name="I1" />
            <blockpin signalname="XLXN_342" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_89">
            <blockpin signalname="XLXN_342" name="I0" />
            <blockpin signalname="XLXN_341" name="I1" />
            <blockpin signalname="collisionHappened" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_100">
            <blockpin signalname="isEndTwo" name="I0" />
            <blockpin signalname="isEndOne" name="I1" />
            <blockpin signalname="diamondTouchBottomScreen" name="O" />
        </block>
        <block symbolname="constant" name="XLXI_123">
            <attr value="01" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_417(15:0)" name="O" />
        </block>
        <block symbolname="diamondHandler" name="XLXI_121">
            <blockpin signalname="clkin" name="clkin" />
            <blockpin signalname="diamondAnimationOnOff" name="startStop" />
            <blockpin signalname="slugResetSizeIn" name="resetToTop" />
            <blockpin signalname="rowCountIn(9:0)" name="monitorV_rowCount(9:0)" />
            <blockpin signalname="columnCountIn(9:0)" name="monitorH_colCount(9:0)" />
            <blockpin signalname="XLXN_459" name="edgeBitsTrue" />
            <blockpin signalname="XLXN_456" name="centerBitsTrue" />
            <blockpin signalname="XLXN_140" name="isHalfway" />
            <blockpin signalname="isEndTwo" name="isEnd" />
            <blockpin signalname="XLXN_417(15:0)" name="resetPosIn(15:0)" />
        </block>
        <block symbolname="slugHandler" name="XLXI_90">
            <blockpin signalname="slugAnimationOnOff" name="slugEnable" />
            <blockpin signalname="slugFlashAnimOn" name="slugFlash" />
            <blockpin signalname="leftRightIn" name="slugLeftRight" />
            <blockpin signalname="clkin" name="clkin" />
            <blockpin signalname="slugResetSizeIn" name="slugResetSize" />
            <blockpin signalname="XLXN_251" name="digselsigIn" />
            <blockpin signalname="slugIncSizeIn" name="slugIncSize" />
            <blockpin signalname="frameTickIn" name="framesig" />
            <blockpin signalname="slugSizeOut(15:0)" name="slugSize(15:0)" />
            <blockpin signalname="slugVisibleOut" name="slugVisible" />
            <blockpin signalname="slugMagentaFlashSig" name="slugMagentaFlashSig" />
            <blockpin signalname="slugCountOut(9:0)" name="slugCountOut(9:0)" />
            <blockpin signalname="slugMagentaColorIn" name="slugMagentaColor" />
        </block>
        <block symbolname="graphicsControl" name="XLXI_81">
            <blockpin signalname="isActiveIn" name="isActiveIn" />
            <blockpin name="leftRightIn" />
            <blockpin name="startIn" />
            <blockpin signalname="columnCountIn(9:0)" name="columnCountIn(9:0)" />
            <blockpin signalname="rowCountIn(9:0)" name="rowCountIn(9:0)" />
            <blockpin name="endOfGame" />
            <blockpin signalname="XLXN_396(2:0)" name="redOut(2:0)" />
            <blockpin signalname="XLXN_397(2:0)" name="greenOut(2:0)" />
            <blockpin signalname="XLXN_398(1:0)" name="blueOut(1:0)" />
            <blockpin signalname="slugCountOut(9:0)" name="slugCounter(9:0)" />
            <blockpin signalname="slugMagentaFlashSig" name="slugMagentaSig" />
            <blockpin signalname="slugVisibleOut" name="slugVisibleSig" />
            <blockpin signalname="slugSizeOut(9:0)" name="slugSize(9:0)" />
            <blockpin signalname="meteorOneSigE" name="meteorBitsTrueEdgesOne" />
            <blockpin signalname="meteorOneSigC" name="meteorBitsTrueCenterOne" />
            <blockpin signalname="meteorTwoSigE" name="meteorBitsTrueEdgesTwo" />
            <blockpin signalname="meteorTwoSigC" name="meteorBitsTrueCenterTwo" />
            <blockpin name="meteorCounter(9:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="slugIncSizeIn">
            <wire x2="624" y1="1456" y2="1456" x1="272" />
            <wire x2="624" y1="1456" y2="1792" x1="624" />
            <wire x2="976" y1="1792" y2="1792" x1="624" />
        </branch>
        <iomarker fontsize="28" x="272" y="1456" name="slugIncSizeIn" orien="R180" />
        <branch name="slugAnimationOnOff">
            <wire x2="784" y1="1280" y2="1280" x1="288" />
            <wire x2="784" y1="1280" y2="1408" x1="784" />
            <wire x2="976" y1="1408" y2="1408" x1="784" />
        </branch>
        <iomarker fontsize="28" x="288" y="1280" name="slugAnimationOnOff" orien="R180" />
        <branch name="slugResetSizeIn">
            <wire x2="304" y1="1408" y2="1424" x1="304" />
            <wire x2="688" y1="1424" y2="1424" x1="304" />
            <wire x2="688" y1="1424" y2="1664" x1="688" />
            <wire x2="976" y1="1664" y2="1664" x1="688" />
        </branch>
        <iomarker fontsize="28" x="304" y="1408" name="slugResetSizeIn" orien="R180" />
        <iomarker fontsize="28" x="320" y="1344" name="slugFlashAnimOn" orien="R180" />
        <branch name="slugFlashAnimOn">
            <wire x2="720" y1="1344" y2="1344" x1="320" />
            <wire x2="720" y1="1344" y2="1472" x1="720" />
            <wire x2="976" y1="1472" y2="1472" x1="720" />
        </branch>
        <branch name="slugMagentaColorIn">
            <wire x2="608" y1="1808" y2="1808" x1="304" />
            <wire x2="608" y1="1808" y2="1920" x1="608" />
            <wire x2="976" y1="1920" y2="1920" x1="608" />
        </branch>
        <branch name="greenOut(2:0)">
            <wire x2="2800" y1="304" y2="304" x1="2400" />
            <wire x2="2400" y1="304" y2="480" x1="2400" />
            <wire x2="3200" y1="240" y2="240" x1="2800" />
            <wire x2="2800" y1="240" y2="304" x1="2800" />
        </branch>
        <branch name="blueOut(1:0)">
            <wire x2="2416" y1="688" y2="688" x1="2400" />
            <wire x2="3200" y1="352" y2="352" x1="2416" />
            <wire x2="2416" y1="352" y2="688" x1="2416" />
        </branch>
        <instance x="2080" y="400" name="XLXI_104(2:0)" orien="R0" />
        <instance x="2080" y="608" name="XLXI_105(2:0)" orien="R0" />
        <instance x="2080" y="816" name="XLXI_106(1:0)" orien="R0" />
        <branch name="XLXN_396(2:0)">
            <wire x2="1744" y1="336" y2="336" x1="1424" />
            <wire x2="1744" y1="240" y2="336" x1="1744" />
            <wire x2="2064" y1="240" y2="240" x1="1744" />
            <wire x2="2080" y1="240" y2="240" x1="2064" />
        </branch>
        <branch name="XLXN_397(2:0)">
            <wire x2="2064" y1="464" y2="464" x1="1424" />
            <wire x2="2080" y1="448" y2="448" x1="2064" />
            <wire x2="2064" y1="448" y2="464" x1="2064" />
        </branch>
        <branch name="XLXN_398(1:0)">
            <wire x2="2064" y1="592" y2="592" x1="1424" />
            <wire x2="2064" y1="592" y2="656" x1="2064" />
            <wire x2="2080" y1="656" y2="656" x1="2064" />
        </branch>
        <branch name="winSig,winSig,winSig">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="304" type="branch" />
            <wire x2="2080" y1="304" y2="304" x1="2064" />
        </branch>
        <branch name="_gnd,_gnd,_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="512" type="branch" />
            <wire x2="2080" y1="512" y2="512" x1="2064" />
        </branch>
        <branch name="_gnd,_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="720" type="branch" />
            <wire x2="2080" y1="720" y2="720" x1="2064" />
        </branch>
        <branch name="endGameIn,endGameIn,endGameIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="368" type="branch" />
            <wire x2="2080" y1="368" y2="368" x1="2064" />
        </branch>
        <branch name="endGameIn,endGameIn,endGameIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="576" type="branch" />
            <wire x2="2080" y1="576" y2="576" x1="2064" />
        </branch>
        <branch name="endGameIn,endGameIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="784" type="branch" />
            <wire x2="2080" y1="784" y2="784" x1="2064" />
        </branch>
        <branch name="redOut(2:0)">
            <wire x2="2416" y1="272" y2="272" x1="2400" />
            <wire x2="3184" y1="144" y2="144" x1="2416" />
            <wire x2="2416" y1="144" y2="272" x1="2416" />
        </branch>
        <instance x="96" y="2640" name="XLXI_24" orien="R0" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="224" y="2464" type="branch" />
            <wire x2="224" y1="2464" y2="2464" x1="160" />
            <wire x2="160" y1="2464" y2="2512" x1="160" />
        </branch>
        <instance x="96" y="2320" name="XLXI_58" orien="R0" />
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="2304" type="branch" />
            <wire x2="240" y1="2320" y2="2320" x1="160" />
            <wire x2="240" y1="2304" y2="2320" x1="240" />
        </branch>
        <rect width="364" x="20" y="2080" height="612" />
        <text style="fontsize:36;fontname:Arial" x="56" y="2124">Ground and Power Signal</text>
        <iomarker fontsize="28" x="304" y="1808" name="slugMagentaColorIn" orien="R180" />
        <branch name="rowCountIn(9:0)">
            <wire x2="384" y1="272" y2="272" x1="272" />
        </branch>
        <branch name="columnCountIn(9:0)">
            <wire x2="416" y1="192" y2="192" x1="320" />
        </branch>
        <iomarker fontsize="28" x="272" y="272" name="rowCountIn(9:0)" orien="R180" />
        <iomarker fontsize="28" x="320" y="192" name="columnCountIn(9:0)" orien="R180" />
        <branch name="isActiveIn">
            <wire x2="320" y1="336" y2="336" x1="176" />
        </branch>
        <branch name="diamondAnimationOnOff">
            <wire x2="496" y1="464" y2="464" x1="384" />
        </branch>
        <branch name="endGameIn">
            <wire x2="448" y1="384" y2="384" x1="208" />
        </branch>
        <branch name="startIn">
            <wire x2="336" y1="656" y2="656" x1="176" />
        </branch>
        <branch name="clkin">
            <wire x2="288" y1="704" y2="704" x1="144" />
        </branch>
        <branch name="leftRightIn">
            <wire x2="384" y1="592" y2="592" x1="208" />
        </branch>
        <branch name="frameTickIn">
            <wire x2="352" y1="528" y2="528" x1="224" />
        </branch>
        <iomarker fontsize="28" x="176" y="336" name="isActiveIn" orien="R180" />
        <iomarker fontsize="28" x="384" y="464" name="diamondAnimationOnOff" orien="R180" />
        <iomarker fontsize="28" x="208" y="384" name="endGameIn" orien="R180" />
        <iomarker fontsize="28" x="176" y="656" name="startIn" orien="R180" />
        <iomarker fontsize="28" x="144" y="704" name="clkin" orien="R180" />
        <iomarker fontsize="28" x="208" y="592" name="leftRightIn" orien="R180" />
        <iomarker fontsize="28" x="224" y="528" name="frameTickIn" orien="R180" />
        <rect width="556" x="16" y="60" height="1856" />
        <text style="fontsize:36;fontname:Arial" x="148" y="80">Module Inputs</text>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="848" y="2320" type="branch" />
            <wire x2="880" y1="2320" y2="2320" x1="848" />
        </branch>
        <branch name="rowCountIn(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="864" y="2512" type="branch" />
            <wire x2="880" y1="2512" y2="2512" x1="864" />
        </branch>
        <branch name="columnCountIn(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="848" y="2576" type="branch" />
            <wire x2="880" y1="2576" y2="2576" x1="848" />
        </branch>
        <branch name="meteorOneSigE">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1472" y="2320" type="branch" />
            <wire x2="1472" y1="2320" y2="2320" x1="1424" />
        </branch>
        <branch name="meteorOneSigC">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1488" y="2400" type="branch" />
            <wire x2="1488" y1="2400" y2="2400" x1="1424" />
        </branch>
        <branch name="diamondAnimationOnOff">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="848" y="2384" type="branch" />
            <wire x2="880" y1="2384" y2="2384" x1="848" />
        </branch>
        <branch name="isEndOne">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1440" y="2560" type="branch" />
            <wire x2="1440" y1="2560" y2="2560" x1="1424" />
        </branch>
        <branch name="slugResetSizeIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="864" y="2448" type="branch" />
            <wire x2="880" y1="2448" y2="2448" x1="864" />
        </branch>
        <branch name="XLXN_416(15:0)">
            <wire x2="880" y1="2640" y2="2640" x1="864" />
        </branch>
        <instance x="720" y="2608" name="XLXI_122" orien="R0">
        </instance>
        <instance x="880" y="2608" name="XLXI_120" orien="R0">
        </instance>
        <branch name="isEndTwo">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2640" y="2480" type="branch" />
            <wire x2="2624" y1="2544" y2="2544" x1="2592" />
            <wire x2="2640" y1="2480" y2="2480" x1="2624" />
            <wire x2="2624" y1="2480" y2="2544" x1="2624" />
        </branch>
        <branch name="XLXN_456">
            <wire x2="2864" y1="2384" y2="2384" x1="2592" />
            <wire x2="2864" y1="2384" y2="2400" x1="2864" />
            <wire x2="2880" y1="2400" y2="2400" x1="2864" />
        </branch>
        <instance x="2880" y="2352" name="XLXI_128" orien="R0" />
        <instance x="2880" y="2560" name="XLXI_129" orien="R0" />
        <branch name="meteorTwoSigE">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3248" y="2224" type="branch" />
            <wire x2="3248" y1="2224" y2="2224" x1="3200" />
        </branch>
        <branch name="meteorTwoSigC">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3248" y="2432" type="branch" />
            <wire x2="3248" y1="2432" y2="2432" x1="3200" />
        </branch>
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2864" y="2256" type="branch" />
            <wire x2="2880" y1="2256" y2="2256" x1="2864" />
        </branch>
        <branch name="slugResetSizeIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2864" y="2320" type="branch" />
            <wire x2="2880" y1="2320" y2="2320" x1="2864" />
        </branch>
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2864" y="2464" type="branch" />
            <wire x2="2880" y1="2464" y2="2464" x1="2864" />
        </branch>
        <branch name="slugResetSizeIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2864" y="2528" type="branch" />
            <wire x2="2880" y1="2528" y2="2528" x1="2864" />
        </branch>
        <rect width="2160" x="520" y="2020" height="680" />
        <text style="fontsize:40;fontname:Arial" x="1724" y="2052">Diamond Draw Module</text>
        <branch name="collisionHappened">
            <wire x2="2864" y1="1104" y2="1104" x1="2848" />
            <wire x2="3200" y1="1104" y2="1104" x1="2864" />
        </branch>
        <instance x="2208" y="1600" name="XLXI_116" orien="R0" />
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2160" y="1472" type="branch" />
            <wire x2="2208" y1="1472" y2="1472" x1="2160" />
        </branch>
        <branch name="isActiveIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2160" y="1536" type="branch" />
            <wire x2="2208" y1="1536" y2="1536" x1="2160" />
        </branch>
        <branch name="winSig">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2768" y="1504" type="branch" />
            <wire x2="2768" y1="1504" y2="1504" x1="2464" />
        </branch>
        <instance x="2240" y="1120" name="XLXI_87" orien="R0" />
        <instance x="2240" y="1296" name="XLXI_88" orien="R0" />
        <branch name="redOut(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2192" y="992" type="branch" />
            <wire x2="2240" y1="992" y2="992" x1="2192" />
        </branch>
        <branch name="greenOut(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2192" y="1168" type="branch" />
            <wire x2="2240" y1="1168" y2="1168" x1="2192" />
        </branch>
        <branch name="blueOut(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2208" y="1056" type="branch" />
            <wire x2="2240" y1="1056" y2="1056" x1="2208" />
        </branch>
        <branch name="blueOut(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2192" y="1232" type="branch" />
            <wire x2="2240" y1="1232" y2="1232" x1="2192" />
        </branch>
        <instance x="2592" y="1200" name="XLXI_89" orien="R0" />
        <branch name="XLXN_341">
            <wire x2="2544" y1="1024" y2="1024" x1="2496" />
            <wire x2="2544" y1="1024" y2="1072" x1="2544" />
            <wire x2="2592" y1="1072" y2="1072" x1="2544" />
        </branch>
        <branch name="XLXN_342">
            <wire x2="2544" y1="1200" y2="1200" x1="2496" />
            <wire x2="2544" y1="1136" y2="1200" x1="2544" />
            <wire x2="2592" y1="1136" y2="1136" x1="2544" />
        </branch>
        <branch name="diamondTouchBottomScreen">
            <wire x2="2448" y1="1792" y2="1792" x1="2432" />
            <wire x2="3104" y1="1792" y2="1792" x1="2448" />
        </branch>
        <instance x="2176" y="1888" name="XLXI_100" orien="R0" />
        <branch name="isEndOne">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="1760" type="branch" />
            <wire x2="2176" y1="1760" y2="1760" x1="2144" />
        </branch>
        <branch name="isEndTwo">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="1824" type="branch" />
            <wire x2="2176" y1="1824" y2="1824" x1="2144" />
        </branch>
        <rect width="940" x="1960" y="824" height="452" />
        <text style="fontsize:40;fontname:Arial" x="2192" y="860">Color Collision Detection</text>
        <rect width="872" x="1632" y="16" height="808" />
        <text style="fontsize:40;fontname:Arial" x="1672" y="60">Independent Win Screen RGB Signal Chooser</text>
        <rect width="964" x="1944" y="1300" height="320" />
        <text style="fontsize:40;fontname:Arial" x="2224" y="1336">Win Signal RGB Color Generator</text>
        <rect width="904" x="1944" y="1636" height="288" />
        <iomarker fontsize="28" x="3184" y="144" name="redOut(2:0)" orien="R0" />
        <iomarker fontsize="28" x="3200" y="240" name="greenOut(2:0)" orien="R0" />
        <iomarker fontsize="28" x="3200" y="352" name="blueOut(1:0)" orien="R0" />
        <rect width="504" x="2996" y="8" height="464" />
        <text style="fontsize:40;fontname:Arial" x="3116" y="40">Module RGB Out</text>
        <rect width="788" x="2712" y="2016" height="684" />
        <text style="fontsize:40;fontname:Arial" x="2688" y="2056">Meteor Display Signal Chooser (For meteor 2)</text>
        <rect width="1144" x="552" y="2076" height="596" />
        <text style="fontsize:40;fontname:Arial" x="936" y="2112">Meteor One Handler</text>
        <rect width="872" x="1744" y="2088" height="588" />
        <branch name="XLXN_140">
            <wire x2="2624" y1="2464" y2="2464" x1="2592" />
        </branch>
        <branch name="columnCountIn(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2032" y="2560" type="branch" />
            <wire x2="2048" y1="2560" y2="2560" x1="2032" />
        </branch>
        <branch name="rowCountIn(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2032" y="2496" type="branch" />
            <wire x2="2048" y1="2496" y2="2496" x1="2032" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2016" y="2304" type="branch" />
            <wire x2="2048" y1="2304" y2="2304" x1="2016" />
        </branch>
        <branch name="slugResetSizeIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2032" y="2432" type="branch" />
            <wire x2="2048" y1="2432" y2="2432" x1="2032" />
        </branch>
        <branch name="XLXN_417(15:0)">
            <wire x2="2048" y1="2624" y2="2624" x1="2000" />
        </branch>
        <instance x="1856" y="2592" name="XLXI_123" orien="R0">
        </instance>
        <instance x="2048" y="2592" name="XLXI_121" orien="R0">
        </instance>
        <branch name="diamondAnimationOnOff">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="2368" type="branch" />
            <wire x2="2048" y1="2368" y2="2368" x1="1984" />
        </branch>
        <branch name="XLXN_459">
            <wire x2="2720" y1="2304" y2="2304" x1="2592" />
            <wire x2="2720" y1="2192" y2="2304" x1="2720" />
            <wire x2="2864" y1="2192" y2="2192" x1="2720" />
            <wire x2="2880" y1="2192" y2="2192" x1="2864" />
        </branch>
        <text style="fontsize:40;fontname:Arial" x="2008" y="2128">Meteor Two Handler</text>
        <rect width="852" x="648" y="12" height="1180" />
        <branch name="leftRightIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="1536" type="branch" />
            <wire x2="976" y1="1536" y2="1536" x1="944" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="1600" type="branch" />
            <wire x2="976" y1="1600" y2="1600" x1="944" />
        </branch>
        <branch name="XLXN_251">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="1728" type="branch" />
            <wire x2="976" y1="1728" y2="1728" x1="944" />
        </branch>
        <branch name="frameTickIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="1856" type="branch" />
            <wire x2="976" y1="1856" y2="1856" x1="944" />
        </branch>
        <branch name="slugSizeOut(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="1408" type="branch" />
            <wire x2="1520" y1="1408" y2="1408" x1="1488" />
        </branch>
        <branch name="slugVisibleOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="1552" type="branch" />
            <wire x2="1520" y1="1552" y2="1552" x1="1488" />
        </branch>
        <branch name="slugMagentaFlashSig">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="1696" type="branch" />
            <wire x2="1520" y1="1696" y2="1696" x1="1488" />
        </branch>
        <branch name="slugCountOut(9:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="1840" type="branch" />
            <wire x2="1520" y1="1840" y2="1840" x1="1488" />
        </branch>
        <instance x="976" y="1888" name="XLXI_90" orien="R0">
        </instance>
        <rect width="1228" x="648" y="1224" height="784" />
        <text style="fontsize:40;fontname:Arial" x="1072" y="1264">Slug Draw Module</text>
        <branch name="isActiveIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="208" type="branch" />
            <wire x2="944" y1="208" y2="208" x1="912" />
        </branch>
        <branch name="columnCountIn(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="400" type="branch" />
            <wire x2="944" y1="400" y2="400" x1="912" />
        </branch>
        <branch name="rowCountIn(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="464" type="branch" />
            <wire x2="944" y1="464" y2="464" x1="912" />
        </branch>
        <branch name="meteorOneSigE">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="912" type="branch" />
            <wire x2="944" y1="912" y2="912" x1="912" />
        </branch>
        <branch name="meteorOneSigC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="976" type="branch" />
            <wire x2="944" y1="976" y2="976" x1="912" />
        </branch>
        <branch name="meteorTwoSigE">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="1040" type="branch" />
            <wire x2="944" y1="1040" y2="1040" x1="912" />
        </branch>
        <branch name="meteorTwoSigC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="1104" type="branch" />
            <wire x2="944" y1="1104" y2="1104" x1="912" />
        </branch>
        <branch name="slugSizeOut(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="752" type="branch" />
            <wire x2="944" y1="752" y2="752" x1="912" />
        </branch>
        <branch name="slugMagentaFlashSig">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="624" type="branch" />
            <wire x2="944" y1="624" y2="624" x1="912" />
        </branch>
        <branch name="slugVisibleOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="688" type="branch" />
            <wire x2="944" y1="688" y2="688" x1="912" />
        </branch>
        <branch name="slugCountOut(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="848" y="576" type="branch" />
            <wire x2="944" y1="576" y2="576" x1="848" />
        </branch>
        <instance x="944" y="560" name="XLXI_81" orien="R0">
        </instance>
        <text style="fontsize:40;fontname:Arial" x="888" y="36">RGB Signal Controller</text>
        <iomarker fontsize="28" x="3200" y="1104" name="collisionHappened" orien="R0" />
        <rect width="476" x="3000" y="808" height="416" />
        <text style="fontsize:40;fontname:Arial" x="3032" y="844">Collision Signal Output</text>
        <text style="fontsize:40;fontname:Arial" x="2008" y="1680">Diamond Bottom of Screen Signal Generator</text>
        <iomarker fontsize="28" x="3104" y="1792" name="diamondTouchBottomScreen" orien="R0" />
        <rect width="616" x="2896" y="1636" height="288" />
        <text style="fontsize:36;fontname:Arial" x="2864" y="1664">Diamond Touch Bottom of Screen Output</text>
        <text style="fontsize:64;fontname:Arial" x="2636" y="564">Schematic 2 GameArtist</text>
    </sheet>
</drawing>