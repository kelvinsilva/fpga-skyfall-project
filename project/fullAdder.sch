<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="a" />
        <signal name="b" />
        <signal name="Cin" />
        <signal name="aInv" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="XLXN_15" />
        <signal name="XLXN_27" />
        <signal name="S" />
        <signal name="Cout" />
        <port polarity="Input" name="a" />
        <port polarity="Input" name="b" />
        <port polarity="Input" name="Cin" />
        <port polarity="Output" name="S" />
        <port polarity="Output" name="Cout" />
        <blockdef name="m4_1e">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-416" y2="-416" x1="0" />
            <line x2="96" y1="-352" y2="-352" x1="0" />
            <line x2="96" y1="-288" y2="-288" x1="0" />
            <line x2="96" y1="-224" y2="-224" x1="0" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
            <line x2="256" y1="-320" y2="-320" x1="320" />
            <line x2="96" y1="-160" y2="-160" x1="0" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
            <line x2="96" y1="-96" y2="-96" x1="176" />
            <line x2="176" y1="-208" y2="-96" x1="176" />
            <line x2="96" y1="-32" y2="-32" x1="224" />
            <line x2="224" y1="-216" y2="-32" x1="224" />
            <line x2="96" y1="-224" y2="-192" x1="256" />
            <line x2="256" y1="-416" y2="-224" x1="256" />
            <line x2="256" y1="-448" y2="-416" x1="96" />
            <line x2="96" y1="-192" y2="-448" x1="96" />
            <line x2="96" y1="-160" y2="-160" x1="128" />
            <line x2="128" y1="-200" y2="-160" x1="128" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="inv" name="XLXI_5">
            <blockpin signalname="a" name="I" />
            <blockpin signalname="aInv" name="O" />
        </block>
        <block symbolname="m4_1e" name="XLXI_1">
            <blockpin signalname="a" name="D0" />
            <blockpin signalname="aInv" name="D1" />
            <blockpin signalname="aInv" name="D2" />
            <blockpin signalname="a" name="D3" />
            <blockpin signalname="XLXN_22" name="E" />
            <blockpin signalname="Cin" name="S0" />
            <blockpin signalname="b" name="S1" />
            <blockpin signalname="S" name="O" />
        </block>
        <block symbolname="m4_1e" name="XLXI_2">
            <blockpin signalname="XLXN_15" name="D0" />
            <blockpin signalname="Cin" name="D1" />
            <blockpin signalname="Cin" name="D2" />
            <blockpin signalname="XLXN_27" name="D3" />
            <blockpin signalname="XLXN_23" name="E" />
            <blockpin signalname="b" name="S0" />
            <blockpin signalname="a" name="S1" />
            <blockpin signalname="Cout" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_6">
            <blockpin signalname="XLXN_22" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_7">
            <blockpin signalname="XLXN_23" name="P" />
        </block>
        <block symbolname="gnd" name="XLXI_8">
            <blockpin signalname="XLXN_15" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_9">
            <blockpin signalname="XLXN_27" name="P" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="a">
            <wire x2="464" y1="320" y2="320" x1="144" />
        </branch>
        <branch name="b">
            <wire x2="160" y1="416" y2="416" x1="144" />
            <wire x2="464" y1="416" y2="416" x1="160" />
        </branch>
        <branch name="Cin">
            <wire x2="464" y1="512" y2="512" x1="144" />
        </branch>
        <branch name="a">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="144" y="640" type="branch" />
            <wire x2="160" y1="640" y2="640" x1="144" />
            <wire x2="256" y1="640" y2="640" x1="160" />
        </branch>
        <iomarker fontsize="28" x="144" y="320" name="a" orien="R180" />
        <iomarker fontsize="28" x="144" y="416" name="b" orien="R180" />
        <iomarker fontsize="28" x="144" y="512" name="Cin" orien="R180" />
        <instance x="256" y="672" name="XLXI_5" orien="R0" />
        <branch name="aInv">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="512" y="640" type="branch" />
            <wire x2="512" y1="640" y2="640" x1="480" />
        </branch>
        <instance x="1152" y="688" name="XLXI_1" orien="R0" />
        <instance x="1136" y="1296" name="XLXI_2" orien="R0" />
        <branch name="a">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1120" y="272" type="branch" />
            <wire x2="1152" y1="272" y2="272" x1="1120" />
        </branch>
        <branch name="aInv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1120" y="336" type="branch" />
            <wire x2="1152" y1="336" y2="336" x1="1120" />
        </branch>
        <branch name="aInv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1120" y="400" type="branch" />
            <wire x2="1152" y1="400" y2="400" x1="1120" />
        </branch>
        <branch name="a">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1120" y="464" type="branch" />
            <wire x2="1152" y1="464" y2="464" x1="1120" />
        </branch>
        <branch name="Cin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1120" y="528" type="branch" />
            <wire x2="1152" y1="528" y2="528" x1="1120" />
        </branch>
        <branch name="b">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1120" y="592" type="branch" />
            <wire x2="1152" y1="592" y2="592" x1="1120" />
        </branch>
        <branch name="Cin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="944" type="branch" />
            <wire x2="1136" y1="944" y2="944" x1="1072" />
        </branch>
        <branch name="Cin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1008" type="branch" />
            <wire x2="1136" y1="1008" y2="1008" x1="1072" />
        </branch>
        <branch name="b">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1136" type="branch" />
            <wire x2="1136" y1="1136" y2="1136" x1="1072" />
        </branch>
        <branch name="a">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1200" type="branch" />
            <wire x2="1136" y1="1200" y2="1200" x1="1072" />
        </branch>
        <instance x="912" y="688" name="XLXI_6" orien="R0" />
        <branch name="XLXN_22">
            <wire x2="1120" y1="688" y2="688" x1="976" />
            <wire x2="1152" y1="656" y2="656" x1="1120" />
            <wire x2="1120" y1="656" y2="688" x1="1120" />
        </branch>
        <instance x="848" y="1280" name="XLXI_7" orien="R0" />
        <branch name="XLXN_23">
            <wire x2="1072" y1="1280" y2="1280" x1="912" />
            <wire x2="1136" y1="1280" y2="1280" x1="1072" />
            <wire x2="1136" y1="1264" y2="1280" x1="1136" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="832" y1="880" y2="928" x1="832" />
            <wire x2="1136" y1="880" y2="880" x1="832" />
        </branch>
        <instance x="768" y="1056" name="XLXI_8" orien="R0" />
        <instance x="864" y="1056" name="XLXI_9" orien="R0" />
        <branch name="XLXN_27">
            <wire x2="928" y1="1056" y2="1072" x1="928" />
            <wire x2="1136" y1="1072" y2="1072" x1="928" />
        </branch>
        <branch name="S">
            <wire x2="1520" y1="368" y2="368" x1="1472" />
        </branch>
        <branch name="Cout">
            <wire x2="1536" y1="976" y2="976" x1="1456" />
        </branch>
        <iomarker fontsize="28" x="1520" y="368" name="S" orien="R0" />
        <iomarker fontsize="28" x="1536" y="976" name="Cout" orien="R0" />
    </sheet>
</drawing>