<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="in0" />
        <signal name="in1" />
        <signal name="in2" />
        <signal name="in3" />
        <signal name="in4" />
        <signal name="in5" />
        <signal name="in6" />
        <signal name="in7" />
        <signal name="sign" />
        <signal name="ZERO" />
        <signal name="inTransform0" />
        <signal name="inTransform1" />
        <signal name="inTransform2" />
        <signal name="inTransform3" />
        <signal name="inTransform4" />
        <signal name="inTransform5" />
        <signal name="inTransform6" />
        <signal name="inTransform7" />
        <signal name="d0" />
        <signal name="d2" />
        <signal name="d3" />
        <signal name="d4" />
        <signal name="d5" />
        <signal name="d6" />
        <signal name="d7" />
        <signal name="d8" />
        <signal name="d1" />
        <port polarity="Input" name="in0" />
        <port polarity="Input" name="in1" />
        <port polarity="Input" name="in2" />
        <port polarity="Input" name="in3" />
        <port polarity="Input" name="in4" />
        <port polarity="Input" name="in5" />
        <port polarity="Input" name="in6" />
        <port polarity="Input" name="in7" />
        <port polarity="Input" name="sign" />
        <port polarity="Output" name="d0" />
        <port polarity="Output" name="d2" />
        <port polarity="Output" name="d3" />
        <port polarity="Output" name="d4" />
        <port polarity="Output" name="d5" />
        <port polarity="Output" name="d6" />
        <port polarity="Output" name="d7" />
        <port polarity="Output" name="d1" />
        <blockdef name="eightBitAdder">
            <timestamp>2016-5-23T23:53:20</timestamp>
            <rect width="256" x="64" y="-1088" height="1088" />
            <line x2="0" y1="-1056" y2="-1056" x1="64" />
            <line x2="0" y1="-992" y2="-992" x1="64" />
            <line x2="0" y1="-928" y2="-928" x1="64" />
            <line x2="0" y1="-864" y2="-864" x1="64" />
            <line x2="0" y1="-800" y2="-800" x1="64" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-1056" y2="-1056" x1="320" />
            <line x2="384" y1="-928" y2="-928" x1="320" />
            <line x2="384" y1="-800" y2="-800" x1="320" />
            <line x2="384" y1="-672" y2="-672" x1="320" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <block symbolname="gnd" name="XLXI_12">
            <blockpin signalname="ZERO" name="G" />
        </block>
        <block symbolname="xor2" name="XLXI_13">
            <blockpin signalname="in0" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform0" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_14">
            <blockpin signalname="in1" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform1" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_15">
            <blockpin signalname="in2" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform2" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_16">
            <blockpin signalname="in3" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform3" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_17">
            <blockpin signalname="in4" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform4" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_18">
            <blockpin signalname="in5" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform5" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_19">
            <blockpin signalname="in6" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform6" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_28">
            <blockpin signalname="in7" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform7" name="O" />
        </block>
        <block symbolname="eightBitAdder" name="XLXI_2">
            <blockpin signalname="sign" name="CIN" />
            <blockpin signalname="ZERO" name="b0" />
            <blockpin signalname="inTransform0" name="a0" />
            <blockpin signalname="ZERO" name="b1" />
            <blockpin signalname="ZERO" name="b2" />
            <blockpin signalname="inTransform2" name="a2" />
            <blockpin signalname="ZERO" name="b3" />
            <blockpin signalname="inTransform3" name="a3" />
            <blockpin signalname="ZERO" name="b4" />
            <blockpin signalname="inTransform4" name="a4" />
            <blockpin signalname="ZERO" name="b5" />
            <blockpin signalname="inTransform5" name="a5" />
            <blockpin signalname="ZERO" name="b6" />
            <blockpin signalname="inTransform6" name="a6" />
            <blockpin signalname="ZERO" name="b7" />
            <blockpin signalname="inTransform7" name="a7" />
            <blockpin signalname="inTransform1" name="a1" />
            <blockpin signalname="d0" name="n0" />
            <blockpin signalname="d1" name="n1" />
            <blockpin signalname="d2" name="n2" />
            <blockpin signalname="d3" name="n3" />
            <blockpin signalname="d4" name="n4" />
            <blockpin signalname="d5" name="n5" />
            <blockpin signalname="d6" name="n6" />
            <blockpin signalname="d7" name="n8" />
            <blockpin signalname="d8" name="n7" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="in0">
            <wire x2="400" y1="176" y2="176" x1="144" />
        </branch>
        <branch name="in1">
            <wire x2="400" y1="256" y2="256" x1="144" />
        </branch>
        <branch name="in2">
            <wire x2="400" y1="336" y2="336" x1="144" />
        </branch>
        <branch name="in3">
            <wire x2="384" y1="400" y2="400" x1="144" />
            <wire x2="400" y1="400" y2="400" x1="384" />
        </branch>
        <branch name="in4">
            <wire x2="384" y1="464" y2="464" x1="144" />
            <wire x2="400" y1="464" y2="464" x1="384" />
        </branch>
        <iomarker fontsize="28" x="144" y="176" name="in0" orien="R180" />
        <iomarker fontsize="28" x="144" y="256" name="in1" orien="R180" />
        <iomarker fontsize="28" x="144" y="336" name="in2" orien="R180" />
        <iomarker fontsize="28" x="144" y="400" name="in3" orien="R180" />
        <iomarker fontsize="28" x="144" y="464" name="in4" orien="R180" />
        <branch name="in5">
            <wire x2="416" y1="544" y2="544" x1="160" />
        </branch>
        <branch name="in6">
            <wire x2="416" y1="640" y2="640" x1="160" />
        </branch>
        <branch name="in7">
            <wire x2="432" y1="704" y2="704" x1="160" />
        </branch>
        <iomarker fontsize="28" x="160" y="544" name="in5" orien="R180" />
        <iomarker fontsize="28" x="160" y="640" name="in6" orien="R180" />
        <iomarker fontsize="28" x="160" y="704" name="in7" orien="R180" />
        <branch name="sign">
            <wire x2="160" y1="96" y2="96" x1="144" />
            <wire x2="160" y1="96" y2="112" x1="160" />
            <wire x2="1920" y1="112" y2="112" x1="160" />
        </branch>
        <iomarker fontsize="28" x="144" y="96" name="sign" orien="R180" />
        <branch name="ZERO">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="576" y="1072" type="branch" />
            <wire x2="576" y1="1072" y2="1072" x1="240" />
        </branch>
        <instance x="176" y="1200" name="XLXI_12" orien="R0" />
        <instance x="1040" y="336" name="XLXI_13" orien="R0" />
        <instance x="1024" y="560" name="XLXI_14" orien="R0" />
        <instance x="1024" y="736" name="XLXI_15" orien="R0" />
        <instance x="1024" y="912" name="XLXI_16" orien="R0" />
        <instance x="1024" y="1088" name="XLXI_17" orien="R0" />
        <instance x="1024" y="1280" name="XLXI_18" orien="R0" />
        <instance x="1008" y="1456" name="XLXI_19" orien="R0" />
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1024" y="208" type="branch" />
            <wire x2="1040" y1="208" y2="208" x1="1024" />
        </branch>
        <branch name="in0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1024" y="272" type="branch" />
            <wire x2="1040" y1="272" y2="272" x1="1024" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1008" y="432" type="branch" />
            <wire x2="1024" y1="432" y2="432" x1="1008" />
        </branch>
        <branch name="in1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="496" type="branch" />
            <wire x2="1024" y1="496" y2="496" x1="992" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1008" y="608" type="branch" />
            <wire x2="1024" y1="608" y2="608" x1="1008" />
        </branch>
        <branch name="in2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="672" type="branch" />
            <wire x2="1024" y1="672" y2="672" x1="992" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="784" type="branch" />
            <wire x2="1024" y1="784" y2="784" x1="992" />
        </branch>
        <branch name="in3">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="848" type="branch" />
            <wire x2="1024" y1="848" y2="848" x1="992" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="960" type="branch" />
            <wire x2="1024" y1="960" y2="960" x1="992" />
        </branch>
        <branch name="in4">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1024" type="branch" />
            <wire x2="1024" y1="1024" y2="1024" x1="976" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="1152" type="branch" />
            <wire x2="1024" y1="1152" y2="1152" x1="992" />
        </branch>
        <branch name="in5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="1216" type="branch" />
            <wire x2="1024" y1="1216" y2="1216" x1="992" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="1328" type="branch" />
            <wire x2="1008" y1="1328" y2="1328" x1="992" />
        </branch>
        <branch name="in6">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1392" type="branch" />
            <wire x2="1008" y1="1392" y2="1392" x1="976" />
        </branch>
        <branch name="inTransform0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1328" y="240" type="branch" />
            <wire x2="1328" y1="240" y2="240" x1="1296" />
        </branch>
        <branch name="inTransform1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1312" y="464" type="branch" />
            <wire x2="1312" y1="464" y2="464" x1="1280" />
        </branch>
        <branch name="inTransform2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1328" y="640" type="branch" />
            <wire x2="1328" y1="640" y2="640" x1="1280" />
        </branch>
        <branch name="inTransform3">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1344" y="816" type="branch" />
            <wire x2="1344" y1="816" y2="816" x1="1280" />
        </branch>
        <branch name="inTransform4">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1312" y="992" type="branch" />
            <wire x2="1312" y1="992" y2="992" x1="1280" />
        </branch>
        <branch name="inTransform5">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1328" y="1184" type="branch" />
            <wire x2="1328" y1="1184" y2="1184" x1="1280" />
        </branch>
        <branch name="inTransform6">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1328" y="1360" type="branch" />
            <wire x2="1328" y1="1360" y2="1360" x1="1264" />
        </branch>
        <instance x="992" y="1648" name="XLXI_28" orien="R0" />
        <branch name="inTransform7">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1280" y="1552" type="branch" />
            <wire x2="1280" y1="1552" y2="1552" x1="1248" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1520" type="branch" />
            <wire x2="992" y1="1520" y2="1520" x1="976" />
        </branch>
        <branch name="in7">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1584" type="branch" />
            <wire x2="992" y1="1584" y2="1584" x1="976" />
        </branch>
        <rect width="736" x="852" y="164" height="1516" />
        <text style="fontsize:20;fontname:Arial" x="1164" y="1664">Simulate Tri State Inverter</text>
        <instance x="1920" y="1168" name="XLXI_2" orien="R0">
        </instance>
        <branch name="ZERO">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1888" y="176" type="branch" />
            <wire x2="1920" y1="176" y2="176" x1="1888" />
        </branch>
        <branch name="inTransform0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1904" y="240" type="branch" />
            <wire x2="1920" y1="240" y2="240" x1="1904" />
        </branch>
        <branch name="ZERO">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1888" y="304" type="branch" />
            <wire x2="1920" y1="304" y2="304" x1="1888" />
        </branch>
        <branch name="ZERO">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1888" y="368" type="branch" />
            <wire x2="1920" y1="368" y2="368" x1="1888" />
        </branch>
        <branch name="inTransform2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1888" y="432" type="branch" />
            <wire x2="1920" y1="432" y2="432" x1="1888" />
        </branch>
        <branch name="ZERO">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1888" y="496" type="branch" />
            <wire x2="1920" y1="496" y2="496" x1="1888" />
        </branch>
        <branch name="inTransform3">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1888" y="560" type="branch" />
            <wire x2="1920" y1="560" y2="560" x1="1888" />
        </branch>
        <branch name="ZERO">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1888" y="624" type="branch" />
            <wire x2="1920" y1="624" y2="624" x1="1888" />
        </branch>
        <branch name="inTransform4">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1872" y="688" type="branch" />
            <wire x2="1920" y1="688" y2="688" x1="1872" />
        </branch>
        <branch name="ZERO">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1856" y="752" type="branch" />
            <wire x2="1920" y1="752" y2="752" x1="1856" />
        </branch>
        <branch name="inTransform5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1872" y="816" type="branch" />
            <wire x2="1920" y1="816" y2="816" x1="1872" />
        </branch>
        <branch name="ZERO">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1872" y="880" type="branch" />
            <wire x2="1920" y1="880" y2="880" x1="1872" />
        </branch>
        <branch name="inTransform6">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1872" y="944" type="branch" />
            <wire x2="1920" y1="944" y2="944" x1="1872" />
        </branch>
        <branch name="ZERO">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1872" y="1008" type="branch" />
            <wire x2="1920" y1="1008" y2="1008" x1="1872" />
        </branch>
        <branch name="inTransform7">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1872" y="1072" type="branch" />
            <wire x2="1920" y1="1072" y2="1072" x1="1872" />
        </branch>
        <branch name="inTransform1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1872" y="1136" type="branch" />
            <wire x2="1920" y1="1136" y2="1136" x1="1872" />
        </branch>
        <branch name="d0">
            <wire x2="2320" y1="112" y2="112" x1="2304" />
        </branch>
        <branch name="d2">
            <wire x2="2320" y1="368" y2="368" x1="2304" />
        </branch>
        <branch name="d3">
            <wire x2="2352" y1="496" y2="496" x1="2304" />
        </branch>
        <branch name="d4">
            <wire x2="2336" y1="624" y2="624" x1="2304" />
        </branch>
        <branch name="d5">
            <wire x2="2368" y1="752" y2="752" x1="2304" />
        </branch>
        <branch name="d6">
            <wire x2="2368" y1="880" y2="880" x1="2304" />
        </branch>
        <branch name="d7">
            <wire x2="2384" y1="1008" y2="1008" x1="2304" />
        </branch>
        <branch name="d8">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2368" y="1136" type="branch" />
            <wire x2="2352" y1="1136" y2="1136" x1="2304" />
            <wire x2="2368" y1="1136" y2="1136" x1="2352" />
        </branch>
        <branch name="d1">
            <wire x2="2336" y1="240" y2="240" x1="2304" />
        </branch>
        <iomarker fontsize="28" x="2320" y="112" name="d0" orien="R0" />
        <iomarker fontsize="28" x="2336" y="240" name="d1" orien="R0" />
        <iomarker fontsize="28" x="2320" y="368" name="d2" orien="R0" />
        <iomarker fontsize="28" x="2352" y="496" name="d3" orien="R0" />
        <iomarker fontsize="28" x="2336" y="624" name="d4" orien="R0" />
        <iomarker fontsize="28" x="2368" y="752" name="d5" orien="R0" />
        <iomarker fontsize="28" x="2368" y="880" name="d6" orien="R0" />
        <iomarker fontsize="28" x="2384" y="1008" name="d7" orien="R0" />
        <text style="fontsize:64;fontname:Arial" x="244" y="56">Schematic 12 Sign Changer for Score Display</text>
    </sheet>
</drawing>