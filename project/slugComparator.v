`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// VERILOG 5 SLUG COMPARATOR. SAME AS ANY OTHER COMPARATOR BUT MORE CONVENIENT.
// Engineer: 
// 
// Create Date:    14:31:22 05/19/2016 
// Design Name: 
// Module Name:    slugComparator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module slugComparator(
    input [9:0] numberIn,
    output _LT,
    output _EQ,
    output _GT,
    input [9:0]numberInTwo
    );

	assign _LT = numberIn < numberInTwo;
	assign _EQ = numberIn == numberInTwo;
	assign _GT = numberIn > numberInTwo;

endmodule
