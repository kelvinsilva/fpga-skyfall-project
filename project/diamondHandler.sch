<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="_gnd" />
        <signal name="clkin" />
        <signal name="isEndOut" />
        <signal name="resetToTop" />
        <signal name="_vcc" />
        <signal name="resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop," />
        <signal name="XLXN_216" />
        <signal name="meteorRandomPos(15:0)" />
        <signal name="XLXN_181(15:0)" />
        <signal name="XLXN_178(15:0)" />
        <signal name="XLXN_189" />
        <signal name="q(15:0)" />
        <signal name="XLXN_230" />
        <signal name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,meteorCountOut(9:0)" />
        <signal name="XLXN_67(15:0)" />
        <signal name="startStop" />
        <signal name="monitorH_colCount(9:0)" />
        <signal name="monitorV_rowCount(9:0)" />
        <signal name="XLXN_239" />
        <signal name="centerBitsTrue" />
        <signal name="resetPosIn(15:0)" />
        <signal name="XLXN_198(15:0)" />
        <signal name="xPosWithAdjustment(15:0)" />
        <signal name="q(15:7),_vcc,q(5:0)" />
        <signal name="XLXN_82" />
        <signal name="frameTickIn" />
        <signal name="XLXN_29(9:0)" />
        <signal name="XLXN_83" />
        <signal name="XLXN_162" />
        <signal name="qOutC(15:0)" />
        <signal name="XLXN_257" />
        <signal name="XLXN_164" />
        <signal name="XLXN_165" />
        <signal name="XLXN_203" />
        <signal name="isHalfway" />
        <signal name="isEnd" />
        <signal name="isHalfOut" />
        <signal name="XLXN_155(15:0)" />
        <signal name="meteorCountOut(9:0)" />
        <signal name="qOutC(9:0)" />
        <signal name="XLXN_272" />
        <signal name="edgeBitsTrue" />
        <signal name="_gnd,xPosWithAdjustment(8:0)" />
        <port polarity="Input" name="clkin" />
        <port polarity="Input" name="resetToTop" />
        <port polarity="Input" name="startStop" />
        <port polarity="Input" name="monitorH_colCount(9:0)" />
        <port polarity="Input" name="monitorV_rowCount(9:0)" />
        <port polarity="Output" name="centerBitsTrue" />
        <port polarity="Input" name="resetPosIn(15:0)" />
        <port polarity="Output" name="isHalfway" />
        <port polarity="Output" name="isEnd" />
        <port polarity="Output" name="edgeBitsTrue" />
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="comp16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-384" height="320" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="320" y1="-224" y2="-224" x1="384" />
        </blockdef>
        <blockdef name="constant">
            <timestamp>2006-1-1T10:10:10</timestamp>
            <rect width="112" x="0" y="0" height="64" />
            <line x2="112" y1="32" y2="32" x1="144" />
        </blockdef>
        <blockdef name="slugComparator">
            <timestamp>2016-5-22T2:16:4</timestamp>
            <rect width="64" x="0" y="20" height="24" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-192" height="256" />
        </blockdef>
        <blockdef name="edgeDetector">
            <timestamp>2016-5-22T2:16:1</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="meteorBitsTrue">
            <timestamp>2016-5-22T2:16:4</timestamp>
            <line x2="496" y1="32" y2="32" x1="432" />
            <line x2="496" y1="96" y2="96" x1="432" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="368" x="64" y="-256" height="384" />
        </blockdef>
        <blockdef name="customLSFR">
            <timestamp>2016-5-22T2:16:1</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="fd16re">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="64" x="320" y="-268" height="24" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="cc16cle">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-448" height="384" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <rect width="64" x="320" y="-396" height="24" />
            <line x2="320" y1="-384" y2="-384" x1="384" />
            <rect width="64" x="0" y="-396" height="24" />
            <line x2="64" y1="-384" y2="-384" x1="0" />
        </blockdef>
        <blockdef name="m2_1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-64" y2="-192" x1="96" />
            <line x2="96" y1="-96" y2="-64" x1="256" />
            <line x2="256" y1="-160" y2="-96" x1="256" />
            <line x2="256" y1="-192" y2="-160" x1="96" />
            <line x2="96" y1="-32" y2="-32" x1="176" />
            <line x2="176" y1="-80" y2="-32" x1="176" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
            <line x2="256" y1="-128" y2="-128" x1="320" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
            <line x2="96" y1="-160" y2="-160" x1="0" />
        </blockdef>
        <block symbolname="gnd" name="XLXI_24">
            <blockpin signalname="_gnd" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_84">
            <blockpin signalname="_vcc" name="P" />
        </block>
        <block symbolname="customLSFR" name="XLXI_26">
            <blockpin signalname="clkin" name="clkin" />
            <blockpin signalname="meteorRandomPos(15:0)" name="outRandom(15:0)" />
        </block>
        <block symbolname="m2_1" name="XLXI_88(15:0)">
            <blockpin signalname="meteorRandomPos(15:0)" name="D0" />
            <blockpin signalname="XLXN_181(15:0)" name="D1" />
            <blockpin signalname="resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop," name="S0" />
            <blockpin signalname="XLXN_178(15:0)" name="O" />
        </block>
        <block symbolname="constant" name="XLXI_89">
            <attr value="140" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_181(15:0)" name="O" />
        </block>
        <block symbolname="fd16re" name="XLXI_87">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="XLXN_189" name="CE" />
            <blockpin signalname="XLXN_178(15:0)" name="D(15:0)" />
            <blockpin signalname="_gnd" name="R" />
            <blockpin signalname="q(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="m2_1" name="XLXI_91">
            <blockpin signalname="isEndOut" name="D0" />
            <blockpin signalname="_vcc" name="D1" />
            <blockpin signalname="resetToTop" name="S0" />
            <blockpin signalname="XLXN_189" name="O" />
        </block>
        <block symbolname="comp16" name="XLXI_40">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,meteorCountOut(9:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_67(15:0)" name="B(15:0)" />
            <blockpin signalname="isEndOut" name="EQ" />
        </block>
        <block symbolname="constant" name="XLXI_41">
            <attr value="20C" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_67(15:0)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_83(15:0)">
            <blockpin signalname="q(15:7),_vcc,q(5:0)" name="I" />
            <blockpin signalname="xPosWithAdjustment(15:0)" name="O" />
        </block>
        <block symbolname="edgeDetector" name="XLXI_51">
            <blockpin signalname="XLXN_83" name="_input" />
            <blockpin signalname="clkin" name="clkin" />
            <blockpin signalname="XLXN_82" name="signalOut" />
        </block>
        <block symbolname="or2" name="XLXI_15">
            <blockpin signalname="frameTickIn" name="I0" />
            <blockpin signalname="XLXN_82" name="I1" />
            <blockpin signalname="XLXN_162" name="O" />
        </block>
        <block symbolname="constant" name="XLXI_14">
            <attr value="1DF" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_29(9:0)" name="O" />
        </block>
        <block symbolname="slugComparator" name="XLXI_13">
            <blockpin signalname="monitorV_rowCount(9:0)" name="numberIn(9:0)" />
            <blockpin name="_LT" />
            <blockpin signalname="XLXN_83" name="_EQ" />
            <blockpin name="_GT" />
            <blockpin signalname="XLXN_29(9:0)" name="numberInTwo(9:0)" />
        </block>
        <block symbolname="and2" name="XLXI_81">
            <blockpin signalname="startStop" name="I0" />
            <blockpin signalname="XLXN_162" name="I1" />
            <blockpin signalname="XLXN_164" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_82">
            <blockpin signalname="resetToTop" name="I0" />
            <blockpin signalname="isEndOut" name="I1" />
            <blockpin signalname="XLXN_165" name="O" />
        </block>
        <block symbolname="cc16cle" name="XLXI_85">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="XLXN_164" name="CE" />
            <blockpin name="CLR" />
            <blockpin signalname="XLXN_198(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_165" name="L" />
            <blockpin name="CEO" />
            <blockpin signalname="qOutC(15:0)" name="Q(15:0)" />
            <blockpin name="TC" />
        </block>
        <block symbolname="constant" name="XLXI_96">
            <attr value="0" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_203" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_95(15:0)">
            <blockpin signalname="XLXN_203" name="D0" />
            <blockpin signalname="resetPosIn(15:0)" name="D1" />
            <blockpin signalname="resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop," name="S0" />
            <blockpin signalname="XLXN_198(15:0)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_77">
            <blockpin signalname="isEndOut" name="I" />
            <blockpin signalname="isEnd" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_79">
            <blockpin signalname="isHalfOut" name="I" />
            <blockpin signalname="isHalfway" name="O" />
        </block>
        <block symbolname="constant" name="XLXI_80">
            <attr value="F0" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_155(15:0)" name="O" />
        </block>
        <block symbolname="comp16" name="XLXI_76">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,meteorCountOut(9:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_155(15:0)" name="B(15:0)" />
            <blockpin signalname="isHalfOut" name="EQ" />
        </block>
        <block symbolname="buf" name="XLXI_86(9:0)">
            <blockpin signalname="qOutC(9:0)" name="I" />
            <blockpin signalname="meteorCountOut(9:0)" name="O" />
        </block>
        <block symbolname="meteorBitsTrue" name="XLXI_52">
            <blockpin signalname="_gnd,xPosWithAdjustment(8:0)" name="xPos(9:0)" />
            <blockpin signalname="meteorCountOut(9:0)" name="yPos(9:0)" />
            <blockpin signalname="monitorH_colCount(9:0)" name="columnCountIn(9:0)" />
            <blockpin signalname="monitorV_rowCount(9:0)" name="rowCountIn(9:0)" />
            <blockpin signalname="edgeBitsTrue" name="drawMeteorEdge" />
            <blockpin signalname="centerBitsTrue" name="drawMeteorCenter" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="160" y="2640" name="XLXI_24" orien="R0" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="288" y="2464" type="branch" />
            <wire x2="288" y1="2464" y2="2464" x1="224" />
            <wire x2="224" y1="2464" y2="2512" x1="224" />
        </branch>
        <instance x="160" y="2384" name="XLXI_84" orien="R0" />
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="288" y="2432" type="branch" />
            <wire x2="224" y1="2384" y2="2432" x1="224" />
            <wire x2="288" y1="2432" y2="2432" x1="224" />
        </branch>
        <rect width="348" x="104" y="2200" height="508" />
        <text style="fontsize:36;fontname:Arial" x="84" y="2232">Ground and Power Signal</text>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="2336" type="branch" />
            <wire x2="992" y1="2336" y2="2336" x1="976" />
        </branch>
        <branch name="meteorRandomPos(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1440" y="2336" type="branch" />
            <wire x2="1440" y1="2336" y2="2336" x1="1376" />
        </branch>
        <instance x="992" y="2368" name="XLXI_26" orien="R0">
        </instance>
        <rect width="2764" x="664" y="2124" height="596" />
        <text style="fontsize:36;fontname:Arial" x="1004" y="2160">Random Position Generator and Value Storage</text>
        <instance x="2160" y="2480" name="XLXI_88(15:0)" orien="R0" />
        <branch name="meteorRandomPos(15:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="2320" type="branch" />
            <wire x2="2160" y1="2320" y2="2320" x1="2144" />
        </branch>
        <branch name="XLXN_181(15:0)">
            <wire x2="2160" y1="2384" y2="2384" x1="2032" />
        </branch>
        <instance x="1888" y="2352" name="XLXI_89" orien="R0">
        </instance>
        <instance x="2784" y="2608" name="XLXI_87" orien="R0" />
        <branch name="XLXN_178(15:0)">
            <wire x2="2784" y1="2352" y2="2352" x1="2480" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2752" y="2480" type="branch" />
            <wire x2="2784" y1="2480" y2="2480" x1="2752" />
        </branch>
        <instance x="2160" y="2720" name="XLXI_91" orien="R0" />
        <branch name="XLXN_189">
            <wire x2="2528" y1="2592" y2="2592" x1="2480" />
            <wire x2="2528" y1="2416" y2="2592" x1="2528" />
            <wire x2="2784" y1="2416" y2="2416" x1="2528" />
        </branch>
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="2624" type="branch" />
            <wire x2="2160" y1="2624" y2="2624" x1="2144" />
        </branch>
        <branch name="isEndOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="2560" type="branch" />
            <wire x2="2160" y1="2560" y2="2560" x1="2144" />
        </branch>
        <branch name="resetToTop">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2128" y="2688" type="branch" />
            <wire x2="2160" y1="2688" y2="2688" x1="2128" />
        </branch>
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2784" y="2624" type="branch" />
            <wire x2="2784" y1="2576" y2="2624" x1="2784" />
        </branch>
        <branch name="q(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3264" y="2352" type="branch" />
            <wire x2="3264" y1="2352" y2="2352" x1="3168" />
        </branch>
        <rect width="804" x="1832" y="2136" height="572" />
        <text style="fontsize:36;fontname:Arial" x="1832" y="2164">Reset Signal Chooses RandomPosition or Preloaded</text>
        <branch name="resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2112" y="2496" type="branch" />
            <wire x2="2144" y1="2496" y2="2496" x1="2112" />
            <wire x2="2160" y1="2448" y2="2448" x1="2144" />
            <wire x2="2144" y1="2448" y2="2496" x1="2144" />
        </branch>
        <instance x="1456" y="1536" name="XLXI_40" orien="R0" />
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,meteorCountOut(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1408" y="1216" type="branch" />
            <wire x2="1456" y1="1216" y2="1216" x1="1408" />
        </branch>
        <branch name="XLXN_67(15:0)">
            <wire x2="1456" y1="1408" y2="1408" x1="1424" />
        </branch>
        <instance x="1280" y="1376" name="XLXI_41" orien="R0">
        </instance>
        <branch name="startStop">
            <wire x2="768" y1="656" y2="656" x1="368" />
        </branch>
        <branch name="resetToTop">
            <wire x2="784" y1="720" y2="720" x1="352" />
        </branch>
        <branch name="clkin">
            <wire x2="784" y1="816" y2="816" x1="368" />
        </branch>
        <branch name="monitorH_colCount(9:0)">
            <wire x2="768" y1="896" y2="896" x1="352" />
        </branch>
        <branch name="monitorV_rowCount(9:0)">
            <wire x2="768" y1="960" y2="960" x1="352" />
        </branch>
        <branch name="centerBitsTrue">
            <wire x2="2704" y1="1072" y2="1072" x1="2656" />
            <wire x2="3232" y1="656" y2="656" x1="2704" />
            <wire x2="3248" y1="656" y2="656" x1="3232" />
            <wire x2="2704" y1="656" y2="1072" x1="2704" />
        </branch>
        <branch name="isEndOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2016" y="1376" type="branch" />
            <wire x2="1984" y1="1312" y2="1312" x1="1840" />
            <wire x2="1984" y1="1312" y2="1376" x1="1984" />
            <wire x2="2016" y1="1376" y2="1376" x1="1984" />
            <wire x2="2080" y1="1312" y2="1312" x1="1984" />
            <wire x2="2080" y1="1312" y2="1616" x1="2080" />
            <wire x2="2176" y1="1616" y2="1616" x1="2080" />
        </branch>
        <branch name="resetPosIn(15:0)">
            <wire x2="736" y1="1104" y2="1104" x1="368" />
        </branch>
        <branch name="XLXN_198(15:0)">
            <wire x2="2960" y1="1424" y2="1424" x1="2912" />
            <wire x2="2960" y1="1424" y2="1616" x1="2960" />
            <wire x2="2976" y1="1616" y2="1616" x1="2960" />
        </branch>
        <branch name="xPosWithAdjustment(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2624" y="2032" type="branch" />
            <wire x2="2624" y1="2032" y2="2032" x1="2560" />
        </branch>
        <instance x="2336" y="2064" name="XLXI_83(15:0)" orien="R0" />
        <branch name="q(15:7),_vcc,q(5:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2320" y="2032" type="branch" />
            <wire x2="2336" y1="2032" y2="2032" x1="2320" />
        </branch>
        <rect width="924" x="2048" y="1900" height="180" />
        <text style="fontsize:36;fontname:Arial" x="2136" y="1924">random Positon renamed to xPosWithAdjustment</text>
        <branch name="XLXN_82">
            <wire x2="1296" y1="1712" y2="1776" x1="1296" />
            <wire x2="1312" y1="1776" y2="1776" x1="1296" />
        </branch>
        <instance x="912" y="1808" name="XLXI_51" orien="R0">
        </instance>
        <instance x="1312" y="1904" name="XLXI_15" orien="R0" />
        <instance x="176" y="1856" name="XLXI_14" orien="R0">
        </instance>
        <instance x="400" y="1856" name="XLXI_13" orien="R0">
        </instance>
        <branch name="frameTickIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1280" y="1840" type="branch" />
            <wire x2="1312" y1="1840" y2="1840" x1="1280" />
        </branch>
        <branch name="monitorV_rowCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="304" y="1808" type="branch" />
            <wire x2="352" y1="1808" y2="1808" x1="304" />
            <wire x2="352" y1="1808" y2="1824" x1="352" />
            <wire x2="400" y1="1824" y2="1824" x1="352" />
        </branch>
        <branch name="XLXN_29(9:0)">
            <wire x2="400" y1="1888" y2="1888" x1="320" />
        </branch>
        <branch name="XLXN_83">
            <wire x2="848" y1="1760" y2="1760" x1="784" />
            <wire x2="848" y1="1712" y2="1760" x1="848" />
            <wire x2="912" y1="1712" y2="1712" x1="848" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="832" y="1968" type="branch" />
            <wire x2="912" y1="1968" y2="1968" x1="832" />
            <wire x2="912" y1="1776" y2="1968" x1="912" />
        </branch>
        <rect width="1544" x="60" y="1536" height="528" />
        <text style="fontsize:36;fontname:Arial" x="536" y="1568">Two Signal Per Frame Generator</text>
        <branch name="startStop">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="1856" type="branch" />
            <wire x2="2080" y1="1856" y2="1856" x1="2064" />
        </branch>
        <instance x="2080" y="1920" name="XLXI_81" orien="R0" />
        <branch name="XLXN_162">
            <wire x2="2080" y1="1808" y2="1808" x1="1568" />
            <wire x2="2080" y1="1792" y2="1808" x1="2080" />
        </branch>
        <rect width="1180" x="964" y="1000" height="468" />
        <text style="fontsize:36;fontname:Arial" x="1016" y="1036">Go To Top When Meteor Count At End Of Screen Signal Generator</text>
        <text style="fontsize:36;fontname:Arial" x="1096" y="1320">End of Screen Constant</text>
        <rect width="396" x="1092" y="1276" height="172" />
        <branch name="resetToTop">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="1680" type="branch" />
            <wire x2="2176" y1="1680" y2="1680" x1="1984" />
        </branch>
        <instance x="2176" y="1744" name="XLXI_82" orien="R0" />
        <rect width="688" x="1796" y="1544" height="172" />
        <text style="fontsize:36;fontname:Arial" x="2000" y="1568">Reset Override</text>
        <rect width="704" x="1800" y="1724" height="172" />
        <text style="fontsize:36;fontname:Arial" x="1784" y="1752">Assert Start Stop Signal with Signal Per Frame</text>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="1872" type="branch" />
            <wire x2="2976" y1="1872" y2="1872" x1="2960" />
        </branch>
        <branch name="qOutC(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3392" y="1616" type="branch" />
            <wire x2="3392" y1="1616" y2="1616" x1="3360" />
        </branch>
        <instance x="2976" y="2000" name="XLXI_85" orien="R0" />
        <branch name="XLXN_164">
            <wire x2="2352" y1="1824" y2="1824" x1="2336" />
            <wire x2="2976" y1="1808" y2="1808" x1="2352" />
            <wire x2="2352" y1="1808" y2="1824" x1="2352" />
        </branch>
        <branch name="XLXN_165">
            <wire x2="2448" y1="1648" y2="1648" x1="2432" />
            <wire x2="2448" y1="1648" y2="1744" x1="2448" />
            <wire x2="2976" y1="1744" y2="1744" x1="2448" />
        </branch>
        <branch name="XLXN_203">
            <wire x2="2592" y1="1392" y2="1392" x1="2576" />
        </branch>
        <branch name="resetPosIn(15:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2576" y="1456" type="branch" />
            <wire x2="2592" y1="1456" y2="1456" x1="2576" />
        </branch>
        <instance x="2432" y="1360" name="XLXI_96" orien="R0">
        </instance>
        <branch name="resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,resetToTop,">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2576" y="1520" type="branch" />
            <wire x2="2592" y1="1520" y2="1520" x1="2576" />
        </branch>
        <instance x="2592" y="1552" name="XLXI_95(15:0)" orien="R0" />
        <branch name="isHalfway">
            <wire x2="3264" y1="720" y2="720" x1="3120" />
        </branch>
        <branch name="isEnd">
            <wire x2="3264" y1="800" y2="800" x1="3120" />
        </branch>
        <instance x="2896" y="832" name="XLXI_77" orien="R0" />
        <branch name="isEndOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2864" y="800" type="branch" />
            <wire x2="2896" y1="800" y2="800" x1="2864" />
        </branch>
        <instance x="2896" y="752" name="XLXI_79" orien="R0" />
        <branch name="isHalfOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2848" y="720" type="branch" />
            <wire x2="2896" y1="720" y2="720" x1="2848" />
        </branch>
        <branch name="_gnd,_gnd,_gnd,_gnd,_gnd,_gnd,meteorCountOut(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1056" y="736" type="branch" />
            <wire x2="1120" y1="736" y2="736" x1="1056" />
        </branch>
        <branch name="XLXN_155(15:0)">
            <wire x2="1120" y1="928" y2="928" x1="1008" />
        </branch>
        <instance x="864" y="896" name="XLXI_80" orien="R0">
        </instance>
        <branch name="isHalfOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1600" y="832" type="branch" />
            <wire x2="1600" y1="832" y2="832" x1="1504" />
        </branch>
        <instance x="1120" y="1056" name="XLXI_76" orien="R0" />
        <rect width="868" x="844" y="556" height="432" />
        <text style="fontsize:36;fontname:Arial" x="888" y="584">Halfway Screen Signal Gen</text>
        <branch name="meteorCountOut(9:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3328" y="1392" type="branch" />
            <wire x2="3312" y1="1392" y2="1392" x1="3296" />
            <wire x2="3328" y1="1392" y2="1392" x1="3312" />
        </branch>
        <branch name="qOutC(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3056" y="1392" type="branch" />
            <wire x2="3072" y1="1392" y2="1392" x1="3056" />
        </branch>
        <instance x="3072" y="1424" name="XLXI_86(9:0)" orien="R0" />
        <rect width="688" x="2220" y="1200" height="368" />
        <text style="fontsize:36;fontname:Arial" x="2248" y="1256">Choose Data To Load Based On resetToTop Input</text>
        <rect width="964" x="1772" y="516" height="464" />
        <branch name="edgeBitsTrue">
            <wire x2="2672" y1="1008" y2="1008" x1="2656" />
            <wire x2="3232" y1="592" y2="592" x1="2672" />
            <wire x2="3248" y1="592" y2="592" x1="3232" />
            <wire x2="2672" y1="592" y2="1008" x1="2672" />
        </branch>
        <instance x="2160" y="976" name="XLXI_52" orien="R0">
        </instance>
        <branch name="monitorH_colCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2128" y="944" type="branch" />
            <wire x2="2160" y1="944" y2="944" x1="2128" />
        </branch>
        <branch name="monitorV_rowCount(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2128" y="880" type="branch" />
            <wire x2="2160" y1="880" y2="880" x1="2128" />
        </branch>
        <branch name="meteorCountOut(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="816" type="branch" />
            <wire x2="2160" y1="816" y2="816" x1="2144" />
        </branch>
        <branch name="_gnd,xPosWithAdjustment(8:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2128" y="752" type="branch" />
            <wire x2="2160" y1="752" y2="752" x1="2128" />
        </branch>
        <rect width="592" x="2908" y="508" height="460" />
        <text style="fontsize:36;fontname:Arial" x="3100" y="924">Module Outputs</text>
        <text style="fontsize:36;fontname:Arial" x="332" y="592">Module Inputs</text>
        <text style="fontsize:36;fontname:Arial" x="1936" y="548">|U - X| + |V - Y|&lt; 64  EQUATION ASSERT</text>
        <rect width="832" x="4" y="560" height="584" />
        <iomarker fontsize="28" x="368" y="656" name="startStop" orien="R180" />
        <iomarker fontsize="28" x="352" y="720" name="resetToTop" orien="R180" />
        <iomarker fontsize="28" x="368" y="816" name="clkin" orien="R180" />
        <iomarker fontsize="28" x="352" y="896" name="monitorH_colCount(9:0)" orien="R180" />
        <iomarker fontsize="28" x="352" y="960" name="monitorV_rowCount(9:0)" orien="R180" />
        <iomarker fontsize="28" x="3248" y="592" name="edgeBitsTrue" orien="R0" />
        <iomarker fontsize="28" x="3248" y="656" name="centerBitsTrue" orien="R0" />
        <iomarker fontsize="28" x="368" y="1104" name="resetPosIn(15:0)" orien="R180" />
        <iomarker fontsize="28" x="3264" y="720" name="isHalfway" orien="R0" />
        <iomarker fontsize="28" x="3264" y="800" name="isEnd" orien="R0" />
        <text style="fontsize:40;fontname:Arial" x="1968" y="76">DiamondHandler: Generates random diamond on screen. Handles x and y position</text>
        <text style="fontsize:64;fontname:Arial" x="56" y="60">Schematic 7 Diamond Handler</text>
    </sheet>
</drawing>