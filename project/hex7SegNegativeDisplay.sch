<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="B" />
        <signal name="C" />
        <signal name="D" />
        <signal name="E" />
        <signal name="F" />
        <signal name="G" />
        <signal name="_VCC" />
        <signal name="A" />
        <signal name="XLXN_18" />
        <signal name="negative" />
        <port polarity="Output" name="B" />
        <port polarity="Output" name="C" />
        <port polarity="Output" name="D" />
        <port polarity="Output" name="E" />
        <port polarity="Output" name="F" />
        <port polarity="Output" name="G" />
        <port polarity="Output" name="_VCC" />
        <port polarity="Output" name="A" />
        <port polarity="Input" name="negative" />
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="vcc" name="XLXI_1">
            <blockpin signalname="_VCC" name="P" />
        </block>
        <block symbolname="buf" name="XLXI_2">
            <blockpin signalname="_VCC" name="I" />
            <blockpin signalname="A" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_3">
            <blockpin signalname="_VCC" name="I" />
            <blockpin signalname="B" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_4">
            <blockpin signalname="_VCC" name="I" />
            <blockpin signalname="C" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_5">
            <blockpin signalname="_VCC" name="I" />
            <blockpin signalname="D" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_6">
            <blockpin signalname="_VCC" name="I" />
            <blockpin signalname="E" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_7">
            <blockpin signalname="_VCC" name="I" />
            <blockpin signalname="F" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_8">
            <blockpin signalname="XLXN_18" name="I" />
            <blockpin signalname="G" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_10">
            <blockpin signalname="negative" name="I" />
            <blockpin signalname="XLXN_18" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="A">
            <wire x2="2816" y1="256" y2="256" x1="2352" />
        </branch>
        <branch name="B">
            <wire x2="2816" y1="352" y2="352" x1="2368" />
        </branch>
        <branch name="C">
            <wire x2="2816" y1="432" y2="432" x1="2368" />
        </branch>
        <branch name="D">
            <wire x2="2800" y1="512" y2="512" x1="2368" />
        </branch>
        <branch name="E">
            <wire x2="2816" y1="608" y2="608" x1="2368" />
        </branch>
        <branch name="F">
            <wire x2="2848" y1="704" y2="704" x1="2368" />
        </branch>
        <branch name="G">
            <wire x2="2864" y1="816" y2="816" x1="2368" />
        </branch>
        <iomarker fontsize="28" x="2816" y="256" name="A" orien="R0" />
        <iomarker fontsize="28" x="2816" y="352" name="B" orien="R0" />
        <iomarker fontsize="28" x="2816" y="432" name="C" orien="R0" />
        <iomarker fontsize="28" x="2800" y="512" name="D" orien="R0" />
        <iomarker fontsize="28" x="2816" y="608" name="E" orien="R0" />
        <iomarker fontsize="28" x="2848" y="704" name="F" orien="R0" />
        <iomarker fontsize="28" x="2864" y="816" name="G" orien="R0" />
        <instance x="320" y="976" name="XLXI_1" orien="R0" />
        <branch name="_VCC">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="464" y="992" type="branch" />
            <wire x2="384" y1="976" y2="992" x1="384" />
            <wire x2="464" y1="992" y2="992" x1="384" />
        </branch>
        <instance x="2128" y="288" name="XLXI_2" orien="R0" />
        <instance x="2144" y="384" name="XLXI_3" orien="R0" />
        <instance x="2144" y="464" name="XLXI_4" orien="R0" />
        <instance x="2144" y="544" name="XLXI_5" orien="R0" />
        <instance x="2144" y="640" name="XLXI_6" orien="R0" />
        <instance x="2144" y="736" name="XLXI_7" orien="R0" />
        <instance x="2144" y="848" name="XLXI_8" orien="R0" />
        <branch name="_VCC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2096" y="256" type="branch" />
            <wire x2="2128" y1="256" y2="256" x1="2096" />
        </branch>
        <branch name="_VCC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2096" y="352" type="branch" />
            <wire x2="2144" y1="352" y2="352" x1="2096" />
        </branch>
        <branch name="_VCC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2096" y="432" type="branch" />
            <wire x2="2144" y1="432" y2="432" x1="2096" />
        </branch>
        <branch name="_VCC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2096" y="512" type="branch" />
            <wire x2="2144" y1="512" y2="512" x1="2096" />
        </branch>
        <branch name="_VCC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2096" y="608" type="branch" />
            <wire x2="2144" y1="608" y2="608" x1="2096" />
        </branch>
        <branch name="_VCC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2096" y="704" type="branch" />
            <wire x2="2144" y1="704" y2="704" x1="2096" />
        </branch>
        <iomarker fontsize="28" x="160" y="256" name="negative" orien="R180" />
        <branch name="XLXN_18">
            <wire x2="2144" y1="816" y2="816" x1="2112" />
        </branch>
        <instance x="1888" y="848" name="XLXI_10" orien="R0" />
        <branch name="negative">
            <wire x2="1024" y1="256" y2="256" x1="160" />
            <wire x2="1024" y1="256" y2="816" x1="1024" />
            <wire x2="1888" y1="816" y2="816" x1="1024" />
        </branch>
    </sheet>
</drawing>