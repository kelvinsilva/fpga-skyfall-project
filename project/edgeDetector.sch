<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="_input" />
        <signal name="clkin" />
        <signal name="XLXN_3" />
        <signal name="XLXN_5" />
        <signal name="signalOut" />
        <port polarity="Input" name="_input" />
        <port polarity="Input" name="clkin" />
        <port polarity="Output" name="signalOut" />
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="fd" name="XLXI_2">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="_input" name="D" />
            <blockpin signalname="XLXN_3" name="Q" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="XLXN_5" name="I0" />
            <blockpin signalname="_input" name="I1" />
            <blockpin signalname="signalOut" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_4">
            <blockpin signalname="XLXN_3" name="I" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="_input">
            <wire x2="816" y1="368" y2="368" x1="400" />
            <wire x2="960" y1="368" y2="368" x1="816" />
            <wire x2="1024" y1="368" y2="368" x1="960" />
            <wire x2="2016" y1="128" y2="128" x1="816" />
            <wire x2="816" y1="128" y2="368" x1="816" />
        </branch>
        <iomarker fontsize="28" x="400" y="368" name="_input" orien="R180" />
        <instance x="1024" y="624" name="XLXI_2" orien="R0" />
        <branch name="clkin">
            <wire x2="1024" y1="496" y2="496" x1="448" />
        </branch>
        <iomarker fontsize="28" x="448" y="496" name="clkin" orien="R180" />
        <branch name="XLXN_3">
            <wire x2="1632" y1="368" y2="368" x1="1408" />
        </branch>
        <instance x="2016" y="256" name="XLXI_3" orien="R0" />
        <instance x="1632" y="400" name="XLXI_4" orien="R0" />
        <branch name="XLXN_5">
            <wire x2="1936" y1="368" y2="368" x1="1856" />
            <wire x2="1936" y1="192" y2="368" x1="1936" />
            <wire x2="2016" y1="192" y2="192" x1="1936" />
        </branch>
        <branch name="signalOut">
            <wire x2="2320" y1="160" y2="160" x1="2272" />
        </branch>
        <iomarker fontsize="28" x="2320" y="160" name="signalOut" orien="R0" />
        <text style="fontsize:64;fontname:Arial" x="84" y="60">Schematic 10 Edge Detector</text>
    </sheet>
</drawing>