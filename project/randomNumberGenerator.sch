<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="R0" />
        <signal name="R1" />
        <signal name="R2" />
        <signal name="R3" />
        <signal name="R4" />
        <signal name="R5" />
        <signal name="R7" />
        <signal name="R6" />
        <signal name="clk" />
        <signal name="XLXN_23" />
        <signal name="XLXN_26" />
        <port polarity="Output" name="R0" />
        <port polarity="Output" name="R1" />
        <port polarity="Output" name="R2" />
        <port polarity="Output" name="R3" />
        <port polarity="Output" name="R4" />
        <port polarity="Output" name="R5" />
        <port polarity="Output" name="R7" />
        <port polarity="Output" name="R6" />
        <port polarity="Input" name="clk" />
        <blockdef name="xor4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="60" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="208" y1="-160" y2="-160" x1="256" />
            <arc ex="64" ey="-208" sx="64" sy="-112" r="56" cx="32" cy="-160" />
            <line x2="64" y1="-208" y2="-208" x1="128" />
            <line x2="64" y1="-112" y2="-112" x1="128" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <arc ex="128" ey="-208" sx="208" sy="-160" r="88" cx="132" cy="-120" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="208" ey="-160" sx="128" sy="-112" r="88" cx="132" cy="-200" />
        </blockdef>
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="xor4" name="XLXI_1">
            <blockpin signalname="R0" name="I0" />
            <blockpin signalname="R5" name="I1" />
            <blockpin signalname="R6" name="I2" />
            <blockpin signalname="R7" name="I3" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_2">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="XLXN_1" name="D" />
            <blockpin signalname="R0" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_3">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="R0" name="D" />
            <blockpin signalname="R1" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_4">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="R1" name="D" />
            <blockpin signalname="R2" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_5">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="R2" name="D" />
            <blockpin signalname="R3" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_6">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="R3" name="D" />
            <blockpin signalname="R4" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_7">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="R4" name="D" />
            <blockpin signalname="R5" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_9">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="R5" name="D" />
            <blockpin signalname="R6" name="Q" />
        </block>
        <block symbolname="inv" name="XLXI_11">
            <blockpin signalname="R6" name="I" />
            <blockpin signalname="XLXN_23" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_12">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="XLXN_23" name="D" />
            <blockpin signalname="XLXN_26" name="Q" />
        </block>
        <block symbolname="inv" name="XLXI_13">
            <blockpin signalname="XLXN_26" name="I" />
            <blockpin signalname="R7" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="336" y="944" name="XLXI_2" orien="R0" />
        <instance x="768" y="944" name="XLXI_3" orien="R0" />
        <instance x="1216" y="944" name="XLXI_4" orien="R0" />
        <instance x="1648" y="944" name="XLXI_5" orien="R0" />
        <instance x="2064" y="944" name="XLXI_6" orien="R0" />
        <instance x="2496" y="944" name="XLXI_7" orien="R0" />
        <instance x="2912" y="944" name="XLXI_9" orien="R0" />
        <instance x="64" y="848" name="XLXI_1" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="336" y1="688" y2="688" x1="320" />
        </branch>
        <branch name="R0">
            <wire x2="736" y1="688" y2="688" x1="720" />
            <wire x2="768" y1="688" y2="688" x1="736" />
            <wire x2="736" y1="576" y2="688" x1="736" />
        </branch>
        <branch name="R1">
            <wire x2="1200" y1="688" y2="688" x1="1152" />
            <wire x2="1216" y1="688" y2="688" x1="1200" />
            <wire x2="1200" y1="592" y2="688" x1="1200" />
        </branch>
        <branch name="R2">
            <wire x2="1632" y1="688" y2="688" x1="1600" />
            <wire x2="1648" y1="688" y2="688" x1="1632" />
            <wire x2="1632" y1="608" y2="688" x1="1632" />
        </branch>
        <branch name="R3">
            <wire x2="2048" y1="688" y2="688" x1="2032" />
            <wire x2="2064" y1="688" y2="688" x1="2048" />
            <wire x2="2048" y1="592" y2="688" x1="2048" />
        </branch>
        <branch name="R4">
            <wire x2="2464" y1="688" y2="688" x1="2448" />
            <wire x2="2496" y1="688" y2="688" x1="2464" />
            <wire x2="2464" y1="592" y2="688" x1="2464" />
        </branch>
        <branch name="R5">
            <wire x2="2896" y1="688" y2="688" x1="2880" />
            <wire x2="2912" y1="688" y2="688" x1="2896" />
            <wire x2="2896" y1="608" y2="688" x1="2896" />
        </branch>
        <branch name="R6">
            <wire x2="2912" y1="928" y2="928" x1="2896" />
            <wire x2="3296" y1="928" y2="928" x1="2912" />
            <wire x2="3312" y1="928" y2="928" x1="3296" />
            <wire x2="2896" y1="928" y2="960" x1="2896" />
            <wire x2="3312" y1="688" y2="688" x1="3296" />
            <wire x2="3312" y1="688" y2="848" x1="3312" />
            <wire x2="3312" y1="848" y2="928" x1="3312" />
            <wire x2="3360" y1="848" y2="848" x1="3312" />
            <wire x2="3360" y1="592" y2="848" x1="3360" />
        </branch>
        <branch name="R7">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="64" y="576" type="branch" />
            <wire x2="64" y1="576" y2="592" x1="64" />
        </branch>
        <branch name="R6">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="48" y="656" type="branch" />
            <wire x2="64" y1="656" y2="656" x1="48" />
        </branch>
        <branch name="R5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="48" y="720" type="branch" />
            <wire x2="64" y1="720" y2="720" x1="48" />
        </branch>
        <branch name="R0">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="64" y="800" type="branch" />
            <wire x2="64" y1="784" y2="800" x1="64" />
        </branch>
        <branch name="clk">
            <wire x2="304" y1="1360" y2="1360" x1="176" />
            <wire x2="736" y1="1360" y2="1360" x1="304" />
            <wire x2="1168" y1="1360" y2="1360" x1="736" />
            <wire x2="1616" y1="1360" y2="1360" x1="1168" />
            <wire x2="2048" y1="1360" y2="1360" x1="1616" />
            <wire x2="2352" y1="1360" y2="1360" x1="2048" />
            <wire x2="2464" y1="1360" y2="1360" x1="2352" />
            <wire x2="2704" y1="1360" y2="1360" x1="2464" />
            <wire x2="2912" y1="1360" y2="1360" x1="2704" />
            <wire x2="3104" y1="1360" y2="1360" x1="2912" />
            <wire x2="336" y1="816" y2="816" x1="304" />
            <wire x2="304" y1="816" y2="1360" x1="304" />
            <wire x2="768" y1="816" y2="816" x1="736" />
            <wire x2="736" y1="816" y2="1360" x1="736" />
            <wire x2="1216" y1="816" y2="816" x1="1168" />
            <wire x2="1168" y1="816" y2="1360" x1="1168" />
            <wire x2="1648" y1="816" y2="816" x1="1616" />
            <wire x2="1616" y1="816" y2="1360" x1="1616" />
            <wire x2="2048" y1="816" y2="1360" x1="2048" />
            <wire x2="2064" y1="816" y2="816" x1="2048" />
            <wire x2="2496" y1="816" y2="816" x1="2464" />
            <wire x2="2464" y1="816" y2="1360" x1="2464" />
            <wire x2="2704" y1="912" y2="1360" x1="2704" />
            <wire x2="2896" y1="912" y2="912" x1="2704" />
            <wire x2="2832" y1="1200" y2="1200" x1="2816" />
            <wire x2="2816" y1="1200" y2="1312" x1="2816" />
            <wire x2="2912" y1="1312" y2="1312" x1="2816" />
            <wire x2="2912" y1="1312" y2="1360" x1="2912" />
            <wire x2="2896" y1="816" y2="912" x1="2896" />
            <wire x2="2912" y1="816" y2="816" x1="2896" />
        </branch>
        <iomarker fontsize="28" x="736" y="576" name="R0" orien="R270" />
        <iomarker fontsize="28" x="1200" y="592" name="R1" orien="R270" />
        <iomarker fontsize="28" x="1632" y="608" name="R2" orien="R270" />
        <iomarker fontsize="28" x="2048" y="592" name="R3" orien="R270" />
        <iomarker fontsize="28" x="2464" y="592" name="R4" orien="R270" />
        <iomarker fontsize="28" x="2896" y="608" name="R5" orien="R270" />
        <iomarker fontsize="28" x="3360" y="592" name="R6" orien="R270" />
        <iomarker fontsize="28" x="176" y="1360" name="clk" orien="R180" />
        <instance x="2896" y="992" name="XLXI_11" orien="R0" />
        <instance x="2832" y="1328" name="XLXI_12" orien="R0" />
        <branch name="XLXN_23">
            <wire x2="2832" y1="1072" y2="1072" x1="2768" />
            <wire x2="2768" y1="1072" y2="1280" x1="2768" />
            <wire x2="3280" y1="1280" y2="1280" x1="2768" />
            <wire x2="3280" y1="960" y2="960" x1="3120" />
            <wire x2="3280" y1="960" y2="1280" x1="3280" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="3392" y1="1072" y2="1072" x1="3216" />
            <wire x2="3392" y1="1072" y2="1280" x1="3392" />
            <wire x2="3392" y1="1280" y2="1296" x1="3392" />
            <wire x2="3232" y1="1296" y2="1296" x1="3216" />
            <wire x2="3392" y1="1296" y2="1296" x1="3232" />
            <wire x2="3216" y1="1296" y2="1360" x1="3216" />
            <wire x2="3232" y1="1360" y2="1360" x1="3216" />
        </branch>
        <instance x="3232" y="1392" name="XLXI_13" orien="R0" />
        <branch name="R7">
            <wire x2="3456" y1="576" y2="624" x1="3456" />
            <wire x2="3472" y1="624" y2="624" x1="3456" />
            <wire x2="3472" y1="624" y2="1360" x1="3472" />
            <wire x2="3472" y1="1360" y2="1360" x1="3456" />
        </branch>
        <iomarker fontsize="28" x="3456" y="576" name="R7" orien="R270" />
        <text style="fontsize:64;fontname:Arial" x="28" y="68">Schematic 15, Random Number Generator</text>
    </sheet>
</drawing>