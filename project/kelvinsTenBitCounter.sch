<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_11" />
        <signal name="CLK" />
        <signal name="R" />
        <signal name="q_begin(7:0)" />
        <signal name="q_last0" />
        <signal name="q_last1" />
        <signal name="q_last1,q_last0,q_begin(7:0)" />
        <signal name="outCount(9:0)" />
        <signal name="TC" />
        <signal name="TC0" />
        <signal name="TC1" />
        <signal name="CE" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="R" />
        <port polarity="Output" name="outCount(9:0)" />
        <port polarity="Output" name="TC" />
        <port polarity="Input" name="CE" />
        <blockdef name="cb8re">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <rect width="64" x="320" y="-268" height="24" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="cb2re">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <line x2="320" y1="-320" y2="-320" x1="384" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <rect width="256" x="64" y="-384" height="320" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <block symbolname="cb8re" name="XLXI_1">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="R" name="R" />
            <blockpin signalname="XLXN_11" name="CEO" />
            <blockpin signalname="q_begin(7:0)" name="Q(7:0)" />
            <blockpin signalname="TC0" name="TC" />
        </block>
        <block symbolname="cb2re" name="XLXI_3">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_11" name="CE" />
            <blockpin signalname="R" name="R" />
            <blockpin name="CEO" />
            <blockpin signalname="q_last0" name="Q0" />
            <blockpin signalname="q_last1" name="Q1" />
            <blockpin signalname="TC1" name="TC" />
        </block>
        <block symbolname="buf" name="XLXI_4(9:0)">
            <blockpin signalname="q_last1,q_last0,q_begin(7:0)" name="I" />
            <blockpin signalname="outCount(9:0)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_9">
            <blockpin signalname="TC1" name="I0" />
            <blockpin signalname="TC0" name="I1" />
            <blockpin signalname="TC" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="544" y="576" name="XLXI_1" orien="R0" />
        <instance x="544" y="1056" name="XLXI_3" orien="R0" />
        <branch name="XLXN_11">
            <wire x2="1008" y1="608" y2="608" x1="512" />
            <wire x2="512" y1="608" y2="864" x1="512" />
            <wire x2="544" y1="864" y2="864" x1="512" />
            <wire x2="1008" y1="384" y2="384" x1="928" />
            <wire x2="1008" y1="384" y2="608" x1="1008" />
        </branch>
        <branch name="CLK">
            <wire x2="336" y1="448" y2="448" x1="304" />
            <wire x2="464" y1="448" y2="448" x1="336" />
            <wire x2="544" y1="448" y2="448" x1="464" />
            <wire x2="464" y1="448" y2="928" x1="464" />
            <wire x2="544" y1="928" y2="928" x1="464" />
        </branch>
        <branch name="R">
            <wire x2="336" y1="544" y2="544" x1="304" />
            <wire x2="368" y1="544" y2="544" x1="336" />
            <wire x2="544" y1="544" y2="544" x1="368" />
            <wire x2="368" y1="544" y2="1024" x1="368" />
            <wire x2="544" y1="1024" y2="1024" x1="368" />
        </branch>
        <branch name="q_begin(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="960" y="320" type="branch" />
            <wire x2="960" y1="320" y2="320" x1="928" />
        </branch>
        <branch name="q_last0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="736" type="branch" />
            <wire x2="976" y1="736" y2="736" x1="928" />
        </branch>
        <branch name="q_last1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="800" type="branch" />
            <wire x2="992" y1="800" y2="800" x1="928" />
        </branch>
        <text style="fontsize:44;fontname:Arial" x="620" y="104">0 to 1023, total 1024 values</text>
        <branch name="q_last1,q_last0,q_begin(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1456" y="400" type="branch" />
            <wire x2="1744" y1="400" y2="400" x1="1456" />
        </branch>
        <instance x="1744" y="432" name="XLXI_4(9:0)" orien="R0" />
        <branch name="outCount(9:0)">
            <wire x2="2000" y1="400" y2="400" x1="1968" />
        </branch>
        <iomarker fontsize="28" x="2000" y="400" name="outCount(9:0)" orien="R0" />
        <branch name="TC">
            <wire x2="2000" y1="576" y2="576" x1="1488" />
        </branch>
        <iomarker fontsize="28" x="2000" y="576" name="TC" orien="R0" />
        <branch name="TC0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="448" type="branch" />
            <wire x2="944" y1="448" y2="448" x1="928" />
        </branch>
        <branch name="TC1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="928" type="branch" />
            <wire x2="944" y1="928" y2="928" x1="928" />
        </branch>
        <instance x="1232" y="672" name="XLXI_9" orien="R0" />
        <branch name="TC0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1216" y="544" type="branch" />
            <wire x2="1232" y1="544" y2="544" x1="1216" />
        </branch>
        <branch name="TC1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1216" y="608" type="branch" />
            <wire x2="1232" y1="608" y2="608" x1="1216" />
        </branch>
        <iomarker fontsize="28" x="304" y="544" name="R" orien="R180" />
        <iomarker fontsize="28" x="304" y="448" name="CLK" orien="R180" />
        <branch name="CE">
            <wire x2="544" y1="384" y2="384" x1="320" />
        </branch>
        <iomarker fontsize="28" x="320" y="384" name="CE" orien="R180" />
        <text style="fontsize:64;fontname:Arial" x="72" y="36">Schematic 9 Kelvins Ten Bit Counter</text>
    </sheet>
</drawing>