// Verilog test fixture created to test sync signals
//  Instructions:
// 1. Add to your project.
// 2. Adjust instantiation of UUT (line 31).
// 3. Comment out lines 107 and 131 if your outputs are not run through FFs.
// 3. Check Syntax.
// 4. Start ISIM.
// 5. Add SERRORS and RGBERRORS to the waveform viewer (under GUT and RUT)
// 6. Reset simulation time to 0
// 7. Set step size to 17ms
// 8. Simulate one step (may take 10 seconds, or more if you have more components.
// 9. If SERRORs or RGBERRORS is not 0 find where they changed: that's a problem.
//10. Otherwise simulate one more step and check them again. 
//
//   Martine  Fri Feb 20 17:31:58 2015

`timescale 1ns / 1ps

module testSyncs_top_sch_tb();

// Inputs
   reg gsr;
   reg betterclk;
   reg clkin;
   reg sw7;

// Output
   wire Red2;
   wire Red1;
   wire Red0;
   wire Blue0;
   wire Blue1;
   wire Green2;
   wire Green0;
   wire Green1;
   wire vsync;
   wire hsync;
   wire led7;
	wire oops;


// You may need to replace the instantiation below
// to match your top level schematic and its ports.
// Replace "top" by the name of your top level module
// and adjust port names:
// for example if your switch 7 input is "SWCH7" then 
// replace ".sw7(sw7),"  by ".SWCH7(sw7)," .
// Instantiate the UUT
   top UUT (
		.gsr(gsr), 
		.Red2(Red2), 
		.Red1(Red1), 
		.Red0(Red0), 
		.betterclk(betterclk), 
		.clkin(clkin), 
		.Blue0(Blue0), 
		.Blue1(Blue1), 
		.Green2(Green2), 
		.Green0(Green0), 
		.Green1(Green1), 
		.vsync(vsync), 
		.hsync(vsync), 
		.sw7(sw7)
		//.led7(led7)
   );
	
	reg good_HS;
	reg good_VS;
	reg activeH;
	reg activeV;
 
// Clock parameters
	parameter PERIOD = 20;
   parameter real DUTY_CYCLE = 0.5;
   parameter OFFSET = 3;

	initial // Clock process for betterclk, sw7 is set to 1 to select this clock
	begin
	   betterclk = 0;
		clkin = 1'b0;
		sw7 = 1;
	   #OFFSET
		betterclk = 1'b1;
      forever
         begin
            #(PERIOD-(PERIOD*DUTY_CYCLE)) betterclk = ~betterclk;
         end
	end
	
	initial // only input needed is a GSR pulse
	  begin // if you have others, set them to 0 or 1
       gsr = 1'b0;
		 #600 gsr = 1'b1;
	    #80 gsr = 1'b0;
	  end
	 
   // process to generate correct values for HS and activeH
   initial
	  begin
	    #OFFSET;
	    #0.1;
		 activeH = 1'b1;
       good_HS = 1'b1;
	    #700; // to get past gsr pulse and clock delay
		 // comment out the line below if your HSync is not passed through a
		 // D FF before leaving the FPGA
	    //#40;  // one more clock for FF delay output
	    forever 
		    begin
			  activeH = 1'b1;
		     #(640*40);
			  activeH = 1'b0;
	        #(15*40);
	        good_HS = 1'b0;
	        #((96)*40);
	        good_HS = 1'b1;
	        #(49*40);
	       end
     end

   //correct values for VS and activeV
   initial
	  begin
	    #OFFSET;
	    #0.1;
		 activeV = 1'b1;
	    good_VS = 1'b1;
	    #700; // to get past gsr and DCM clock delay
       // comment out the line below if your VSync is not passed through a
		 // D FF before leaving the FPGA
	  //  #40;  // one more clock for FF delay output
		 forever 
		    begin
            activeV = 1'b1;
            #(480*800*40);
            activeV=1'b0;		 
		      #(9*800*40); 
	         good_VS = 1'b0;
	         #(2*800*40);
	         good_VS = 1'b1;
	         #(34*800*40);
	       end
     end



// Instantiate the module comparing good and actual sync signals
   check_the_sync_signals GUT (
		.myHS(HS), 
		.myVS(VS), 
		.correct_HS(good_HS), 
		.correct_VS(good_VS),
		.clk(betterclk),
		.gsr(gsr),
		.sync_error(oops)
   );
	
// Instantiate the module comparing checking RGB signals are 
// low outside the active region
   check_the_rgb_signals RUT (
	    .VR2(VR2), .VR1(VR1), .VR0(VR0), 
		 .VB0(VB0), .VB1(VB1), .VG2(VG2), .VG1(VG1), .VG0(VG0),
       .activeH(activeH), .activeV(activeV),
	    .clk(betterclk),
	 	 .gsr(gsr),
		 .rgb_error(rgb_oops)
   );	

endmodule

module check_the_sync_signals(
 input myHS,
 input myVS,
 input correct_HS,
 input correct_VS,
 input clk,
 input gsr,
 output sync_error);
 
   // sync_error is high when actual and expected sync signals differ
  assign sync_error = (myHS^correct_HS)|(myVS^correct_VS);
 
   // SERRORS is incremented when sync_error is high at the rising edge of betterclk 
  integer SERRORS; 
  
   // since betterclk is divided by 2 in clkcntrl4 the error count 
	// will be double (each error will be counted twice)	
  always @(posedge clk)
    begin
	    SERRORS  <= SERRORS + sync_error;   // non-blocking assignment
	 end
	 
	 // reset SERRORS on rising edge of gsr
  always @(posedge gsr)
    begin
	    SERRORS <= 32'b0;   //  non-blocking assignment 
	 end
 
endmodule

module check_the_rgb_signals(VR2, VR1, VR0, VB0, VB1, VG2, VG1, VG0, 
                      activeH, activeV, clk, gsr, rgb_error);

 input VR2, VR1, VR0, VB0, VB1, VG2, VG1, VG0;
 input activeH, activeV;
 input clk, gsr;
 output rgb_error;
 
   // rgb_error is high when any RGB output is high outside the active region
 assign rgb_error = (VR2|VR1|VR0|VB0|VB1|VG2|VG1|VG0)&~(activeH*activeV);
 
   // RGBERRORS is incremented when rgb_error is high at the rising edge of betterclk 
  integer RGBERRORS; 
  
   // since betterclk is divided by 2 in clkcntrl4 the error count 
	// will be double (each error will be counted twice)	
  always @(posedge clk)
    begin
	    RGBERRORS  <= RGBERRORS + rgb_error;   // non-blocking assignment
	 end
	 
	 // reset SERRORS on rising edge of gsr
  always @(posedge gsr)
    begin
	    RGBERRORS <= 32'b0;   //  non-blocking assignment 
	 end
 
endmodule


