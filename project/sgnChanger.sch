<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="inBit(0)" />
        <signal name="inBit(1)" />
        <signal name="inBit(2)" />
        <signal name="inBit(3)" />
        <signal name="inBit(4)" />
        <signal name="inBit(5)" />
        <signal name="inBit(6)" />
        <signal name="inBit(7)" />
        <signal name="sign" />
        <signal name="ZERO" />
        <signal name="inTransform0" />
        <signal name="inTransform1" />
        <signal name="inTransform2" />
        <signal name="inTransform3" />
        <signal name="inTransform4" />
        <signal name="inTransform5" />
        <signal name="inTransform6" />
        <signal name="inTransform7" />
        <signal name="ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO," />
        <signal name="inBit(9:0)" />
        <signal name="inTransform8" />
        <signal name="inTransform9" />
        <signal name="inBit(8)" />
        <signal name="inBit(9)" />
        <signal name="ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,inTransform9,inTransform8,inTransform7,inTransform6,inTransform5,inTransform4,inTransform3,inTransform2,inTransform1,inTransform0" />
        <signal name="sout(15:0)" />
        <signal name="sout(9:0)" />
        <signal name="d(9:0)" />
        <port polarity="Input" name="sign" />
        <port polarity="Input" name="inBit(9:0)" />
        <port polarity="Output" name="d(9:0)" />
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <block symbolname="gnd" name="XLXI_12">
            <blockpin signalname="ZERO" name="G" />
        </block>
        <block symbolname="xor2" name="XLXI_13">
            <blockpin signalname="inBit(0)" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform0" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_14">
            <blockpin signalname="inBit(1)" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform1" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_15">
            <blockpin signalname="inBit(2)" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform2" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_16">
            <blockpin signalname="inBit(3)" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform3" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_17">
            <blockpin signalname="inBit(4)" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform4" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_18">
            <blockpin signalname="inBit(5)" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform5" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_19">
            <blockpin signalname="inBit(6)" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform6" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_28">
            <blockpin signalname="inBit(7)" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform7" name="O" />
        </block>
        <block symbolname="add16" name="XLXI_29">
            <blockpin signalname="ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,inTransform9,inTransform8,inTransform7,inTransform6,inTransform5,inTransform4,inTransform3,inTransform2,inTransform1,inTransform0" name="A(15:0)" />
            <blockpin signalname="ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO," name="B(15:0)" />
            <blockpin signalname="sign" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="sout(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="xor2" name="XLXI_39">
            <blockpin signalname="inBit(8)" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform8" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_40">
            <blockpin signalname="inBit(9)" name="I0" />
            <blockpin signalname="sign" name="I1" />
            <blockpin signalname="inTransform9" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_41(9:0)">
            <blockpin signalname="sout(9:0)" name="I" />
            <blockpin signalname="d(9:0)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="sign">
            <wire x2="160" y1="96" y2="96" x1="144" />
            <wire x2="160" y1="96" y2="112" x1="160" />
            <wire x2="1920" y1="112" y2="112" x1="160" />
        </branch>
        <iomarker fontsize="28" x="144" y="96" name="sign" orien="R180" />
        <branch name="ZERO">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="576" y="1072" type="branch" />
            <wire x2="576" y1="1072" y2="1072" x1="240" />
        </branch>
        <instance x="176" y="1200" name="XLXI_12" orien="R0" />
        <instance x="1040" y="336" name="XLXI_13" orien="R0" />
        <instance x="1024" y="560" name="XLXI_14" orien="R0" />
        <instance x="1024" y="736" name="XLXI_15" orien="R0" />
        <instance x="1024" y="912" name="XLXI_16" orien="R0" />
        <instance x="1024" y="1088" name="XLXI_17" orien="R0" />
        <instance x="1024" y="1280" name="XLXI_18" orien="R0" />
        <instance x="1008" y="1456" name="XLXI_19" orien="R0" />
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1024" y="208" type="branch" />
            <wire x2="1040" y1="208" y2="208" x1="1024" />
        </branch>
        <branch name="inBit(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1024" y="272" type="branch" />
            <wire x2="1040" y1="272" y2="272" x1="1024" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1008" y="432" type="branch" />
            <wire x2="1024" y1="432" y2="432" x1="1008" />
        </branch>
        <branch name="inBit(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="496" type="branch" />
            <wire x2="1024" y1="496" y2="496" x1="992" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1008" y="608" type="branch" />
            <wire x2="1024" y1="608" y2="608" x1="1008" />
        </branch>
        <branch name="inBit(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="672" type="branch" />
            <wire x2="1024" y1="672" y2="672" x1="992" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="784" type="branch" />
            <wire x2="1024" y1="784" y2="784" x1="992" />
        </branch>
        <branch name="inBit(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="848" type="branch" />
            <wire x2="1024" y1="848" y2="848" x1="992" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="960" type="branch" />
            <wire x2="1024" y1="960" y2="960" x1="992" />
        </branch>
        <branch name="inBit(4)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1024" type="branch" />
            <wire x2="1024" y1="1024" y2="1024" x1="976" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="1152" type="branch" />
            <wire x2="1024" y1="1152" y2="1152" x1="992" />
        </branch>
        <branch name="inBit(5)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="1216" type="branch" />
            <wire x2="1024" y1="1216" y2="1216" x1="992" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="1328" type="branch" />
            <wire x2="1008" y1="1328" y2="1328" x1="992" />
        </branch>
        <branch name="inBit(6)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1392" type="branch" />
            <wire x2="1008" y1="1392" y2="1392" x1="976" />
        </branch>
        <branch name="inTransform0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1328" y="240" type="branch" />
            <wire x2="1328" y1="240" y2="240" x1="1296" />
        </branch>
        <branch name="inTransform1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1312" y="464" type="branch" />
            <wire x2="1312" y1="464" y2="464" x1="1280" />
        </branch>
        <branch name="inTransform2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1328" y="640" type="branch" />
            <wire x2="1328" y1="640" y2="640" x1="1280" />
        </branch>
        <branch name="inTransform3">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1344" y="816" type="branch" />
            <wire x2="1344" y1="816" y2="816" x1="1280" />
        </branch>
        <branch name="inTransform4">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1312" y="992" type="branch" />
            <wire x2="1312" y1="992" y2="992" x1="1280" />
        </branch>
        <branch name="inTransform5">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1328" y="1184" type="branch" />
            <wire x2="1328" y1="1184" y2="1184" x1="1280" />
        </branch>
        <branch name="inTransform6">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1328" y="1360" type="branch" />
            <wire x2="1328" y1="1360" y2="1360" x1="1264" />
        </branch>
        <instance x="992" y="1648" name="XLXI_28" orien="R0" />
        <branch name="inTransform7">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1280" y="1552" type="branch" />
            <wire x2="1280" y1="1552" y2="1552" x1="1248" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1520" type="branch" />
            <wire x2="992" y1="1520" y2="1520" x1="976" />
        </branch>
        <branch name="inBit(7)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1584" type="branch" />
            <wire x2="992" y1="1584" y2="1584" x1="976" />
        </branch>
        <instance x="2320" y="1792" name="XLXI_29" orien="R0" />
        <branch name="ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2256" y="1600" type="branch" />
            <wire x2="2320" y1="1600" y2="1600" x1="2256" />
        </branch>
        <branch name="inBit(9:0)">
            <wire x2="288" y1="304" y2="304" x1="176" />
            <wire x2="560" y1="304" y2="304" x1="288" />
        </branch>
        <iomarker fontsize="28" x="176" y="304" name="inBit(9:0)" orien="R180" />
        <instance x="992" y="1840" name="XLXI_39" orien="R0" />
        <instance x="976" y="2016" name="XLXI_40" orien="R0" />
        <branch name="inTransform8">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1296" y="1744" type="branch" />
            <wire x2="1280" y1="1744" y2="1744" x1="1248" />
            <wire x2="1296" y1="1744" y2="1744" x1="1280" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="1712" type="branch" />
            <wire x2="992" y1="1712" y2="1712" x1="944" />
        </branch>
        <branch name="inBit(8)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="1776" type="branch" />
            <wire x2="992" y1="1776" y2="1776" x1="944" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="1888" type="branch" />
            <wire x2="976" y1="1888" y2="1888" x1="944" />
        </branch>
        <branch name="inBit(9)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="944" y="1952" type="branch" />
            <wire x2="976" y1="1952" y2="1952" x1="944" />
        </branch>
        <branch name="inTransform9">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1312" y="1920" type="branch" />
            <wire x2="1280" y1="1920" y2="1920" x1="1232" />
            <wire x2="1312" y1="1920" y2="1920" x1="1280" />
        </branch>
        <branch name="ZERO,ZERO,ZERO,ZERO,ZERO,ZERO,inTransform9,inTransform8,inTransform7,inTransform6,inTransform5,inTransform4,inTransform3,inTransform2,inTransform1,inTransform0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2256" y="1472" type="branch" />
            <wire x2="2320" y1="1472" y2="1472" x1="2256" />
        </branch>
        <branch name="sign">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="1344" type="branch" />
            <wire x2="2320" y1="1344" y2="1344" x1="2272" />
        </branch>
        <branch name="sout(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2800" y="1536" type="branch" />
            <wire x2="2800" y1="1536" y2="1536" x1="2768" />
        </branch>
        <branch name="sout(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1872" y="2256" type="branch" />
            <wire x2="1984" y1="2256" y2="2256" x1="1872" />
        </branch>
        <instance x="1984" y="2288" name="XLXI_41(9:0)" orien="R0" />
        <branch name="d(9:0)">
            <wire x2="2256" y1="2256" y2="2256" x1="2208" />
        </branch>
        <iomarker fontsize="28" x="2256" y="2256" name="d(9:0)" orien="R0" />
        <text style="fontsize:64;fontname:Arial" x="348" y="56">Schematic 13 Sign CHanger for Meteor Bits True -- Absolute Value</text>
    </sheet>
</drawing>