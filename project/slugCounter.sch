<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="frameTickIn" />
        <signal name="clockIn" />
        <signal name="incOrDec" />
        <signal name="_gnd" />
        <signal name="slugEqEnd" />
        <signal name="slugEqBeg" />
        <signal name="slugSizeIn(15:0)" />
        <signal name="XLXN_25" />
        <signal name="XLXN_9(15:0)" />
        <signal name="qOut(15:0)" />
        <signal name="XLXN_36" />
        <signal name="XLXN_37" />
        <signal name="subSlugSize(9:0)" />
        <signal name="XLXN_46" />
        <signal name="outCount(9:0)" />
        <signal name="XLXN_57" />
        <signal name="qOut(9:0)" />
        <signal name="XLXN_56(9:0)" />
        <signal name="subSlugSize(15:0)" />
        <signal name="slugSizeInput(15:0)" />
        <signal name="_vcc" />
        <signal name="XLXN_61" />
        <signal name="XLXN_65" />
        <signal name="lder" />
        <port polarity="Input" name="frameTickIn" />
        <port polarity="Input" name="clockIn" />
        <port polarity="Input" name="incOrDec" />
        <port polarity="Output" name="outCount(9:0)" />
        <port polarity="Input" name="slugSizeInput(15:0)" />
        <blockdef name="slugComparator">
            <timestamp>2016-5-19T21:34:26</timestamp>
            <rect width="64" x="0" y="20" height="24" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-192" height="256" />
        </blockdef>
        <blockdef name="constant">
            <timestamp>2006-1-1T10:10:10</timestamp>
            <rect width="112" x="0" y="0" height="64" />
            <line x2="112" y1="32" y2="32" x1="144" />
        </blockdef>
        <blockdef name="cc16cled">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <rect width="64" x="320" y="-460" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-448" y2="-448" x1="384" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <rect width="64" x="0" y="-460" height="24" />
            <rect width="256" x="64" y="-512" height="448" />
            <line x2="320" y1="-192" y2="-192" x1="384" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="ftc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="edgeDetector">
            <timestamp>2016-5-19T21:54:36</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <blockdef name="adsu16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="48" y1="-64" y2="-64" x1="128" />
            <line x2="128" y1="-96" y2="-64" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="ftce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="gnd" name="XLXI_18">
            <blockpin signalname="_gnd" name="G" />
        </block>
        <block symbolname="ftc" name="XLXI_20">
            <blockpin signalname="clockIn" name="C" />
            <blockpin signalname="_gnd" name="CLR" />
            <blockpin signalname="XLXN_25" name="T" />
            <blockpin signalname="XLXN_36" name="Q" />
        </block>
        <block symbolname="cc16cled" name="XLXI_16">
            <blockpin signalname="clockIn" name="C" />
            <blockpin signalname="frameTickIn" name="CE" />
            <blockpin signalname="_gnd" name="CLR" />
            <blockpin signalname="XLXN_9(15:0)" name="D(15:0)" />
            <blockpin signalname="lder" name="L" />
            <blockpin signalname="XLXN_37" name="UP" />
            <blockpin name="CEO" />
            <blockpin signalname="qOut(15:0)" name="Q(15:0)" />
            <blockpin name="TC" />
        </block>
        <block symbolname="inv" name="XLXI_25">
            <blockpin signalname="XLXN_36" name="I" />
            <blockpin signalname="XLXN_37" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_33">
            <blockpin signalname="slugEqBeg" name="I0" />
            <blockpin signalname="slugEqEnd" name="I1" />
            <blockpin signalname="incOrDec" name="I2" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="slugComparator" name="XLXI_3">
            <blockpin signalname="outCount(9:0)" name="numberIn(9:0)" />
            <blockpin name="_LT" />
            <blockpin signalname="XLXN_46" name="_EQ" />
            <blockpin name="_GT" />
            <blockpin signalname="subSlugSize(9:0)" name="numberInTwo(9:0)" />
        </block>
        <block symbolname="edgeDetector" name="XLXI_31">
            <blockpin signalname="XLXN_46" name="_input" />
            <blockpin signalname="clockIn" name="clkin" />
            <blockpin signalname="slugEqEnd" name="signalOut" />
        </block>
        <block symbolname="slugComparator" name="XLXI_37">
            <blockpin signalname="outCount(9:0)" name="numberIn(9:0)" />
            <blockpin name="_LT" />
            <blockpin signalname="XLXN_57" name="_EQ" />
            <blockpin name="_GT" />
            <blockpin signalname="XLXN_56(9:0)" name="numberInTwo(9:0)" />
        </block>
        <block symbolname="edgeDetector" name="XLXI_39">
            <blockpin signalname="XLXN_57" name="_input" />
            <blockpin signalname="clockIn" name="clkin" />
            <blockpin signalname="slugEqBeg" name="signalOut" />
        </block>
        <block symbolname="buf" name="XLXI_30(9:0)">
            <blockpin signalname="qOut(9:0)" name="I" />
            <blockpin signalname="outCount(9:0)" name="O" />
        </block>
        <block symbolname="constant" name="XLXI_38">
            <attr value="9" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_56(9:0)" name="O" />
        </block>
        <block symbolname="adsu16" name="XLXI_44">
            <blockpin signalname="slugSizeIn(15:0)" name="A(15:0)" />
            <blockpin signalname="_gnd" name="ADD" />
            <blockpin signalname="slugSizeInput(15:0)" name="B(15:0)" />
            <blockpin signalname="_gnd" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="subSlugSize(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="constant" name="XLXI_64">
            <attr value="276" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="slugSizeIn(15:0)" name="O" />
        </block>
        <block symbolname="ftce" name="XLXI_66">
            <blockpin signalname="clockIn" name="C" />
            <blockpin signalname="XLXN_61" name="CE" />
            <blockpin name="CLR" />
            <blockpin signalname="_vcc" name="T" />
            <blockpin signalname="XLXN_65" name="Q" />
        </block>
        <block symbolname="vcc" name="XLXI_67">
            <blockpin signalname="_vcc" name="P" />
        </block>
        <block symbolname="inv" name="XLXI_68">
            <blockpin signalname="XLXN_65" name="I" />
            <blockpin signalname="XLXN_61" name="O" />
        </block>
        <block symbolname="edgeDetector" name="XLXI_69">
            <blockpin signalname="XLXN_65" name="_input" />
            <blockpin signalname="clockIn" name="clkin" />
            <blockpin signalname="lder" name="signalOut" />
        </block>
        <block symbolname="constant" name="XLXI_70">
            <attr value="064" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_9(15:0)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="frameTickIn">
            <wire x2="1120" y1="256" y2="256" x1="176" />
        </branch>
        <iomarker fontsize="28" x="176" y="256" name="frameTickIn" orien="R180" />
        <branch name="clockIn">
            <wire x2="1120" y1="464" y2="464" x1="208" />
        </branch>
        <iomarker fontsize="28" x="208" y="464" name="clockIn" orien="R180" />
        <branch name="incOrDec">
            <wire x2="736" y1="352" y2="352" x1="208" />
        </branch>
        <iomarker fontsize="28" x="208" y="352" name="incOrDec" orien="R180" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="336" y="800" type="branch" />
            <wire x2="192" y1="800" y2="912" x1="192" />
            <wire x2="336" y1="800" y2="800" x1="192" />
        </branch>
        <instance x="128" y="1040" name="XLXI_18" orien="R0" />
        <instance x="1808" y="848" name="XLXI_20" orien="R0" />
        <branch name="XLXN_25">
            <wire x2="1808" y1="592" y2="592" x1="1792" />
        </branch>
        <branch name="clockIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1792" y="720" type="branch" />
            <wire x2="1808" y1="720" y2="720" x1="1792" />
        </branch>
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1792" y="816" type="branch" />
            <wire x2="1808" y1="816" y2="816" x1="1792" />
        </branch>
        <instance x="2560" y="944" name="XLXI_16" orien="R0" />
        <branch name="XLXN_9(15:0)">
            <wire x2="2560" y1="496" y2="496" x1="2528" />
        </branch>
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2528" y="912" type="branch" />
            <wire x2="2560" y1="912" y2="912" x1="2528" />
        </branch>
        <branch name="clockIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2528" y="816" type="branch" />
            <wire x2="2560" y1="816" y2="816" x1="2528" />
        </branch>
        <branch name="frameTickIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2512" y="752" type="branch" />
            <wire x2="2560" y1="752" y2="752" x1="2512" />
        </branch>
        <branch name="qOut(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2992" y="496" type="branch" />
            <wire x2="2992" y1="496" y2="496" x1="2944" />
        </branch>
        <branch name="XLXN_36">
            <wire x2="2224" y1="592" y2="592" x1="2192" />
        </branch>
        <instance x="2224" y="624" name="XLXI_25" orien="R0" />
        <branch name="XLXN_37">
            <wire x2="2496" y1="592" y2="592" x1="2448" />
            <wire x2="2496" y1="592" y2="624" x1="2496" />
            <wire x2="2560" y1="624" y2="624" x1="2496" />
        </branch>
        <instance x="1536" y="720" name="XLXI_33" orien="R0" />
        <branch name="incOrDec">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="528" type="branch" />
            <wire x2="1536" y1="528" y2="528" x1="1520" />
        </branch>
        <branch name="slugEqEnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="592" type="branch" />
            <wire x2="1536" y1="592" y2="592" x1="1520" />
        </branch>
        <branch name="slugEqBeg">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="688" type="branch" />
            <wire x2="1536" y1="688" y2="688" x1="1520" />
            <wire x2="1536" y1="656" y2="688" x1="1536" />
        </branch>
        <instance x="1616" y="1424" name="XLXI_3" orien="R0">
        </instance>
        <branch name="subSlugSize(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1568" y="1552" type="branch" />
            <wire x2="1600" y1="1552" y2="1552" x1="1568" />
            <wire x2="1600" y1="1456" y2="1552" x1="1600" />
            <wire x2="1616" y1="1456" y2="1456" x1="1600" />
        </branch>
        <instance x="2096" y="1408" name="XLXI_31" orien="R0">
        </instance>
        <branch name="XLXN_46">
            <wire x2="2048" y1="1328" y2="1328" x1="2000" />
            <wire x2="2048" y1="1312" y2="1328" x1="2048" />
            <wire x2="2096" y1="1312" y2="1312" x1="2048" />
        </branch>
        <branch name="clockIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2000" y="1696" type="branch" />
            <wire x2="2096" y1="1696" y2="1696" x1="2000" />
            <wire x2="2096" y1="1376" y2="1376" x1="2032" />
            <wire x2="2032" y1="1376" y2="1472" x1="2032" />
            <wire x2="2096" y1="1472" y2="1472" x1="2032" />
            <wire x2="2096" y1="1472" y2="1696" x1="2096" />
        </branch>
        <branch name="slugEqEnd">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2512" y="1296" type="branch" />
            <wire x2="2496" y1="1312" y2="1312" x1="2480" />
            <wire x2="2496" y1="1296" y2="1312" x1="2496" />
            <wire x2="2512" y1="1296" y2="1296" x1="2496" />
        </branch>
        <instance x="1168" y="2320" name="XLXI_37" orien="R0">
        </instance>
        <branch name="XLXN_57">
            <wire x2="1600" y1="2224" y2="2224" x1="1552" />
        </branch>
        <instance x="1600" y="2320" name="XLXI_39" orien="R0">
        </instance>
        <branch name="clockIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1536" y="2512" type="branch" />
            <wire x2="1600" y1="2512" y2="2512" x1="1536" />
            <wire x2="1600" y1="2288" y2="2288" x1="1584" />
            <wire x2="1584" y1="2288" y2="2384" x1="1584" />
            <wire x2="1600" y1="2384" y2="2384" x1="1584" />
            <wire x2="1600" y1="2384" y2="2512" x1="1600" />
        </branch>
        <branch name="slugEqBeg">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2000" y="2224" type="branch" />
            <wire x2="2000" y1="2224" y2="2224" x1="1984" />
        </branch>
        <instance x="288" y="1472" name="XLXI_30(9:0)" orien="R0" />
        <branch name="qOut(9:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="272" y="1440" type="branch" />
            <wire x2="272" y1="1440" y2="1440" x1="256" />
            <wire x2="288" y1="1440" y2="1440" x1="272" />
        </branch>
        <branch name="XLXN_56(9:0)">
            <wire x2="1168" y1="2352" y2="2352" x1="464" />
        </branch>
        <instance x="320" y="2320" name="XLXI_38" orien="R0">
        </instance>
        <branch name="outCount(9:0)">
            <wire x2="528" y1="1536" y2="1536" x1="448" />
            <wire x2="448" y1="1536" y2="2288" x1="448" />
            <wire x2="1168" y1="2288" y2="2288" x1="448" />
            <wire x2="528" y1="1440" y2="1440" x1="512" />
            <wire x2="848" y1="1440" y2="1440" x1="528" />
            <wire x2="528" y1="1440" y2="1536" x1="528" />
            <wire x2="848" y1="1392" y2="1440" x1="848" />
            <wire x2="880" y1="1392" y2="1392" x1="848" />
            <wire x2="1616" y1="1392" y2="1392" x1="880" />
            <wire x2="2960" y1="1040" y2="1040" x1="880" />
            <wire x2="880" y1="1040" y2="1392" x1="880" />
        </branch>
        <instance x="848" y="2032" name="XLXI_44" orien="R0" />
        <branch name="slugSizeIn(15:0)">
            <wire x2="848" y1="1712" y2="1712" x1="832" />
        </branch>
        <branch name="subSlugSize(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1360" y="1776" type="branch" />
            <wire x2="1360" y1="1776" y2="1776" x1="1296" />
        </branch>
        <instance x="688" y="1680" name="XLXI_64" orien="R0">
        </instance>
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="832" y="1584" type="branch" />
            <wire x2="848" y1="1584" y2="1584" x1="832" />
        </branch>
        <rect width="2184" x="672" y="976" height="1060" />
        <rect width="2648" x="88" y="2052" height="540" />
        <text style="fontsize:32;fontname:Arial" x="1720" y="1004">upperBound</text>
        <text style="fontsize:32;fontname:Arial" x="1760" y="2084">lowerBound</text>
        <text style="fontsize:32;fontname:Arial" x="212" y="1320">currentCount</text>
        <rect width="532" x="108" y="1212" height="484" />
        <rect width="1144" x="1260" y="348" height="596" />
        <text style="fontsize:32;fontname:Arial" x="1744" y="388">invertPosition</text>
        <rect width="836" x="2444" y="344" height="596" />
        <text style="fontsize:32;fontname:Arial" x="2676" y="364">slugPostitionMoveCount</text>
        <iomarker fontsize="28" x="2960" y="1040" name="outCount(9:0)" orien="R0" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="816" y="1968" type="branch" />
            <wire x2="848" y1="1968" y2="1968" x1="816" />
        </branch>
        <branch name="slugSizeInput(15:0)">
            <wire x2="608" y1="624" y2="624" x1="208" />
        </branch>
        <iomarker fontsize="28" x="208" y="624" name="slugSizeInput(15:0)" orien="R180" />
        <branch name="slugSizeInput(15:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="816" y="1840" type="branch" />
            <wire x2="848" y1="1840" y2="1840" x1="816" />
        </branch>
        <instance x="2096" y="336" name="XLXI_66" orien="R0" />
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2032" y="80" type="branch" />
            <wire x2="2096" y1="80" y2="80" x1="2032" />
        </branch>
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="640" y="752" type="branch" />
            <wire x2="640" y1="752" y2="752" x1="512" />
        </branch>
        <instance x="448" y="752" name="XLXI_67" orien="R0" />
        <branch name="XLXN_61">
            <wire x2="2096" y1="144" y2="144" x1="2064" />
        </branch>
        <instance x="1840" y="176" name="XLXI_68" orien="R0" />
        <branch name="clockIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="208" type="branch" />
            <wire x2="2096" y1="208" y2="208" x1="2064" />
        </branch>
        <instance x="2800" y="256" name="XLXI_69" orien="R0">
        </instance>
        <branch name="XLXN_65">
            <wire x2="1840" y1="144" y2="144" x1="1760" />
            <wire x2="1760" y1="144" y2="384" x1="1760" />
            <wire x2="2560" y1="384" y2="384" x1="1760" />
            <wire x2="2560" y1="80" y2="80" x1="2480" />
            <wire x2="2560" y1="80" y2="272" x1="2560" />
            <wire x2="2560" y1="272" y2="384" x1="2560" />
            <wire x2="2656" y1="272" y2="272" x1="2560" />
            <wire x2="2800" y1="160" y2="160" x1="2656" />
            <wire x2="2656" y1="160" y2="272" x1="2656" />
        </branch>
        <branch name="clockIn">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2784" y="224" type="branch" />
            <wire x2="2800" y1="224" y2="224" x1="2784" />
        </branch>
        <branch name="lder">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3200" y="160" type="branch" />
            <wire x2="3200" y1="160" y2="160" x1="3184" />
        </branch>
        <branch name="lder">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2544" y="688" type="branch" />
            <wire x2="2560" y1="688" y2="688" x1="2544" />
        </branch>
        <instance x="2384" y="464" name="XLXI_70" orien="R0">
        </instance>
        <rect width="1880" x="1404" y="8" height="316" />
        <text style="fontsize:40;fontname:Arial" x="1484" y="40">Loader Toggle Hold</text>
        <text style="fontsize:64;fontname:Arial" x="144" y="52">Schematic 11 Slug Counter</text>
    </sheet>
</drawing>