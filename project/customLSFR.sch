<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clkin" />
        <signal name="r7" />
        <signal name="r6" />
        <signal name="r5" />
        <signal name="r4" />
        <signal name="r3" />
        <signal name="r2" />
        <signal name="r1" />
        <signal name="r0" />
        <signal name="r8" />
        <signal name="r9" />
        <signal name="r10" />
        <signal name="r11" />
        <signal name="r12" />
        <signal name="r13" />
        <signal name="r14" />
        <signal name="r15" />
        <signal name="outRandom(15:0)" />
        <signal name="r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15" />
        <port polarity="Input" name="clkin" />
        <port polarity="Output" name="outRandom(15:0)" />
        <blockdef name="randomNumberGenerator">
            <timestamp>2016-5-21T21:43:23</timestamp>
            <rect width="256" x="64" y="-512" height="512" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <block symbolname="randomNumberGenerator" name="XLXI_1">
            <blockpin signalname="clkin" name="clk" />
            <blockpin signalname="r0" name="R0" />
            <blockpin signalname="r1" name="R1" />
            <blockpin signalname="r2" name="R2" />
            <blockpin signalname="r3" name="R3" />
            <blockpin signalname="r4" name="R4" />
            <blockpin signalname="r5" name="R5" />
            <blockpin signalname="r6" name="R7" />
            <blockpin signalname="r7" name="R6" />
        </block>
        <block symbolname="randomNumberGenerator" name="XLXI_2">
            <blockpin signalname="clkin" name="clk" />
            <blockpin signalname="r8" name="R0" />
            <blockpin signalname="r9" name="R1" />
            <blockpin signalname="r10" name="R2" />
            <blockpin signalname="r11" name="R3" />
            <blockpin signalname="r12" name="R4" />
            <blockpin signalname="r13" name="R5" />
            <blockpin signalname="r14" name="R7" />
            <blockpin signalname="r15" name="R6" />
        </block>
        <block symbolname="buf" name="XLXI_8(15:0)">
            <blockpin signalname="r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15" name="I" />
            <blockpin signalname="outRandom(15:0)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="784" y="1408" name="XLXI_1" orien="R0">
        </instance>
        <instance x="784" y="2016" name="XLXI_2" orien="R0">
        </instance>
        <branch name="clkin">
            <wire x2="512" y1="480" y2="480" x1="320" />
            <wire x2="736" y1="480" y2="480" x1="512" />
            <wire x2="736" y1="480" y2="928" x1="736" />
            <wire x2="784" y1="928" y2="928" x1="736" />
            <wire x2="736" y1="928" y2="1536" x1="736" />
            <wire x2="752" y1="1536" y2="1536" x1="736" />
            <wire x2="784" y1="1536" y2="1536" x1="752" />
        </branch>
        <iomarker fontsize="28" x="320" y="480" name="clkin" orien="R180" />
        <branch name="r7">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1376" type="branch" />
            <wire x2="1200" y1="1376" y2="1376" x1="1168" />
        </branch>
        <branch name="r6">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1312" type="branch" />
            <wire x2="1200" y1="1312" y2="1312" x1="1168" />
        </branch>
        <branch name="r5">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1248" type="branch" />
            <wire x2="1200" y1="1248" y2="1248" x1="1168" />
        </branch>
        <branch name="r4">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1184" type="branch" />
            <wire x2="1200" y1="1184" y2="1184" x1="1168" />
        </branch>
        <branch name="r3">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1120" type="branch" />
            <wire x2="1200" y1="1120" y2="1120" x1="1168" />
        </branch>
        <branch name="r2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1056" type="branch" />
            <wire x2="1200" y1="1056" y2="1056" x1="1168" />
        </branch>
        <branch name="r1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="992" type="branch" />
            <wire x2="1200" y1="992" y2="992" x1="1168" />
        </branch>
        <branch name="r0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="928" type="branch" />
            <wire x2="1200" y1="928" y2="928" x1="1168" />
        </branch>
        <branch name="r8">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1216" y="1536" type="branch" />
            <wire x2="1200" y1="1536" y2="1536" x1="1168" />
            <wire x2="1216" y1="1536" y2="1536" x1="1200" />
        </branch>
        <branch name="r9">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1600" type="branch" />
            <wire x2="1200" y1="1600" y2="1600" x1="1168" />
        </branch>
        <branch name="r10">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1664" type="branch" />
            <wire x2="1200" y1="1664" y2="1664" x1="1168" />
        </branch>
        <branch name="r11">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1728" type="branch" />
            <wire x2="1200" y1="1728" y2="1728" x1="1168" />
        </branch>
        <branch name="r12">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1792" type="branch" />
            <wire x2="1200" y1="1792" y2="1792" x1="1168" />
        </branch>
        <branch name="r13">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1856" type="branch" />
            <wire x2="1200" y1="1856" y2="1856" x1="1168" />
        </branch>
        <branch name="r14">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1216" y="1920" type="branch" />
            <wire x2="1216" y1="1920" y2="1920" x1="1168" />
        </branch>
        <branch name="r15">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1200" y="1984" type="branch" />
            <wire x2="1200" y1="1984" y2="1984" x1="1168" />
        </branch>
        <branch name="outRandom(15:0)">
            <wire x2="2800" y1="944" y2="944" x1="2608" />
        </branch>
        <instance x="2384" y="976" name="XLXI_8(15:0)" orien="R0" />
        <branch name="r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2320" y="944" type="branch" />
            <wire x2="2368" y1="944" y2="944" x1="2320" />
            <wire x2="2384" y1="944" y2="944" x1="2368" />
        </branch>
        <iomarker fontsize="28" x="2800" y="944" name="outRandom(15:0)" orien="R0" />
        <text style="fontsize:64;fontname:Arial" x="64" y="56">Schematic 14 Custom LFSR, Random Number Generator</text>
    </sheet>
</drawing>