`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// VERILOG 6 METEOR EDGE COMPARATOR
// Engineer: 
// 
// Create Date:    18:45:34 05/21/2016 
// Design Name: 
// Module Name:    meteorEdgeComparators 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module meteorEdgeComparators(
    input [15:0] inputBits,
    output meteorCenterTrue,
    output meteorEdgeTrue
    );
	 
	 assign meteorEdgeTrue = (inputBits < 64) & (inputBits > 56); //edge of meteor
	 assign meteorCenterTrue = inputBits < 56; 						  //center of meteor



endmodule
