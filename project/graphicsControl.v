`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// VERILOG 2 RGB ASSIGNMENT MODULE/GRAPHICS CONTROL. MAIN MODULE OF GAME ARTIST
// Engineer: Kelvin Silva
// 
// Create Date:    18:10:12 05/15/2016 
// Design Name: 
// Module Name:    graphicsControl 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module graphicsControl(
    input [9:0] columnCountIn,
    input [9:0] rowCountIn,
    input isActiveIn,
	 
    input [9:0]slugCounter,
	 input [9:0]slugSize,
	 input slugMagentaSig,
	 input slugVisibleSig,
    input leftRightIn,
	 
    input startIn,
	 
	 input [9:0] meteorCounter,
	 input meteorBitsTrueEdgesOne,
	 input meteorBitsTrueCenterOne,
	 input meteorBitsTrueEdgesTwo,
	 input meteorBitsTrueCenterTwo,
	 

    output [2:0] redOut,
    output [2:0] greenOut,
    output [1:0] blueOut,
    output endOfGame
    );

	wire topEdgeTrue, bottomEdgeTrue, leftEdgeTrue, rightEdgeTrue;
	
	assign topEdgeTrue = isActiveIn&( (rowCountIn < 8)  );
	assign bottomEdgeTrue = isActiveIn&( (rowCountIn > 472) & (rowCountIn < 480) );
	
	assign leftEdgeTrue = isActiveIn&( (columnCountIn < 8) );
	assign rightEdgeTrue = isActiveIn&( (columnCountIn > 632) & (columnCountIn < 640) );
	
	
	wire slugTrueBlue;
	assign slugTrueBlue = slugVisibleSig & isActiveIn & (  (rowCountIn > 400) & (rowCountIn < 416) & ( (columnCountIn > slugCounter ) & (columnCountIn < slugCounter + slugSize) ) );
	assign slugTrueMagenta = slugMagentaSig & slugVisibleSig & isActiveIn & (  (rowCountIn > 400) & (rowCountIn < 416) & ( (columnCountIn > slugCounter ) & (columnCountIn < slugCounter + slugSize) ) );
	
	wire meteorBitsTrueEdgesOne_, meteorBitsTrueCenterOne_;
	wire meteorBitsTrueEdgesTwo_, meteorBitsTrueCenterTwo_;
	
	assign meteorBitsTrueEdgesOne_ = meteorBitsTrueEdgesOne & isActiveIn;
	assign meteorBitsTrueCenterOne_ = meteorBitsTrueCenterOne & isActiveIn;
	
	assign meteorBitsTrueEdgesTwo_ = meteorBitsTrueEdgesTwo & isActiveIn;
	assign meteorBitsTrueCenterTwo_ = meteorBitsTrueCenterTwo & isActiveIn;
	
	
	
	assign redOut[2] = topEdgeTrue | bottomEdgeTrue | leftEdgeTrue | rightEdgeTrue | meteorBitsTrueEdgesOne_ | meteorBitsTrueCenterTwo_ | slugTrueMagenta;
	assign redOut[1] = topEdgeTrue | bottomEdgeTrue | leftEdgeTrue | rightEdgeTrue;
	assign redOut[0] = topEdgeTrue | bottomEdgeTrue | leftEdgeTrue | rightEdgeTrue;
	
	assign greenOut[2] = topEdgeTrue | bottomEdgeTrue | leftEdgeTrue | rightEdgeTrue | meteorBitsTrueCenterOne_ | meteorBitsTrueEdgesTwo_ ;
	assign greenOut[1] = topEdgeTrue | bottomEdgeTrue | leftEdgeTrue | rightEdgeTrue;
	assign greenOut[0] = topEdgeTrue | bottomEdgeTrue | leftEdgeTrue | rightEdgeTrue;
	
	assign blueOut[0] = slugTrueBlue;
	assign blueOut[1] = slugTrueBlue;
	
	
	
	

endmodule
