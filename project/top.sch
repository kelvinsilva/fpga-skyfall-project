<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="isActive" />
        <signal name="_rowCountOut(9:0)" />
        <signal name="_columnCountOut(9:0)" />
        <signal name="clk" />
        <signal name="frameSigTop" />
        <signal name="vsOut" />
        <signal name="sLROut" />
        <signal name="sAnimOnOffOut" />
        <signal name="dAnimOnOffOut" />
        <signal name="sFlaAnimOnOffOut" />
        <signal name="_vcc" />
        <signal name="_gnd" />
        <signal name="pb2SigE" />
        <signal name="pb1SigE" />
        <signal name="pb0SigE" />
        <signal name="collisionHappenedGA" />
        <signal name="diamondCountUpGA" />
        <signal name="dig_sel" />
        <signal name="an3" />
        <signal name="an2" />
        <signal name="an1" />
        <signal name="an0" />
        <signal name="cg" />
        <signal name="ca" />
        <signal name="cb" />
        <signal name="cc" />
        <signal name="cd" />
        <signal name="ce" />
        <signal name="cf" />
        <signal name="sIncSzOut" />
        <signal name="rscOut" />
        <signal name="eqDivTen" />
        <signal name="terminC" />
        <signal name="incScOut" />
        <signal name="incLOut" />
        <signal name="resetSecTimer" />
        <signal name="sMagOut" />
        <signal name="lvlSig(7:0)" />
        <signal name="gameWinOut" />
        <signal name="XLXN_1" />
        <signal name="hsync" />
        <signal name="vsync" />
        <signal name="clkin" />
        <signal name="XLXN_13" />
        <signal name="betterclk" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_23" />
        <signal name="sw7" />
        <signal name="XLXN_7" />
        <signal name="gsr" />
        <signal name="XLXN_491(7:0)" />
        <signal name="XLXN_492(7:0)" />
        <signal name="XLXN_493" />
        <signal name="XLXN_504" />
        <signal name="pb2" />
        <signal name="pb2Sig" />
        <signal name="pb1Sig" />
        <signal name="pb0Sig" />
        <signal name="sw0" />
        <signal name="sw0Sig" />
        <signal name="sw1sig" />
        <signal name="sw1" />
        <signal name="sw2" />
        <signal name="sw3" />
        <signal name="sw3Sig" />
        <signal name="sw2Sig" />
        <signal name="_gnd,_gnd,_gnd,_gnd,countOutAux(3:0)" />
        <signal name="XLXN_601(7:0)" />
        <signal name="XLXN_572" />
        <signal name="_gnd,_gnd,_gnd,_gnd,sw3Sig,sw2Sig,sw1Sig,sw0Sig" />
        <signal name="divTen(7:0)" />
        <signal name="XLXN_560(7:0)" />
        <signal name="greenOut(0)" />
        <signal name="greenOut(1)" />
        <signal name="greenOut(2)" />
        <signal name="blueOut(0)" />
        <signal name="blueOut(1)" />
        <signal name="Red0" />
        <signal name="Red1" />
        <signal name="Red2" />
        <signal name="Green0" />
        <signal name="Green1" />
        <signal name="Green2" />
        <signal name="Blue0" />
        <signal name="Blue1" />
        <signal name="redOut(0)" />
        <signal name="redOut(1)" />
        <signal name="redOut(2)" />
        <signal name="redOut(2:0)" />
        <signal name="greenOut(2:0)" />
        <signal name="blueOut(1:0)" />
        <signal name="XLXN_568" />
        <signal name="gOvOut" />
        <signal name="countOutAux(3:0)" />
        <signal name="XLXN_320" />
        <signal name="caOut" />
        <signal name="cbOut" />
        <signal name="ccOut" />
        <signal name="cdOut" />
        <signal name="ceOut" />
        <signal name="cfOut" />
        <signal name="cgOut" />
        <signal name="dpout" />
        <signal name="an0out" />
        <signal name="an1out" />
        <signal name="an2out" />
        <signal name="an3out" />
        <signal name="pb1" />
        <signal name="pb0" />
        <signal name="XLXN_602" />
        <signal name="XLXN_603" />
        <port polarity="Output" name="hsync" />
        <port polarity="Output" name="vsync" />
        <port polarity="Input" name="clkin" />
        <port polarity="Input" name="betterclk" />
        <port polarity="Input" name="sw7" />
        <port polarity="Input" name="gsr" />
        <port polarity="Input" name="pb2" />
        <port polarity="Input" name="sw0" />
        <port polarity="Input" name="sw1" />
        <port polarity="Input" name="sw2" />
        <port polarity="Input" name="sw3" />
        <port polarity="Output" name="Red0" />
        <port polarity="Output" name="Red1" />
        <port polarity="Output" name="Red2" />
        <port polarity="Output" name="Green0" />
        <port polarity="Output" name="Green1" />
        <port polarity="Output" name="Green2" />
        <port polarity="Output" name="Blue0" />
        <port polarity="Output" name="Blue1" />
        <port polarity="Output" name="caOut" />
        <port polarity="Output" name="cbOut" />
        <port polarity="Output" name="ccOut" />
        <port polarity="Output" name="cdOut" />
        <port polarity="Output" name="ceOut" />
        <port polarity="Output" name="cfOut" />
        <port polarity="Output" name="cgOut" />
        <port polarity="Output" name="dpout" />
        <port polarity="Output" name="an0out" />
        <port polarity="Output" name="an1out" />
        <port polarity="Output" name="an2out" />
        <port polarity="Output" name="an3out" />
        <port polarity="Input" name="pb1" />
        <port polarity="Input" name="pb0" />
        <blockdef name="vgaController">
            <timestamp>2016-5-24T4:56:26</timestamp>
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <rect width="64" x="320" y="-140" height="24" />
            <line x2="384" y1="-128" y2="-128" x1="320" />
            <line x2="384" y1="-64" y2="-64" x1="320" />
            <rect width="64" x="320" y="-12" height="24" />
            <line x2="384" y1="0" y2="0" x1="320" />
            <rect width="64" x="320" y="52" height="24" />
            <line x2="384" y1="64" y2="64" x1="320" />
            <line x2="384" y1="-256" y2="-256" x1="320" />
            <line x2="384" y1="-192" y2="-192" x1="320" />
            <rect width="256" x="64" y="-284" height="380" />
        </blockdef>
        <blockdef name="startup_spartan3e">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <rect width="352" x="64" y="-384" height="320" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
        </blockdef>
        <blockdef name="ibuf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="0" y2="-64" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
        </blockdef>
        <blockdef name="ibufg">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="0" y2="-64" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
        </blockdef>
        <blockdef name="bufg">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="0" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
        </blockdef>
        <blockdef name="clkcntrl4">
            <timestamp>2016-5-15T22:58:0</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="obuf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="0" y2="-64" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="edgeDetector">
            <timestamp>2016-5-19T21:54:36</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="gamStateMachine">
            <timestamp>2016-5-24T3:52:17</timestamp>
            <line x2="576" y1="32" y2="32" x1="512" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <line x2="0" y1="-592" y2="-592" x1="64" />
            <line x2="0" y1="-512" y2="-512" x1="64" />
            <line x2="0" y1="-432" y2="-432" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-272" y2="-272" x1="64" />
            <line x2="0" y1="-192" y2="-192" x1="64" />
            <line x2="0" y1="-112" y2="-112" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="576" y1="-672" y2="-672" x1="512" />
            <line x2="576" y1="-608" y2="-608" x1="512" />
            <line x2="576" y1="-544" y2="-544" x1="512" />
            <line x2="576" y1="-480" y2="-480" x1="512" />
            <line x2="576" y1="-416" y2="-416" x1="512" />
            <line x2="576" y1="-352" y2="-352" x1="512" />
            <line x2="576" y1="-288" y2="-288" x1="512" />
            <line x2="576" y1="-224" y2="-224" x1="512" />
            <line x2="576" y1="-160" y2="-160" x1="512" />
            <line x2="576" y1="-96" y2="-96" x1="512" />
            <line x2="576" y1="-32" y2="-32" x1="512" />
            <rect width="448" x="64" y="-704" height="768" />
        </blockdef>
        <blockdef name="compm8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <rect width="64" x="0" y="-332" height="24" />
            <rect width="256" x="64" y="-384" height="320" />
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
        </blockdef>
        <blockdef name="constant">
            <timestamp>2006-1-1T10:10:10</timestamp>
            <rect width="112" x="0" y="0" height="64" />
            <line x2="112" y1="32" y2="32" x1="144" />
        </blockdef>
        <blockdef name="cb8re">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <rect width="64" x="320" y="-268" height="24" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="cb2re">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <line x2="320" y1="-320" y2="-320" x1="384" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <rect width="256" x="64" y="-384" height="320" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="scoreDisplay">
            <timestamp>2016-5-24T6:18:53</timestamp>
            <rect width="64" x="320" y="20" height="24" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <rect width="64" x="320" y="84" height="24" />
            <line x2="384" y1="96" y2="96" x1="320" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="384" y1="-736" y2="-736" x1="320" />
            <line x2="384" y1="-672" y2="-672" x1="320" />
            <line x2="384" y1="-608" y2="-608" x1="320" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-768" height="896" />
        </blockdef>
        <blockdef name="comp8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-384" height="320" />
            <line x2="320" y1="-224" y2="-224" x1="384" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
        </blockdef>
        <blockdef name="adsu8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="48" y1="-64" y2="-64" x1="128" />
            <line x2="128" y1="-96" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="gameArtist">
            <timestamp>2016-5-30T19:34:19</timestamp>
            <line x2="0" y1="608" y2="608" x1="64" />
            <line x2="480" y1="544" y2="544" x1="416" />
            <line x2="0" y1="480" y2="480" x1="64" />
            <line x2="480" y1="416" y2="416" x1="416" />
            <line x2="0" y1="352" y2="352" x1="64" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <line x2="0" y1="160" y2="160" x1="64" />
            <line x2="0" y1="224" y2="224" x1="64" />
            <line x2="0" y1="288" y2="288" x1="64" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-428" height="24" />
            <line x2="480" y1="-416" y2="-416" x1="416" />
            <rect width="64" x="416" y="-300" height="24" />
            <line x2="480" y1="-288" y2="-288" x1="416" />
            <rect width="64" x="416" y="-172" height="24" />
            <line x2="480" y1="-160" y2="-160" x1="416" />
            <rect width="352" x="64" y="-448" height="1088" />
        </blockdef>
        <blockdef name="bufgmux">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-64" y2="-192" x1="96" />
            <line x2="96" y1="-96" y2="-64" x1="256" />
            <line x2="256" y1="-160" y2="-96" x1="256" />
            <line x2="256" y1="-192" y2="-160" x1="96" />
            <line x2="96" y1="-32" y2="-32" x1="176" />
            <line x2="176" y1="-80" y2="-32" x1="176" />
            <line x2="256" y1="-128" y2="-128" x1="320" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
            <line x2="96" y1="-160" y2="-160" x1="0" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
        </blockdef>
        <block symbolname="vcc" name="XLXI_24">
            <blockpin signalname="_vcc" name="P" />
        </block>
        <block symbolname="gnd" name="XLXI_25">
            <blockpin signalname="_gnd" name="G" />
        </block>
        <block symbolname="obuf" name="XLXI_12">
            <attr value="J14" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="XLXN_1" name="I" />
            <blockpin signalname="hsync" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_13">
            <attr value="K13" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="vsOut" name="I" />
            <blockpin signalname="vsync" name="O" />
        </block>
        <block symbolname="vgaController" name="XLXI_1">
            <blockpin signalname="XLXN_1" name="hsOut" />
            <blockpin signalname="vsOut" name="vsOut" />
            <blockpin signalname="clk" name="clkin" />
            <blockpin name="hsCount(9:0)" />
            <blockpin signalname="isActive" name="isActive" />
            <blockpin signalname="_columnCountOut(9:0)" name="columnCount(9:0)" />
            <blockpin signalname="_rowCountOut(9:0)" name="rowCount(9:0)" />
        </block>
        <block symbolname="ibufg" name="XLXI_6">
            <attr value="B8" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="clkin" name="I" />
            <blockpin signalname="XLXN_13" name="O" />
        </block>
        <block symbolname="bufg" name="XLXI_7">
            <blockpin signalname="XLXN_13" name="I" />
            <blockpin signalname="XLXN_603" name="O" />
        </block>
        <block symbolname="ibufg" name="XLXI_8">
            <attr value="M6" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="betterclk" name="I" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="ibuf" name="XLXI_11">
            <attr value="N3" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="sw7" name="I" />
            <blockpin signalname="XLXN_23" name="O" />
        </block>
        <block symbolname="clkcntrl4" name="XLXI_10">
            <blockpin signalname="XLXN_602" name="clkin" />
            <blockpin signalname="clk" name="clkb2" />
            <blockpin signalname="dig_sel" name="seldig" />
        </block>
        <block symbolname="startup_spartan3e" name="XLXI_2">
            <blockpin signalname="XLXN_602" name="CLK" />
            <blockpin signalname="XLXN_7" name="GSR" />
            <blockpin name="GTS" />
            <blockpin name="MBT" />
        </block>
        <block symbolname="ibuf" name="XLXI_3">
            <attr value="A7" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="gsr" name="I" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="compm8" name="XLXI_208">
            <blockpin signalname="XLXN_491(7:0)" name="A(7:0)" />
            <blockpin signalname="XLXN_492(7:0)" name="B(7:0)" />
            <blockpin signalname="XLXN_504" name="GT" />
            <blockpin name="LT" />
        </block>
        <block symbolname="constant" name="XLXI_209">
            <attr value="3C" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_492(7:0)" name="O" />
        </block>
        <block symbolname="cb8re" name="XLXI_210">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="frameSigTop" name="CE" />
            <blockpin signalname="XLXN_493" name="R" />
            <blockpin name="CEO" />
            <blockpin signalname="XLXN_491(7:0)" name="Q(7:0)" />
            <blockpin name="TC" />
        </block>
        <block symbolname="or2" name="XLXI_213">
            <blockpin signalname="XLXN_504" name="I0" />
            <blockpin signalname="resetSecTimer" name="I1" />
            <blockpin signalname="XLXN_493" name="O" />
        </block>
        <block symbolname="cb2re" name="XLXI_211">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="XLXN_504" name="CE" />
            <blockpin signalname="resetSecTimer" name="R" />
            <blockpin name="CEO" />
            <blockpin name="Q0" />
            <blockpin name="Q1" />
            <blockpin signalname="terminC" name="TC" />
        </block>
        <block symbolname="ibuf" name="XLXI_49">
            <attr value="M4" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="pb2" name="I" />
            <blockpin signalname="pb2Sig" name="O" />
        </block>
        <block symbolname="ibuf" name="XLXI_116">
            <attr value="P11" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="sw0" name="I" />
            <blockpin signalname="sw0Sig" name="O" />
        </block>
        <block symbolname="ibuf" name="XLXI_117">
            <attr value="L3" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="sw1" name="I" />
            <blockpin signalname="sw1sig" name="O" />
        </block>
        <block symbolname="ibuf" name="XLXI_177">
            <attr value="K3" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="sw2" name="I" />
            <blockpin signalname="sw2Sig" name="O" />
        </block>
        <block symbolname="ibuf" name="XLXI_178">
            <attr value="B4" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="sw3" name="I" />
            <blockpin signalname="sw3Sig" name="O" />
        </block>
        <block symbolname="adsu8" name="XLXI_230">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,countOutAux(3:0)" name="A(7:0)" />
            <blockpin signalname="_gnd" name="ADD" />
            <blockpin signalname="XLXN_601(7:0)" name="B(7:0)" />
            <blockpin signalname="_vcc" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="lvlSig(7:0)" name="S(7:0)" />
        </block>
        <block symbolname="constant" name="XLXI_238">
            <attr value="01" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_601(7:0)" name="O" />
        </block>
        <block symbolname="comp8" name="XLXI_225">
            <blockpin signalname="_gnd,_gnd,_gnd,_gnd,sw3Sig,sw2Sig,sw1Sig,sw0Sig" name="A(7:0)" />
            <blockpin signalname="lvlSig(7:0)" name="B(7:0)" />
            <blockpin signalname="XLXN_568" name="EQ" />
        </block>
        <block symbolname="cb8re" name="XLXI_219">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="incScOut" name="CE" />
            <blockpin signalname="XLXN_572" name="R" />
            <blockpin name="CEO" />
            <blockpin signalname="divTen(7:0)" name="Q(7:0)" />
            <blockpin name="TC" />
        </block>
        <block symbolname="or2" name="XLXI_229">
            <blockpin signalname="rscOut" name="I0" />
            <blockpin signalname="eqDivTen" name="I1" />
            <blockpin signalname="XLXN_572" name="O" />
        </block>
        <block symbolname="constant" name="XLXI_224">
            <attr value="0A" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_560(7:0)" name="O" />
        </block>
        <block symbolname="comp8" name="XLXI_223">
            <blockpin signalname="divTen(7:0)" name="A(7:0)" />
            <blockpin signalname="XLXN_560(7:0)" name="B(7:0)" />
            <blockpin signalname="eqDivTen" name="EQ" />
        </block>
        <block symbolname="obuf" name="XLXI_16">
            <attr value="C14" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="redOut(0)" name="I" />
            <blockpin signalname="Red0" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_17">
            <attr value="D13" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="redOut(1)" name="I" />
            <blockpin signalname="Red1" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_18">
            <attr value="F13" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="redOut(2)" name="I" />
            <blockpin signalname="Red2" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_19">
            <attr value="F14" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="greenOut(0)" name="I" />
            <blockpin signalname="Green0" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_20">
            <attr value="G13" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="greenOut(1)" name="I" />
            <blockpin signalname="Green1" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_21">
            <attr value="G14" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="greenOut(2)" name="I" />
            <blockpin signalname="Green2" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_22">
            <attr value="H13" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="blueOut(0)" name="I" />
            <blockpin signalname="Blue0" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_23">
            <attr value="J13" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="blueOut(1)" name="I" />
            <blockpin signalname="Blue1" name="O" />
        </block>
        <block symbolname="gamStateMachine" name="XLXI_201">
            <blockpin signalname="collisionHappenedGA" name="collisionHappenedIn" />
            <blockpin signalname="diamondCountUpGA" name="diamondCountUpIn" />
            <blockpin signalname="eqDivTen" name="diamondLevelUpIn" />
            <blockpin signalname="pb0SigE" name="btn0In" />
            <blockpin signalname="pb1SigE" name="btn1In" />
            <blockpin signalname="pb2SigE" name="btn2In" />
            <blockpin signalname="terminC" name="fourSecSigIn" />
            <blockpin signalname="XLXN_568" name="levelsMatchIn" />
            <blockpin signalname="clk" name="clkin" />
            <blockpin signalname="sIncSzOut" name="slugIncSizeOut" />
            <blockpin signalname="sAnimOnOffOut" name="slugAnimationOnOffOut" />
            <blockpin signalname="sLROut" name="leftRightSlugOut" />
            <blockpin signalname="sFlaAnimOnOffOut" name="slugFlashAnimOnOut" />
            <blockpin signalname="dAnimOnOffOut" name="diamondAnimOnOffOut" />
            <blockpin signalname="incScOut" name="incScoreOut" />
            <blockpin signalname="incLOut" name="incLevelOut" />
            <blockpin signalname="resetSecTimer" name="resetFourSecTimerOut" />
            <blockpin signalname="gameWinOut" name="gameWinSigOut" />
            <blockpin signalname="sMagOut" name="slugMagentaOut" />
            <blockpin signalname="gOvOut" name="gameOverOut" />
            <blockpin signalname="rscOut" name="resetScoreOut" />
        </block>
        <block symbolname="scoreDisplay" name="XLXI_214">
            <blockpin signalname="clk" name="clkin" />
            <blockpin signalname="incScOut" name="inc" />
            <blockpin signalname="dig_sel" name="digitalADV" />
            <blockpin signalname="_vcc" name="enableIn" />
            <blockpin signalname="incLOut" name="increaseAux" />
            <blockpin signalname="rscOut" name="resetData" />
            <blockpin signalname="ca" name="outputA" />
            <blockpin signalname="cb" name="outputB" />
            <blockpin signalname="cc" name="outputC" />
            <blockpin signalname="cd" name="outputD" />
            <blockpin signalname="cf" name="outputF" />
            <blockpin signalname="ce" name="outputE" />
            <blockpin signalname="cg" name="outputG" />
            <blockpin signalname="XLXN_320" name="an0" />
            <blockpin signalname="an3" name="an3_Out" />
            <blockpin signalname="an2" name="an2_Out" />
            <blockpin signalname="an1" name="an1_Out" />
            <blockpin signalname="an0" name="an0_Out" />
            <blockpin name="countOut(7:0)" />
            <blockpin signalname="countOutAux(3:0)" name="countOutAux(3:0)" />
        </block>
        <block symbolname="obuf" name="XLXI_188">
            <attr value="L14" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="ca" name="I" />
            <blockpin signalname="caOut" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_190">
            <attr value="H12" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="cb" name="I" />
            <blockpin signalname="cbOut" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_191">
            <attr value="N14" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="cc" name="I" />
            <blockpin signalname="ccOut" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_192">
            <attr value="N11" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="cd" name="I" />
            <blockpin signalname="cdOut" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_193">
            <attr value="P12" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="ce" name="I" />
            <blockpin signalname="ceOut" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_194">
            <attr value="L13" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="cf" name="I" />
            <blockpin signalname="cfOut" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_195">
            <attr value="M12" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="cg" name="I" />
            <blockpin signalname="cgOut" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_196">
            <attr value="N13" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="_vcc" name="I" />
            <blockpin signalname="dpout" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_197">
            <attr value="F12" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="an0" name="I" />
            <blockpin signalname="an0out" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_198">
            <attr value="J12" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="an1" name="I" />
            <blockpin signalname="an1out" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_199">
            <attr value="M13" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="an2" name="I" />
            <blockpin signalname="an2out" name="O" />
        </block>
        <block symbolname="obuf" name="XLXI_200">
            <attr value="K14" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="an3" name="I" />
            <blockpin signalname="an3out" name="O" />
        </block>
        <block symbolname="edgeDetector" name="XLXI_114">
            <blockpin signalname="pb1Sig" name="_input" />
            <blockpin signalname="clk" name="clkin" />
            <blockpin signalname="pb1SigE" name="signalOut" />
        </block>
        <block symbolname="edgeDetector" name="XLXI_172">
            <blockpin signalname="pb2Sig" name="_input" />
            <blockpin signalname="clk" name="clkin" />
            <blockpin signalname="pb2SigE" name="signalOut" />
        </block>
        <block symbolname="edgeDetector" name="XLXI_113">
            <blockpin signalname="pb0Sig" name="_input" />
            <blockpin signalname="clk" name="clkin" />
            <blockpin signalname="pb0SigE" name="signalOut" />
        </block>
        <block symbolname="ibuf" name="XLXI_79">
            <attr value="C11" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="pb1" name="I" />
            <blockpin signalname="pb1Sig" name="O" />
        </block>
        <block symbolname="ibuf" name="XLXI_112">
            <attr value="G12" name="LOC">
                <trait verilog="all:0 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="pb0" name="I" />
            <blockpin signalname="pb0Sig" name="O" />
        </block>
        <block symbolname="edgeDetector" name="XLXI_48">
            <blockpin signalname="vsOut" name="_input" />
            <blockpin signalname="clk" name="clkin" />
            <blockpin signalname="frameSigTop" name="signalOut" />
        </block>
        <block symbolname="gameArtist" name="XLXI_239">
            <blockpin signalname="_columnCountOut(9:0)" name="columnCountIn(9:0)" />
            <blockpin signalname="_rowCountOut(9:0)" name="rowCountIn(9:0)" />
            <blockpin signalname="isActive" name="isActiveIn" />
            <blockpin signalname="frameSigTop" name="frameTickIn" />
            <blockpin signalname="clk" name="clkin" />
            <blockpin signalname="sLROut" name="leftRightIn" />
            <blockpin signalname="rscOut" name="slugResetSizeIn" />
            <blockpin signalname="sIncSzOut" name="slugIncSizeIn" />
            <blockpin signalname="sAnimOnOffOut" name="slugAnimationOnOff" />
            <blockpin signalname="dAnimOnOffOut" name="diamondAnimationOnOff" />
            <blockpin signalname="sFlaAnimOnOffOut" name="slugFlashAnimOn" />
            <blockpin signalname="sMagOut" name="slugMagentaColorIn" />
            <blockpin signalname="gameWinOut" name="endGameIn" />
            <blockpin name="startIn" />
            <blockpin signalname="redOut(2:0)" name="redOut(2:0)" />
            <blockpin signalname="greenOut(2:0)" name="greenOut(2:0)" />
            <blockpin signalname="blueOut(1:0)" name="blueOut(1:0)" />
            <blockpin signalname="collisionHappenedGA" name="collisionHappened" />
            <blockpin signalname="diamondCountUpGA" name="diamondTouchBottomScreen" />
        </block>
        <block symbolname="bufgmux" name="XLXI_240">
            <blockpin signalname="XLXN_603" name="I0" />
            <blockpin signalname="XLXN_16" name="I1" />
            <blockpin signalname="XLXN_23" name="S" />
            <blockpin signalname="XLXN_602" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="256" y="3216" name="XLXI_24" orien="R0" />
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="384" y="3216" type="branch" />
            <wire x2="320" y1="3216" y2="3232" x1="320" />
            <wire x2="384" y1="3232" y2="3232" x1="320" />
            <wire x2="384" y1="3216" y2="3232" x1="384" />
        </branch>
        <instance x="256" y="3424" name="XLXI_25" orien="R0" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="384" y="3296" type="branch" />
            <wire x2="320" y1="3280" y2="3296" x1="320" />
            <wire x2="384" y1="3280" y2="3280" x1="320" />
            <wire x2="384" y1="3280" y2="3296" x1="384" />
        </branch>
        <rect width="436" x="168" y="3028" height="428" />
        <branch name="XLXN_1">
            <wire x2="1456" y1="3056" y2="3056" x1="1440" />
            <wire x2="1504" y1="3056" y2="3056" x1="1456" />
            <wire x2="1456" y1="3024" y2="3056" x1="1456" />
        </branch>
        <branch name="isActive">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1616" y="3248" type="branch" />
            <wire x2="1616" y1="3248" y2="3248" x1="1440" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1040" y="3184" type="branch" />
            <wire x2="1056" y1="3184" y2="3184" x1="1040" />
        </branch>
        <branch name="_rowCountOut(9:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="3376" type="branch" />
            <wire x2="1520" y1="3376" y2="3376" x1="1440" />
        </branch>
        <instance x="1504" y="3088" name="XLXI_12" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="1504" y="3152" name="XLXI_13" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="112" y="-20" type="instance" />
        </instance>
        <branch name="vsOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1536" y="3168" type="branch" />
            <wire x2="1456" y1="3120" y2="3120" x1="1440" />
            <wire x2="1504" y1="3120" y2="3120" x1="1456" />
            <wire x2="1456" y1="3120" y2="3168" x1="1456" />
            <wire x2="1536" y1="3168" y2="3168" x1="1456" />
        </branch>
        <branch name="hsync">
            <wire x2="1888" y1="3056" y2="3056" x1="1728" />
        </branch>
        <branch name="vsync">
            <wire x2="1904" y1="3120" y2="3120" x1="1728" />
        </branch>
        <instance x="1056" y="3312" name="XLXI_1" orien="R0">
        </instance>
        <branch name="_columnCountOut(9:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1504" y="3312" type="branch" />
            <wire x2="1504" y1="3312" y2="3312" x1="1440" />
        </branch>
        <rect width="1356" x="736" y="2816" height="628" />
        <text style="fontsize:36;fontname:Arial" x="1080" y="2856">VGA H/V synch and Timing Controller</text>
        <iomarker fontsize="28" x="1904" y="3120" name="vsync" orien="R0" />
        <iomarker fontsize="28" x="1888" y="3056" name="hsync" orien="R0" />
        <branch name="clkin">
            <wire x2="2464" y1="2992" y2="2992" x1="2432" />
        </branch>
        <instance x="2464" y="3024" name="XLXI_6" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="XLXN_13">
            <wire x2="2720" y1="2992" y2="2992" x1="2688" />
        </branch>
        <instance x="2720" y="3024" name="XLXI_7" orien="R0" />
        <branch name="betterclk">
            <wire x2="2608" y1="3184" y2="3184" x1="2448" />
        </branch>
        <instance x="2608" y="3216" name="XLXI_8" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="XLXN_23">
            <wire x2="3024" y1="3344" y2="3344" x1="2672" />
            <wire x2="3024" y1="3168" y2="3344" x1="3024" />
            <wire x2="3072" y1="3168" y2="3168" x1="3024" />
            <wire x2="3072" y1="3168" y2="3200" x1="3072" />
            <wire x2="3136" y1="3200" y2="3200" x1="3072" />
        </branch>
        <instance x="2448" y="3376" name="XLXI_11" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="sw7">
            <wire x2="2448" y1="3344" y2="3344" x1="2416" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4368" y="3008" type="branch" />
            <wire x2="4064" y1="3008" y2="3168" x1="4064" />
            <wire x2="4144" y1="3168" y2="3168" x1="4064" />
            <wire x2="4144" y1="3168" y2="3264" x1="4144" />
            <wire x2="4368" y1="3008" y2="3008" x1="4064" />
            <wire x2="4144" y1="3264" y2="3264" x1="4064" />
        </branch>
        <branch name="dig_sel">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4352" y="3200" type="branch" />
            <wire x2="4336" y1="3328" y2="3328" x1="4064" />
            <wire x2="4352" y1="3200" y2="3200" x1="4336" />
            <wire x2="4336" y1="3200" y2="3328" x1="4336" />
        </branch>
        <instance x="3680" y="3360" name="XLXI_10" orien="R0">
        </instance>
        <instance x="3552" y="3296" name="XLXI_2" orien="R0" />
        <branch name="XLXN_7">
            <wire x2="3536" y1="2928" y2="2928" x1="3264" />
            <wire x2="3536" y1="2928" y2="2976" x1="3536" />
            <wire x2="3552" y1="2976" y2="2976" x1="3536" />
        </branch>
        <instance x="3040" y="2960" name="XLXI_3" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="gsr">
            <wire x2="3040" y1="2928" y2="2928" x1="3008" />
        </branch>
        <rect width="2276" x="2284" y="2760" height="692" />
        <text style="fontsize:36;fontname:Arial" x="3108" y="2792">System Clock Management Module</text>
        <iomarker fontsize="28" x="2432" y="2992" name="clkin" orien="R180" />
        <iomarker fontsize="28" x="2448" y="3184" name="betterclk" orien="R180" />
        <iomarker fontsize="28" x="2416" y="3344" name="sw7" orien="R180" />
        <iomarker fontsize="28" x="3008" y="2928" name="gsr" orien="R180" />
        <branch name="XLXN_491(7:0)">
            <wire x2="1056" y1="2352" y2="2352" x1="976" />
        </branch>
        <branch name="XLXN_492(7:0)">
            <wire x2="1008" y1="2624" y2="2624" x1="976" />
            <wire x2="1008" y1="2544" y2="2624" x1="1008" />
            <wire x2="1056" y1="2544" y2="2544" x1="1008" />
        </branch>
        <instance x="1056" y="2672" name="XLXI_208" orien="R0" />
        <instance x="832" y="2592" name="XLXI_209" orien="R0">
        </instance>
        <branch name="frameSigTop">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="480" y="2416" type="branch" />
            <wire x2="592" y1="2416" y2="2416" x1="480" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="576" y="2480" type="branch" />
            <wire x2="592" y1="2480" y2="2480" x1="576" />
        </branch>
        <instance x="592" y="2608" name="XLXI_210" orien="R0" />
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1712" y="2480" type="branch" />
            <wire x2="1728" y1="2480" y2="2480" x1="1712" />
        </branch>
        <branch name="terminC">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2128" y="2480" type="branch" />
            <wire x2="2128" y1="2480" y2="2480" x1="2112" />
        </branch>
        <branch name="resetSecTimer">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1712" y="2576" type="branch" />
            <wire x2="1728" y1="2576" y2="2576" x1="1712" />
        </branch>
        <branch name="XLXN_493">
            <wire x2="592" y1="2576" y2="2704" x1="592" />
            <wire x2="1120" y1="2704" y2="2704" x1="592" />
        </branch>
        <branch name="XLXN_504">
            <wire x2="1504" y1="2672" y2="2672" x1="1376" />
            <wire x2="1504" y1="2416" y2="2416" x1="1440" />
            <wire x2="1728" y1="2416" y2="2416" x1="1504" />
            <wire x2="1504" y1="2416" y2="2672" x1="1504" />
        </branch>
        <instance x="1376" y="2608" name="XLXI_213" orien="R180" />
        <branch name="resetSecTimer">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1424" y="2736" type="branch" />
            <wire x2="1424" y1="2736" y2="2736" x1="1376" />
        </branch>
        <instance x="1728" y="2608" name="XLXI_211" orien="R0" />
        <rect width="1984" x="256" y="2020" height="776" />
        <text style="fontsize:40;fontname:Arial" x="652" y="2056">Four Seconds Counter Module -- For Game State Machine</text>
        <branch name="pb2">
            <wire x2="304" y1="272" y2="272" x1="224" />
        </branch>
        <instance x="304" y="304" name="XLXI_49" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="pb2Sig">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="560" y="256" type="branch" />
            <wire x2="544" y1="272" y2="272" x1="528" />
            <wire x2="544" y1="256" y2="272" x1="544" />
            <wire x2="560" y1="256" y2="256" x1="544" />
        </branch>
        <instance x="272" y="384" name="XLXI_116" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="sw0">
            <wire x2="272" y1="352" y2="352" x1="240" />
        </branch>
        <branch name="sw0Sig">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="512" y="352" type="branch" />
            <wire x2="512" y1="352" y2="352" x1="496" />
        </branch>
        <instance x="272" y="496" name="XLXI_117" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="sw1sig">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="512" y="464" type="branch" />
            <wire x2="512" y1="464" y2="464" x1="496" />
        </branch>
        <branch name="sw1">
            <wire x2="272" y1="464" y2="464" x1="256" />
        </branch>
        <instance x="320" y="592" name="XLXI_177" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="sw2">
            <wire x2="320" y1="560" y2="560" x1="304" />
        </branch>
        <branch name="sw3">
            <wire x2="320" y1="656" y2="656" x1="304" />
        </branch>
        <branch name="sw3Sig">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="560" y="656" type="branch" />
            <wire x2="560" y1="656" y2="656" x1="544" />
        </branch>
        <branch name="sw2Sig">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="560" y="560" type="branch" />
            <wire x2="560" y1="560" y2="560" x1="544" />
        </branch>
        <instance x="320" y="688" name="XLXI_178" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <iomarker fontsize="28" x="224" y="272" name="pb2" orien="R180" />
        <iomarker fontsize="28" x="240" y="352" name="sw0" orien="R180" />
        <iomarker fontsize="28" x="256" y="464" name="sw1" orien="R180" />
        <iomarker fontsize="28" x="304" y="656" name="sw3" orien="R180" />
        <iomarker fontsize="28" x="304" y="560" name="sw2" orien="R180" />
        <branch name="lvlSig(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1120" y="1040" type="branch" />
            <wire x2="1104" y1="1072" y2="1072" x1="1088" />
            <wire x2="1120" y1="1040" y2="1040" x1="1104" />
            <wire x2="1104" y1="1040" y2="1072" x1="1104" />
        </branch>
        <instance x="640" y="1328" name="XLXI_230" orien="R0" />
        <branch name="_gnd,_gnd,_gnd,_gnd,countOutAux(3:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="1008" type="branch" />
            <wire x2="640" y1="1008" y2="1008" x1="624" />
        </branch>
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="640" y="864" type="branch" />
            <wire x2="640" y1="864" y2="880" x1="640" />
        </branch>
        <branch name="XLXN_601(7:0)">
            <wire x2="640" y1="1136" y2="1136" x1="496" />
        </branch>
        <instance x="352" y="1104" name="XLXI_238" orien="R0">
        </instance>
        <rect width="1196" x="68" y="728" height="612" />
        <text style="fontsize:36;fontname:Arial" x="396" y="760">Current Level Counter</text>
        <instance x="1712" y="1968" name="XLXI_225" orien="R0" />
        <branch name="_gnd,_gnd,_gnd,_gnd,sw3Sig,sw2Sig,sw1Sig,sw0Sig">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="1600" type="branch" />
            <wire x2="1664" y1="1600" y2="1648" x1="1664" />
            <wire x2="1712" y1="1648" y2="1648" x1="1664" />
        </branch>
        <branch name="lvlSig(7:0)">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="1808" type="branch" />
            <wire x2="1664" y1="1808" y2="1840" x1="1664" />
            <wire x2="1712" y1="1840" y2="1840" x1="1664" />
        </branch>
        <rect width="1512" x="60" y="1436" height="552" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="1264" type="branch" />
            <wire x2="640" y1="1264" y2="1264" x1="624" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="480" y="1808" type="branch" />
            <wire x2="496" y1="1808" y2="1808" x1="480" />
        </branch>
        <branch name="incScOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="480" y="1744" type="branch" />
            <wire x2="496" y1="1744" y2="1744" x1="480" />
        </branch>
        <instance x="496" y="1936" name="XLXI_219" orien="R0" />
        <branch name="divTen(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="896" y="1680" type="branch" />
            <wire x2="896" y1="1680" y2="1680" x1="880" />
        </branch>
        <branch name="eqDivTen">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="176" y="1872" type="branch" />
            <wire x2="208" y1="1872" y2="1872" x1="176" />
        </branch>
        <branch name="rscOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="176" y="1936" type="branch" />
            <wire x2="208" y1="1936" y2="1936" x1="176" />
        </branch>
        <instance x="208" y="2000" name="XLXI_229" orien="R0" />
        <branch name="XLXN_572">
            <wire x2="496" y1="1904" y2="1904" x1="464" />
        </branch>
        <instance x="944" y="1792" name="XLXI_224" orien="R0">
        </instance>
        <instance x="1104" y="1952" name="XLXI_223" orien="R0" />
        <branch name="XLXN_560(7:0)">
            <wire x2="1104" y1="1824" y2="1824" x1="1088" />
        </branch>
        <branch name="divTen(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1088" y="1632" type="branch" />
            <wire x2="1104" y1="1632" y2="1632" x1="1088" />
        </branch>
        <branch name="eqDivTen">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="1728" type="branch" />
            <wire x2="1520" y1="1728" y2="1728" x1="1488" />
        </branch>
        <text style="fontsize:40;fontname:Arial" x="216" y="1460">Level Increase Signal Module</text>
        <rect width="460" x="1600" y="772" height="1200" />
        <branch name="greenOut(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4704" y="640" type="branch" />
            <wire x2="4848" y1="640" y2="640" x1="4704" />
        </branch>
        <branch name="greenOut(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4720" y="752" type="branch" />
            <wire x2="4864" y1="752" y2="752" x1="4720" />
        </branch>
        <branch name="greenOut(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4736" y="864" type="branch" />
            <wire x2="4864" y1="864" y2="864" x1="4736" />
        </branch>
        <branch name="blueOut(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4736" y="1008" type="branch" />
            <wire x2="4880" y1="1008" y2="1008" x1="4736" />
        </branch>
        <branch name="blueOut(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4736" y="1120" type="branch" />
            <wire x2="4880" y1="1120" y2="1120" x1="4736" />
        </branch>
        <instance x="4848" y="352" name="XLXI_16" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4848" y="448" name="XLXI_17" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4848" y="560" name="XLXI_18" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4848" y="672" name="XLXI_19" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4864" y="784" name="XLXI_20" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4864" y="896" name="XLXI_21" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4880" y="1040" name="XLXI_22" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4880" y="1152" name="XLXI_23" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="Red0">
            <wire x2="5104" y1="320" y2="320" x1="5072" />
        </branch>
        <branch name="Red1">
            <wire x2="5104" y1="416" y2="416" x1="5072" />
        </branch>
        <branch name="Red2">
            <wire x2="5104" y1="528" y2="528" x1="5072" />
        </branch>
        <branch name="Green0">
            <wire x2="5104" y1="640" y2="640" x1="5072" />
        </branch>
        <branch name="Green1">
            <wire x2="5120" y1="752" y2="752" x1="5088" />
        </branch>
        <branch name="Green2">
            <wire x2="5120" y1="864" y2="864" x1="5088" />
        </branch>
        <branch name="Blue0">
            <wire x2="5136" y1="1008" y2="1008" x1="5104" />
        </branch>
        <branch name="Blue1">
            <wire x2="5136" y1="1120" y2="1120" x1="5104" />
        </branch>
        <branch name="redOut(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4688" y="320" type="branch" />
            <wire x2="4848" y1="320" y2="320" x1="4688" />
        </branch>
        <branch name="redOut(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4672" y="416" type="branch" />
            <wire x2="4848" y1="416" y2="416" x1="4672" />
        </branch>
        <branch name="redOut(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4672" y="528" type="branch" />
            <wire x2="4848" y1="528" y2="528" x1="4672" />
        </branch>
        <iomarker fontsize="28" x="5104" y="320" name="Red0" orien="R0" />
        <iomarker fontsize="28" x="5104" y="416" name="Red1" orien="R0" />
        <iomarker fontsize="28" x="5104" y="528" name="Red2" orien="R0" />
        <iomarker fontsize="28" x="5104" y="640" name="Green0" orien="R0" />
        <iomarker fontsize="28" x="5120" y="752" name="Green1" orien="R0" />
        <iomarker fontsize="28" x="5120" y="864" name="Green2" orien="R0" />
        <iomarker fontsize="28" x="5136" y="1008" name="Blue0" orien="R0" />
        <iomarker fontsize="28" x="5136" y="1120" name="Blue1" orien="R0" />
        <branch name="redOut(2:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4096" y="176" type="branch" />
            <wire x2="4096" y1="176" y2="176" x1="4064" />
        </branch>
        <branch name="greenOut(2:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4112" y="304" type="branch" />
            <wire x2="4112" y1="304" y2="304" x1="4064" />
        </branch>
        <branch name="blueOut(1:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4096" y="432" type="branch" />
            <wire x2="4096" y1="432" y2="432" x1="4064" />
        </branch>
        <branch name="isActive">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3552" y="304" type="branch" />
            <wire x2="3584" y1="304" y2="304" x1="3552" />
        </branch>
        <branch name="_rowCountOut(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3520" y="240" type="branch" />
            <wire x2="3584" y1="240" y2="240" x1="3520" />
        </branch>
        <branch name="_columnCountOut(9:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3536" y="176" type="branch" />
            <wire x2="3584" y1="176" y2="176" x1="3536" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3552" y="624" type="branch" />
            <wire x2="3584" y1="624" y2="624" x1="3552" />
        </branch>
        <branch name="sLROut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3552" y="496" type="branch" />
            <wire x2="3584" y1="496" y2="496" x1="3552" />
        </branch>
        <branch name="sAnimOnOffOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3552" y="816" type="branch" />
            <wire x2="3584" y1="816" y2="816" x1="3552" />
        </branch>
        <branch name="dAnimOnOffOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3552" y="880" type="branch" />
            <wire x2="3584" y1="880" y2="880" x1="3552" />
        </branch>
        <branch name="sFlaAnimOnOffOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3568" y="944" type="branch" />
            <wire x2="3584" y1="944" y2="944" x1="3568" />
        </branch>
        <branch name="collisionHappenedGA">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4080" y="1008" type="branch" />
            <wire x2="4080" y1="1008" y2="1008" x1="4064" />
        </branch>
        <branch name="diamondCountUpGA">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4080" y="1136" type="branch" />
            <wire x2="4080" y1="1136" y2="1136" x1="4064" />
        </branch>
        <branch name="sIncSzOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3552" y="752" type="branch" />
            <wire x2="3584" y1="752" y2="752" x1="3552" />
        </branch>
        <branch name="rscOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3536" y="688" type="branch" />
            <wire x2="3584" y1="688" y2="688" x1="3536" />
        </branch>
        <branch name="sMagOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3568" y="1072" type="branch" />
            <wire x2="3584" y1="1072" y2="1072" x1="3568" />
        </branch>
        <branch name="gameWinOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3568" y="1200" type="branch" />
            <wire x2="3584" y1="1200" y2="1200" x1="3568" />
        </branch>
        <rect width="1196" x="3208" y="12" height="1236" />
        <branch name="diamondCountUpGA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2640" y="1664" type="branch" />
            <wire x2="2672" y1="1664" y2="1664" x1="2640" />
        </branch>
        <branch name="eqDivTen">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2640" y="1744" type="branch" />
            <wire x2="2672" y1="1744" y2="1744" x1="2640" />
        </branch>
        <branch name="pb0SigE">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2640" y="1824" type="branch" />
            <wire x2="2672" y1="1824" y2="1824" x1="2640" />
        </branch>
        <branch name="pb1SigE">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2640" y="1904" type="branch" />
            <wire x2="2672" y1="1904" y2="1904" x1="2640" />
        </branch>
        <branch name="pb2SigE">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2640" y="1984" type="branch" />
            <wire x2="2672" y1="1984" y2="1984" x1="2640" />
        </branch>
        <branch name="terminC">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2640" y="2064" type="branch" />
            <wire x2="2672" y1="2064" y2="2064" x1="2640" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2640" y="2224" type="branch" />
            <wire x2="2672" y1="2224" y2="2224" x1="2640" />
        </branch>
        <branch name="sIncSzOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3280" y="1584" type="branch" />
            <wire x2="3280" y1="1584" y2="1584" x1="3248" />
        </branch>
        <branch name="sAnimOnOffOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3280" y="1648" type="branch" />
            <wire x2="3280" y1="1648" y2="1648" x1="3248" />
        </branch>
        <branch name="sLROut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3280" y="1712" type="branch" />
            <wire x2="3280" y1="1712" y2="1712" x1="3248" />
        </branch>
        <branch name="sFlaAnimOnOffOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3280" y="1776" type="branch" />
            <wire x2="3280" y1="1776" y2="1776" x1="3248" />
        </branch>
        <branch name="dAnimOnOffOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3280" y="1840" type="branch" />
            <wire x2="3280" y1="1840" y2="1840" x1="3248" />
        </branch>
        <branch name="incScOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3280" y="1904" type="branch" />
            <wire x2="3280" y1="1904" y2="1904" x1="3248" />
        </branch>
        <branch name="incLOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3280" y="1968" type="branch" />
            <wire x2="3280" y1="1968" y2="1968" x1="3248" />
        </branch>
        <branch name="resetSecTimer">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3280" y="2032" type="branch" />
            <wire x2="3280" y1="2032" y2="2032" x1="3248" />
        </branch>
        <branch name="sMagOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3280" y="2160" type="branch" />
            <wire x2="3280" y1="2160" y2="2160" x1="3248" />
        </branch>
        <instance x="2672" y="2256" name="XLXI_201" orien="R0">
        </instance>
        <branch name="rscOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3280" y="2288" type="branch" />
            <wire x2="3280" y1="2288" y2="2288" x1="3248" />
        </branch>
        <branch name="collisionHappenedGA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2656" y="1584" type="branch" />
            <wire x2="2672" y1="1584" y2="1584" x1="2656" />
        </branch>
        <branch name="gOvOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3264" y="2224" type="branch" />
            <wire x2="3264" y1="2224" y2="2224" x1="3248" />
        </branch>
        <branch name="gameWinOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3264" y="2096" type="branch" />
            <wire x2="3264" y1="2096" y2="2096" x1="3248" />
        </branch>
        <branch name="XLXN_568">
            <wire x2="2400" y1="1744" y2="1744" x1="2096" />
            <wire x2="2400" y1="1744" y2="2144" x1="2400" />
            <wire x2="2672" y1="2144" y2="2144" x1="2400" />
        </branch>
        <rect width="1236" x="2312" y="1272" height="1088" />
        <text style="fontsize:40;fontname:Arial" x="2728" y="1316">Game State Machine Master</text>
        <instance x="3904" y="2368" name="XLXI_214" orien="R0">
        </instance>
        <branch name="countOutAux(3:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="2464" type="branch" />
            <wire x2="4320" y1="2464" y2="2464" x1="4288" />
        </branch>
        <branch name="incLOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3872" y="2144" type="branch" />
            <wire x2="3904" y1="2144" y2="2144" x1="3872" />
        </branch>
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3840" y="2016" type="branch" />
            <wire x2="3904" y1="2016" y2="2016" x1="3840" />
        </branch>
        <branch name="dig_sel">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3872" y="1888" type="branch" />
            <wire x2="3904" y1="1888" y2="1888" x1="3872" />
        </branch>
        <branch name="incScOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3872" y="1760" type="branch" />
            <wire x2="3904" y1="1760" y2="1760" x1="3872" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3856" y="1632" type="branch" />
            <wire x2="3904" y1="1632" y2="1632" x1="3856" />
        </branch>
        <branch name="an0">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="2336" type="branch" />
            <wire x2="4320" y1="2336" y2="2336" x1="4288" />
        </branch>
        <branch name="an1">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="2272" type="branch" />
            <wire x2="4320" y1="2272" y2="2272" x1="4288" />
        </branch>
        <branch name="an2">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="2208" type="branch" />
            <wire x2="4320" y1="2208" y2="2208" x1="4288" />
        </branch>
        <branch name="an3">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="2144" type="branch" />
            <wire x2="4320" y1="2144" y2="2144" x1="4288" />
        </branch>
        <branch name="XLXN_320">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="2080" type="branch" />
            <wire x2="4320" y1="2080" y2="2080" x1="4288" />
        </branch>
        <branch name="cg">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="2016" type="branch" />
            <wire x2="4320" y1="2016" y2="2016" x1="4288" />
        </branch>
        <branch name="ce">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="1952" type="branch" />
            <wire x2="4320" y1="1952" y2="1952" x1="4288" />
        </branch>
        <branch name="cf">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="1888" type="branch" />
            <wire x2="4320" y1="1888" y2="1888" x1="4288" />
        </branch>
        <branch name="cd">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="1824" type="branch" />
            <wire x2="4320" y1="1824" y2="1824" x1="4288" />
        </branch>
        <branch name="cc">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="1760" type="branch" />
            <wire x2="4320" y1="1760" y2="1760" x1="4288" />
        </branch>
        <branch name="cb">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="1696" type="branch" />
            <wire x2="4320" y1="1696" y2="1696" x1="4288" />
        </branch>
        <branch name="ca">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4320" y="1632" type="branch" />
            <wire x2="4320" y1="1632" y2="1632" x1="4288" />
        </branch>
        <branch name="rscOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3872" y="2272" type="branch" />
            <wire x2="3904" y1="2272" y2="2272" x1="3872" />
        </branch>
        <rect width="948" x="3612" y="1280" height="1268" />
        <text style="fontsize:40;fontname:Arial" x="3752" y="1320">Hex7 Seg and Score Counter Controller</text>
        <rect width="832" x="4512" y="64" height="1140" />
        <text style="fontsize:40;fontname:Arial" x="4864" y="100">RGB Out</text>
        <branch name="an3">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4768" y="2384" type="branch" />
            <wire x2="4960" y1="2384" y2="2384" x1="4768" />
        </branch>
        <branch name="an2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4768" y="2320" type="branch" />
            <wire x2="4928" y1="2320" y2="2320" x1="4768" />
        </branch>
        <branch name="an1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4768" y="2256" type="branch" />
            <wire x2="4928" y1="2256" y2="2256" x1="4768" />
        </branch>
        <branch name="an0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4768" y="2176" type="branch" />
            <wire x2="4944" y1="2176" y2="2176" x1="4768" />
        </branch>
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4768" y="2080" type="branch" />
            <wire x2="4928" y1="2080" y2="2080" x1="4768" />
        </branch>
        <branch name="cg">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4784" y="2016" type="branch" />
            <wire x2="4944" y1="2016" y2="2016" x1="4784" />
        </branch>
        <branch name="ca">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4784" y="1520" type="branch" />
            <wire x2="4896" y1="1520" y2="1520" x1="4784" />
        </branch>
        <instance x="4896" y="1552" name="XLXI_188" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="cb">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4800" y="1616" type="branch" />
            <wire x2="4960" y1="1616" y2="1616" x1="4800" />
        </branch>
        <branch name="cc">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4816" y="1696" type="branch" />
            <wire x2="4944" y1="1696" y2="1696" x1="4816" />
        </branch>
        <branch name="cd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4816" y="1776" type="branch" />
            <wire x2="4976" y1="1776" y2="1776" x1="4816" />
        </branch>
        <branch name="ce">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4784" y="1856" type="branch" />
            <wire x2="4960" y1="1856" y2="1856" x1="4784" />
        </branch>
        <branch name="cf">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4784" y="1936" type="branch" />
            <wire x2="4944" y1="1936" y2="1936" x1="4784" />
        </branch>
        <instance x="4960" y="1648" name="XLXI_190" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4944" y="1728" name="XLXI_191" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4976" y="1808" name="XLXI_192" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="112" y="-20" type="instance" />
        </instance>
        <instance x="4960" y="1888" name="XLXI_193" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4944" y="1968" name="XLXI_194" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4944" y="2048" name="XLXI_195" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="112" y="-20" type="instance" />
        </instance>
        <instance x="4928" y="2112" name="XLXI_196" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4944" y="2208" name="XLXI_197" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4928" y="2288" name="XLXI_198" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <instance x="4928" y="2352" name="XLXI_199" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="112" y="-20" type="instance" />
        </instance>
        <instance x="4960" y="2416" name="XLXI_200" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="112" y="-20" type="instance" />
        </instance>
        <branch name="caOut">
            <wire x2="5152" y1="1520" y2="1520" x1="5120" />
        </branch>
        <branch name="cbOut">
            <wire x2="5216" y1="1616" y2="1616" x1="5184" />
        </branch>
        <branch name="ccOut">
            <wire x2="5200" y1="1696" y2="1696" x1="5168" />
        </branch>
        <branch name="cdOut">
            <wire x2="5232" y1="1776" y2="1776" x1="5200" />
        </branch>
        <branch name="ceOut">
            <wire x2="5216" y1="1856" y2="1856" x1="5184" />
        </branch>
        <branch name="cfOut">
            <wire x2="5200" y1="1936" y2="1936" x1="5168" />
        </branch>
        <branch name="cgOut">
            <wire x2="5200" y1="2016" y2="2016" x1="5168" />
        </branch>
        <branch name="dpout">
            <wire x2="5184" y1="2080" y2="2080" x1="5152" />
        </branch>
        <branch name="an0out">
            <wire x2="5200" y1="2176" y2="2176" x1="5168" />
        </branch>
        <branch name="an1out">
            <wire x2="5184" y1="2256" y2="2256" x1="5152" />
        </branch>
        <branch name="an2out">
            <wire x2="5184" y1="2320" y2="2320" x1="5152" />
        </branch>
        <branch name="an3out">
            <wire x2="5216" y1="2384" y2="2384" x1="5184" />
        </branch>
        <iomarker fontsize="28" x="5152" y="1520" name="caOut" orien="R0" />
        <iomarker fontsize="28" x="5216" y="1616" name="cbOut" orien="R0" />
        <iomarker fontsize="28" x="5200" y="1696" name="ccOut" orien="R0" />
        <iomarker fontsize="28" x="5232" y="1776" name="cdOut" orien="R0" />
        <iomarker fontsize="28" x="5216" y="1856" name="ceOut" orien="R0" />
        <iomarker fontsize="28" x="5200" y="1936" name="cfOut" orien="R0" />
        <iomarker fontsize="28" x="5200" y="2016" name="cgOut" orien="R0" />
        <iomarker fontsize="28" x="5184" y="2080" name="dpout" orien="R0" />
        <iomarker fontsize="28" x="5200" y="2176" name="an0out" orien="R0" />
        <iomarker fontsize="28" x="5184" y="2256" name="an1out" orien="R0" />
        <iomarker fontsize="28" x="5184" y="2320" name="an2out" orien="R0" />
        <iomarker fontsize="28" x="5216" y="2384" name="an3out" orien="R0" />
        <rect width="692" x="4672" y="1272" height="1264" />
        <text style="fontsize:40;fontname:Arial" x="4872" y="1308">Hex 7 Seg Out</text>
        <instance x="992" y="496" name="XLXI_114" orien="R0">
        </instance>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="960" y="288" type="branch" />
            <wire x2="976" y1="288" y2="288" x1="960" />
        </branch>
        <branch name="pb0Sig">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="960" y="224" type="branch" />
            <wire x2="976" y1="224" y2="224" x1="960" />
        </branch>
        <branch name="pb1Sig">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="400" type="branch" />
            <wire x2="992" y1="400" y2="400" x1="976" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="960" y="464" type="branch" />
            <wire x2="992" y1="464" y2="464" x1="960" />
        </branch>
        <instance x="992" y="672" name="XLXI_172" orien="R0">
        </instance>
        <branch name="pb2Sig">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="576" type="branch" />
            <wire x2="992" y1="576" y2="576" x1="976" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="976" y="640" type="branch" />
            <wire x2="992" y1="640" y2="640" x1="976" />
        </branch>
        <branch name="pb2SigE">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="576" type="branch" />
            <wire x2="1392" y1="576" y2="576" x1="1376" />
        </branch>
        <branch name="pb1SigE">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="400" type="branch" />
            <wire x2="1392" y1="400" y2="400" x1="1376" />
        </branch>
        <branch name="pb0SigE">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="224" type="branch" />
            <wire x2="1376" y1="224" y2="224" x1="1360" />
        </branch>
        <instance x="976" y="320" name="XLXI_113" orien="R0">
        </instance>
        <instance x="288" y="240" name="XLXI_79" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="pb1">
            <wire x2="288" y1="208" y2="208" x1="240" />
        </branch>
        <branch name="pb1Sig">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="576" y="208" type="branch" />
            <wire x2="576" y1="208" y2="208" x1="512" />
        </branch>
        <instance x="288" y="160" name="XLXI_112" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="LOC" x="0" y="-64" type="instance" />
        </instance>
        <branch name="pb0Sig">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="560" y="128" type="branch" />
            <wire x2="560" y1="128" y2="128" x1="512" />
        </branch>
        <branch name="pb0">
            <wire x2="288" y1="128" y2="128" x1="256" />
        </branch>
        <iomarker fontsize="28" x="240" y="208" name="pb1" orien="R180" />
        <iomarker fontsize="28" x="256" y="128" name="pb0" orien="R180" />
        <rect width="728" x="36" y="8" height="704" />
        <text style="fontsize:40;fontname:Arial" x="32" y="36">Push Button and Switch Inputs (BASYS 2)</text>
        <rect width="764" x="816" y="36" height="668" />
        <text style="fontsize:40;fontname:Arial" x="884" y="68">Push Button Signal Generation</text>
        <instance x="2112" y="528" name="XLXI_48" orien="R0">
        </instance>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2080" y="496" type="branch" />
            <wire x2="2112" y1="496" y2="496" x1="2080" />
        </branch>
        <branch name="vsOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="432" type="branch" />
            <wire x2="2112" y1="432" y2="432" x1="2064" />
        </branch>
        <rect width="616" x="1968" y="152" height="472" />
        <branch name="frameSigTop">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="2544" y="400" type="branch" />
            <wire x2="2544" y1="432" y2="432" x1="2496" />
            <wire x2="2752" y1="432" y2="432" x1="2544" />
            <wire x2="3584" y1="432" y2="432" x1="2752" />
            <wire x2="2544" y1="400" y2="432" x1="2544" />
        </branch>
        <text style="fontsize:40;fontname:Arial" x="2068" y="180">Frame Signal Generation</text>
        <text style="fontsize:40;fontname:Arial" x="3620" y="44">RGB Master Control</text>
        <instance x="3584" y="592" name="XLXI_239" orien="R0">
        </instance>
        <text style="fontsize:36;fontname:Arial" x="180" y="3060">Ground and Power Signal</text>
        <text style="fontsize:36;fontname:Arial" x="1592" y="816">Levels Match Switches Module</text>
        <text style="fontsize:64;fontname:Arial" x="2048" y="60">Schematic 1 Top</text>
        <branch name="XLXN_16">
            <wire x2="2848" y1="3184" y2="3184" x1="2832" />
            <wire x2="2848" y1="3104" y2="3184" x1="2848" />
            <wire x2="2992" y1="3104" y2="3104" x1="2848" />
            <wire x2="2992" y1="3104" y2="3136" x1="2992" />
            <wire x2="3136" y1="3136" y2="3136" x1="2992" />
        </branch>
        <branch name="XLXN_602">
            <wire x2="3456" y1="3104" y2="3136" x1="3456" />
            <wire x2="3568" y1="3136" y2="3136" x1="3456" />
            <wire x2="3568" y1="3136" y2="3264" x1="3568" />
            <wire x2="3680" y1="3264" y2="3264" x1="3568" />
            <wire x2="3456" y1="3136" y2="3168" x1="3456" />
            <wire x2="3552" y1="3168" y2="3168" x1="3456" />
        </branch>
        <instance x="3136" y="3232" name="XLXI_240" orien="R0" />
        <branch name="XLXN_603">
            <wire x2="2976" y1="2992" y2="2992" x1="2944" />
            <wire x2="2976" y1="2992" y2="3040" x1="2976" />
            <wire x2="3024" y1="3040" y2="3040" x1="2976" />
            <wire x2="3024" y1="3040" y2="3072" x1="3024" />
            <wire x2="3136" y1="3072" y2="3072" x1="3024" />
        </branch>
    </sheet>
</drawing>