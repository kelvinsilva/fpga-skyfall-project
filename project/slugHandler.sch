<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="slugEnable" />
        <signal name="slugFlash" />
        <signal name="slugLeftRight" />
        <signal name="clkin" />
        <signal name="slugSize(15:0)" />
        <signal name="slugVisible" />
        <signal name="slugMagentaFlashSig" />
        <signal name="slugCountOut(9:0)" />
        <signal name="slugResetSize" />
        <signal name="slugIncSize" />
        <signal name="framesig" />
        <signal name="_gnd" />
        <signal name="_vcc" />
        <signal name="slugSzOut(15:0)" />
        <signal name="XLXN_89" />
        <signal name="tcOut" />
        <signal name="slugMagentaColor" />
        <signal name="digselsigIn" />
        <signal name="XLXN_22" />
        <signal name="XLXN_50" />
        <signal name="XLXN_162" />
        <signal name="XLXN_165" />
        <signal name="XLXN_71(15:0)" />
        <signal name="XLXN_72(15:0)" />
        <signal name="XLXN_85" />
        <signal name="XLXN_161" />
        <port polarity="Input" name="slugEnable" />
        <port polarity="Input" name="slugFlash" />
        <port polarity="Input" name="slugLeftRight" />
        <port polarity="Input" name="clkin" />
        <port polarity="Output" name="slugSize(15:0)" />
        <port polarity="Output" name="slugVisible" />
        <port polarity="Output" name="slugMagentaFlashSig" />
        <port polarity="Output" name="slugCountOut(9:0)" />
        <port polarity="Input" name="slugResetSize" />
        <port polarity="Input" name="slugIncSize" />
        <port polarity="Input" name="framesig" />
        <port polarity="Input" name="slugMagentaColor" />
        <port polarity="Input" name="digselsigIn" />
        <blockdef name="edgeDetector">
            <timestamp>2016-5-22T2:16:1</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="ftc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="acc16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="448" y1="-448" y2="-448" x1="512" />
            <rect width="384" x="64" y="-576" height="512" />
            <rect width="64" x="0" y="-460" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="448" y1="-512" y2="-512" x1="512" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="256" />
            <line x2="256" y1="-64" y2="-32" x1="256" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <rect width="64" x="448" y="-524" height="24" />
            <line x2="64" y1="-512" y2="-512" x1="0" />
            <line x2="64" y1="-384" y2="-384" x1="0" />
            <rect width="64" x="0" y="-396" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="448" y1="-384" y2="-384" x1="512" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="constant">
            <timestamp>2006-1-1T10:10:10</timestamp>
            <rect width="112" x="0" y="0" height="64" />
            <line x2="112" y1="32" y2="32" x1="144" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="slugCounter">
            <timestamp>2016-5-23T3:18:23</timestamp>
            <rect width="64" x="0" y="148" height="24" />
            <line x2="0" y1="160" y2="160" x1="64" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="256" x="64" y="-128" height="320" />
        </blockdef>
        <blockdef name="cb4re">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="320" y1="-320" y2="-320" x1="384" />
            <line x2="320" y1="-384" y2="-384" x1="384" />
            <line x2="320" y1="-448" y2="-448" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <rect width="256" x="64" y="-512" height="448" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="nand2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
        </blockdef>
        <blockdef name="and3b1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="40" y1="-64" y2="-64" x1="0" />
            <circle r="12" cx="52" cy="-64" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
        </blockdef>
        <block symbolname="gnd" name="XLXI_21">
            <blockpin signalname="_gnd" name="G" />
        </block>
        <block symbolname="buf" name="XLXI_30">
            <blockpin signalname="slugMagentaColor" name="I" />
            <blockpin signalname="slugMagentaFlashSig" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_39(15:0)">
            <blockpin signalname="slugSzOut(15:0)" name="I" />
            <blockpin signalname="slugSize(15:0)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_45">
            <blockpin signalname="XLXN_89" name="I" />
            <blockpin signalname="slugVisible" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_25">
            <blockpin signalname="_vcc" name="P" />
        </block>
        <block symbolname="slugCounter" name="XLXI_40">
            <blockpin signalname="XLXN_50" name="frameTickIn" />
            <blockpin signalname="clkin" name="clockIn" />
            <blockpin signalname="XLXN_22" name="incOrDec" />
            <blockpin signalname="slugCountOut(9:0)" name="outCount(9:0)" />
            <blockpin signalname="slugSzOut(15:0)" name="slugSizeInput(15:0)" />
        </block>
        <block symbolname="and3b1" name="XLXI_75">
            <blockpin signalname="slugFlash" name="I0" />
            <blockpin signalname="slugEnable" name="I1" />
            <blockpin signalname="framesig" name="I2" />
            <blockpin signalname="XLXN_50" name="O" />
        </block>
        <block symbolname="edgeDetector" name="XLXI_7">
            <blockpin signalname="slugLeftRight" name="_input" />
            <blockpin signalname="clkin" name="clkin" />
            <blockpin signalname="XLXN_22" name="signalOut" />
        </block>
        <block symbolname="ftc" name="XLXI_69">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="XLXN_162" name="CLR" />
            <blockpin signalname="tcOut" name="T" />
            <blockpin signalname="XLXN_165" name="Q" />
        </block>
        <block symbolname="inv" name="XLXI_70">
            <blockpin signalname="slugFlash" name="I" />
            <blockpin signalname="XLXN_162" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_74">
            <blockpin signalname="XLXN_165" name="I0" />
            <blockpin signalname="slugEnable" name="I1" />
            <blockpin signalname="XLXN_89" name="O" />
        </block>
        <block symbolname="acc16" name="XLXI_23">
            <blockpin signalname="_vcc" name="ADD" />
            <blockpin signalname="XLXN_71(15:0)" name="B(15:0)" />
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="XLXN_85" name="CE" />
            <blockpin signalname="_gnd" name="CI" />
            <blockpin signalname="XLXN_72(15:0)" name="D(15:0)" />
            <blockpin signalname="slugResetSize" name="L" />
            <blockpin signalname="_gnd" name="R" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="slugSzOut(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="constant" name="XLXI_27">
            <attr value="40" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_72(15:0)" name="O" />
        </block>
        <block symbolname="constant" name="XLXI_28">
            <attr value="10" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_71(15:0)" name="O" />
        </block>
        <block symbolname="edgeDetector" name="XLXI_29">
            <blockpin signalname="slugIncSize" name="_input" />
            <blockpin signalname="clkin" name="clkin" />
            <blockpin signalname="XLXN_85" name="signalOut" />
        </block>
        <block symbolname="cb4re" name="XLXI_63">
            <blockpin signalname="clkin" name="C" />
            <blockpin signalname="XLXN_161" name="CE" />
            <blockpin name="R" />
            <blockpin name="CEO" />
            <blockpin name="Q0" />
            <blockpin name="Q1" />
            <blockpin name="Q2" />
            <blockpin name="Q3" />
            <blockpin signalname="tcOut" name="TC" />
        </block>
        <block symbolname="and2" name="XLXI_65">
            <blockpin signalname="framesig" name="I0" />
            <blockpin signalname="slugFlash" name="I1" />
            <blockpin signalname="XLXN_161" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="slugSize(15:0)">
            <wire x2="3104" y1="288" y2="288" x1="2608" />
        </branch>
        <branch name="slugCountOut(9:0)">
            <wire x2="3120" y1="384" y2="384" x1="2608" />
        </branch>
        <branch name="slugMagentaFlashSig">
            <wire x2="3152" y1="656" y2="656" x1="3120" />
        </branch>
        <iomarker fontsize="28" x="3104" y="288" name="slugSize(15:0)" orien="R0" />
        <iomarker fontsize="28" x="3120" y="384" name="slugCountOut(9:0)" orien="R0" />
        <iomarker fontsize="28" x="3136" y="496" name="slugVisible" orien="R0" />
        <iomarker fontsize="28" x="3152" y="656" name="slugMagentaFlashSig" orien="R0" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="208" y="1584" type="branch" />
            <wire x2="176" y1="1584" y2="1664" x1="176" />
            <wire x2="208" y1="1584" y2="1584" x1="176" />
        </branch>
        <instance x="112" y="1792" name="XLXI_21" orien="R0" />
        <instance x="2384" y="320" name="XLXI_39(15:0)" orien="R0" />
        <branch name="slugSzOut(15:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2352" y="288" type="branch" />
            <wire x2="2384" y1="288" y2="288" x1="2352" />
        </branch>
        <instance x="2896" y="688" name="XLXI_30" orien="R0" />
        <branch name="slugMagentaColor">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2864" y="656" type="branch" />
            <wire x2="2896" y1="656" y2="656" x1="2864" />
        </branch>
        <branch name="slugVisible">
            <wire x2="3136" y1="496" y2="496" x1="2416" />
        </branch>
        <instance x="2192" y="528" name="XLXI_45" orien="R0" />
        <branch name="XLXN_89">
            <wire x2="2192" y1="496" y2="496" x1="2176" />
            <wire x2="2176" y1="496" y2="592" x1="2176" />
            <wire x2="2768" y1="592" y2="592" x1="2176" />
            <wire x2="2768" y1="592" y2="1248" x1="2768" />
            <wire x2="2768" y1="1248" y2="1248" x1="2704" />
        </branch>
        <branch name="slugEnable">
            <wire x2="464" y1="80" y2="80" x1="304" />
        </branch>
        <branch name="slugFlash">
            <wire x2="448" y1="208" y2="208" x1="304" />
        </branch>
        <branch name="slugLeftRight">
            <wire x2="464" y1="352" y2="352" x1="304" />
        </branch>
        <branch name="clkin">
            <wire x2="448" y1="464" y2="464" x1="304" />
        </branch>
        <branch name="slugResetSize">
            <wire x2="448" y1="688" y2="688" x1="304" />
        </branch>
        <branch name="digselsigIn">
            <wire x2="448" y1="752" y2="752" x1="304" />
        </branch>
        <branch name="slugIncSize">
            <wire x2="448" y1="624" y2="624" x1="288" />
        </branch>
        <branch name="framesig">
            <wire x2="448" y1="560" y2="560" x1="304" />
        </branch>
        <branch name="slugMagentaColor">
            <wire x2="448" y1="832" y2="832" x1="304" />
        </branch>
        <iomarker fontsize="28" x="304" y="80" name="slugEnable" orien="R180" />
        <iomarker fontsize="28" x="304" y="208" name="slugFlash" orien="R180" />
        <iomarker fontsize="28" x="304" y="352" name="slugLeftRight" orien="R180" />
        <iomarker fontsize="28" x="304" y="464" name="clkin" orien="R180" />
        <iomarker fontsize="28" x="304" y="688" name="slugResetSize" orien="R180" />
        <iomarker fontsize="28" x="304" y="752" name="digselsigIn" orien="R180" />
        <iomarker fontsize="28" x="288" y="624" name="slugIncSize" orien="R180" />
        <iomarker fontsize="28" x="304" y="560" name="framesig" orien="R180" />
        <iomarker fontsize="28" x="304" y="832" name="slugMagentaColor" orien="R180" />
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="240" y="1504" type="branch" />
            <wire x2="176" y1="1520" y2="1536" x1="176" />
            <wire x2="240" y1="1536" y2="1536" x1="176" />
            <wire x2="240" y1="1504" y2="1536" x1="240" />
        </branch>
        <instance x="112" y="1520" name="XLXI_25" orien="R0" />
        <branch name="XLXN_22">
            <wire x2="1312" y1="544" y2="544" x1="1152" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1296" y="480" type="branch" />
            <wire x2="1312" y1="480" y2="480" x1="1296" />
        </branch>
        <branch name="slugCountOut(9:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1728" y="416" type="branch" />
            <wire x2="1728" y1="416" y2="416" x1="1696" />
        </branch>
        <branch name="XLXN_50">
            <wire x2="1248" y1="320" y2="320" x1="1184" />
            <wire x2="1248" y1="320" y2="416" x1="1248" />
            <wire x2="1312" y1="416" y2="416" x1="1248" />
        </branch>
        <instance x="1312" y="512" name="XLXI_40" orien="R0">
        </instance>
        <branch name="slugSzOut(15:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1280" y="672" type="branch" />
            <wire x2="1312" y1="672" y2="672" x1="1280" />
        </branch>
        <instance x="928" y="448" name="XLXI_75" orien="R0" />
        <branch name="framesig">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="256" type="branch" />
            <wire x2="928" y1="256" y2="256" x1="912" />
        </branch>
        <branch name="slugEnable">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="320" type="branch" />
            <wire x2="928" y1="320" y2="320" x1="912" />
        </branch>
        <branch name="slugFlash">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="912" y="384" type="branch" />
            <wire x2="928" y1="384" y2="384" x1="912" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="736" y="608" type="branch" />
            <wire x2="768" y1="608" y2="608" x1="736" />
        </branch>
        <branch name="slugLeftRight">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="736" y="544" type="branch" />
            <wire x2="768" y1="544" y2="544" x1="736" />
        </branch>
        <instance x="768" y="640" name="XLXI_7" orien="R0">
        </instance>
        <branch name="slugFlash">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1632" y="1312" type="branch" />
            <wire x2="1728" y1="1312" y2="1312" x1="1632" />
        </branch>
        <branch name="slugEnable">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="2432" y="992" type="branch" />
            <wire x2="2432" y1="992" y2="1216" x1="2432" />
            <wire x2="2448" y1="1216" y2="1216" x1="2432" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1920" y="1216" type="branch" />
            <wire x2="1984" y1="1216" y2="1216" x1="1920" />
        </branch>
        <branch name="tcOut">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1952" y="1088" type="branch" />
            <wire x2="1984" y1="1088" y2="1088" x1="1952" />
        </branch>
        <instance x="1984" y="1344" name="XLXI_69" orien="R0" />
        <branch name="XLXN_162">
            <wire x2="1984" y1="1312" y2="1312" x1="1952" />
        </branch>
        <instance x="1728" y="1344" name="XLXI_70" orien="R0" />
        <branch name="XLXN_165">
            <wire x2="2384" y1="1088" y2="1088" x1="2368" />
            <wire x2="2384" y1="1088" y2="1280" x1="2384" />
            <wire x2="2448" y1="1280" y2="1280" x1="2384" />
        </branch>
        <instance x="2448" y="1344" name="XLXI_74" orien="R0" />
        <instance x="1616" y="2320" name="XLXI_23" orien="R0" />
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1600" y="1808" type="branch" />
            <wire x2="1616" y1="1808" y2="1808" x1="1600" />
        </branch>
        <branch name="XLXN_71(15:0)">
            <wire x2="1616" y1="1872" y2="1872" x1="1408" />
        </branch>
        <branch name="XLXN_72(15:0)">
            <wire x2="1616" y1="1936" y2="1936" x1="1600" />
        </branch>
        <branch name="slugResetSize">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1600" y="2000" type="branch" />
            <wire x2="1616" y1="2000" y2="2000" x1="1600" />
        </branch>
        <branch name="_vcc">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1584" y="2064" type="branch" />
            <wire x2="1616" y1="2064" y2="2064" x1="1584" />
        </branch>
        <instance x="1456" y="1904" name="XLXI_27" orien="R0">
        </instance>
        <instance x="1264" y="1840" name="XLXI_28" orien="R0">
        </instance>
        <branch name="_gnd">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1600" y="2288" type="branch" />
            <wire x2="1616" y1="2288" y2="2288" x1="1600" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1600" y="2192" type="branch" />
            <wire x2="1616" y1="2192" y2="2192" x1="1600" />
        </branch>
        <branch name="XLXN_85">
            <wire x2="1616" y1="2128" y2="2128" x1="1456" />
        </branch>
        <instance x="1072" y="2224" name="XLXI_29" orien="R0">
        </instance>
        <branch name="slugIncSize">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1040" y="2128" type="branch" />
            <wire x2="1072" y1="2128" y2="2128" x1="1040" />
        </branch>
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1040" y="2192" type="branch" />
            <wire x2="1072" y1="2192" y2="2192" x1="1040" />
        </branch>
        <branch name="slugSzOut(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2192" y="1808" type="branch" />
            <wire x2="2192" y1="1808" y2="1808" x1="2128" />
        </branch>
        <rect width="1700" x="844" y="1468" height="948" />
        <text style="fontsize:40;fontname:Arial" x="1520" y="1520">Slug Size Accumulator</text>
        <instance x="1072" y="1408" name="XLXI_63" orien="R0" />
        <branch name="clkin">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1040" y="1280" type="branch" />
            <wire x2="1072" y1="1280" y2="1280" x1="1040" />
        </branch>
        <branch name="tcOut">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1488" y="1280" type="branch" />
            <wire x2="1488" y1="1280" y2="1280" x1="1456" />
        </branch>
        <instance x="800" y="1312" name="XLXI_65" orien="R0" />
        <branch name="slugFlash">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="768" y="1184" type="branch" />
            <wire x2="800" y1="1184" y2="1184" x1="768" />
        </branch>
        <branch name="framesig">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="768" y="1248" type="branch" />
            <wire x2="800" y1="1248" y2="1248" x1="768" />
        </branch>
        <branch name="XLXN_161">
            <wire x2="1072" y1="1216" y2="1216" x1="1056" />
        </branch>
        <rect width="2200" x="624" y="744" height="696" />
        <text style="fontsize:40;fontname:Arial" x="1572" y="792">Slug Flasher Module</text>
        <rect width="1476" x="536" y="68" height="660" />
        <text style="fontsize:40;fontname:Arial" x="1012" y="128">Slug Position Counter Module</text>
        <rect width="344" x="24" y="1280" height="532" />
        <text style="fontsize:40;fontname:Arial" x="60" y="1328">&amp; Ground </text>
        <text style="fontsize:64;fontname:Arial" x="2056" y="48">Schematic 6 Slug Handler</text>
    </sheet>
</drawing>