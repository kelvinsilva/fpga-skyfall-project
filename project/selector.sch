<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="s0" />
        <signal name="s1" />
        <signal name="s2" />
        <signal name="s3" />
        <signal name="x(15:0)" />
        <signal name="s0inv" />
        <signal name="s1inv" />
        <signal name="s2inv" />
        <signal name="s3inv" />
        <signal name="XLXN_142" />
        <signal name="XLXN_143" />
        <signal name="fA" />
        <signal name="XLXN_145" />
        <signal name="XLXN_146" />
        <signal name="fB" />
        <signal name="fB,fB,fB,fB" />
        <signal name="fA,fA,fA,fA" />
        <signal name="x(3:0)" />
        <signal name="x(7:4)" />
        <signal name="ONE_,ONE_,ONE_,ONE_" />
        <signal name="GND_" />
        <signal name="ONE_" />
        <signal name="y(3:0)" />
        <signal name="x(11:8)" />
        <signal name="x(15:12)" />
        <port polarity="Input" name="s0" />
        <port polarity="Input" name="s1" />
        <port polarity="Input" name="s2" />
        <port polarity="Input" name="s3" />
        <port polarity="Input" name="x(15:0)" />
        <port polarity="Output" name="y(3:0)" />
        <blockdef name="m4_1e">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-416" y2="-416" x1="0" />
            <line x2="96" y1="-352" y2="-352" x1="0" />
            <line x2="96" y1="-288" y2="-288" x1="0" />
            <line x2="96" y1="-224" y2="-224" x1="0" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
            <line x2="256" y1="-320" y2="-320" x1="320" />
            <line x2="96" y1="-160" y2="-160" x1="0" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
            <line x2="96" y1="-96" y2="-96" x1="176" />
            <line x2="176" y1="-208" y2="-96" x1="176" />
            <line x2="96" y1="-32" y2="-32" x1="224" />
            <line x2="224" y1="-216" y2="-32" x1="224" />
            <line x2="96" y1="-224" y2="-192" x1="256" />
            <line x2="256" y1="-416" y2="-224" x1="256" />
            <line x2="256" y1="-448" y2="-416" x1="96" />
            <line x2="96" y1="-192" y2="-448" x1="96" />
            <line x2="96" y1="-160" y2="-160" x1="128" />
            <line x2="128" y1="-200" y2="-160" x1="128" />
        </blockdef>
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="m4_1e" name="XLXI_8(3:0)">
            <blockpin signalname="x(3:0)" name="D0" />
            <blockpin signalname="x(7:4)" name="D1" />
            <blockpin signalname="x(11:8)" name="D2" />
            <blockpin signalname="x(15:12)" name="D3" />
            <blockpin signalname="ONE_,ONE_,ONE_,ONE_" name="E" />
            <blockpin signalname="fB,fB,fB,fB" name="S0" />
            <blockpin signalname="fA,fA,fA,fA" name="S1" />
            <blockpin signalname="y(3:0)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_11">
            <blockpin signalname="XLXN_143" name="I0" />
            <blockpin signalname="XLXN_142" name="I1" />
            <blockpin signalname="fA" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_9">
            <blockpin signalname="s3inv" name="I0" />
            <blockpin signalname="s2inv" name="I1" />
            <blockpin signalname="s1" name="I2" />
            <blockpin signalname="s0inv" name="I3" />
            <blockpin signalname="XLXN_142" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_10">
            <blockpin signalname="s3inv" name="I0" />
            <blockpin signalname="s2inv" name="I1" />
            <blockpin signalname="s1inv" name="I2" />
            <blockpin signalname="s0" name="I3" />
            <blockpin signalname="XLXN_143" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_17">
            <blockpin signalname="s0" name="I" />
            <blockpin signalname="s0inv" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_18">
            <blockpin signalname="s1" name="I" />
            <blockpin signalname="s1inv" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_19">
            <blockpin signalname="s2" name="I" />
            <blockpin signalname="s2inv" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_20">
            <blockpin signalname="s3" name="I" />
            <blockpin signalname="s3inv" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_12">
            <blockpin signalname="XLXN_146" name="I0" />
            <blockpin signalname="XLXN_145" name="I1" />
            <blockpin signalname="fB" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_13">
            <blockpin signalname="s3inv" name="I0" />
            <blockpin signalname="s2" name="I1" />
            <blockpin signalname="s1inv" name="I2" />
            <blockpin signalname="s0inv" name="I3" />
            <blockpin signalname="XLXN_145" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_14">
            <blockpin signalname="s3inv" name="I0" />
            <blockpin signalname="s2inv" name="I1" />
            <blockpin signalname="s1inv" name="I2" />
            <blockpin signalname="s0" name="I3" />
            <blockpin signalname="XLXN_146" name="O" />
        </block>
        <block symbolname="gnd" name="XLXI_24">
            <blockpin signalname="GND_" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_25">
            <blockpin signalname="ONE_" name="P" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="s0">
            <wire x2="352" y1="128" y2="128" x1="160" />
        </branch>
        <branch name="s1">
            <wire x2="352" y1="224" y2="224" x1="160" />
        </branch>
        <branch name="s2">
            <wire x2="352" y1="304" y2="304" x1="160" />
        </branch>
        <branch name="s3">
            <wire x2="352" y1="384" y2="384" x1="160" />
        </branch>
        <iomarker fontsize="28" x="160" y="128" name="s0" orien="R180" />
        <iomarker fontsize="28" x="160" y="224" name="s1" orien="R180" />
        <iomarker fontsize="28" x="160" y="304" name="s2" orien="R180" />
        <iomarker fontsize="28" x="160" y="384" name="s3" orien="R180" />
        <instance x="640" y="1312" name="XLXI_8(3:0)" orien="R0" />
        <instance x="1680" y="432" name="XLXI_11" orien="R0" />
        <branch name="s0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="608" y="144" type="branch" />
            <wire x2="816" y1="144" y2="144" x1="608" />
        </branch>
        <instance x="1392" y="368" name="XLXI_9" orien="R0" />
        <instance x="1392" y="640" name="XLXI_10" orien="R0" />
        <branch name="s0inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="112" type="branch" />
            <wire x2="1392" y1="112" y2="112" x1="1376" />
        </branch>
        <branch name="s1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="176" type="branch" />
            <wire x2="1392" y1="176" y2="176" x1="1376" />
        </branch>
        <branch name="s2inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="240" type="branch" />
            <wire x2="1392" y1="240" y2="240" x1="1376" />
        </branch>
        <branch name="s3inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="304" type="branch" />
            <wire x2="1392" y1="304" y2="304" x1="1376" />
        </branch>
        <branch name="s0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="384" type="branch" />
            <wire x2="1392" y1="384" y2="384" x1="1376" />
        </branch>
        <branch name="s1inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="448" type="branch" />
            <wire x2="1392" y1="448" y2="448" x1="1376" />
        </branch>
        <branch name="s2inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="512" type="branch" />
            <wire x2="1392" y1="512" y2="512" x1="1376" />
        </branch>
        <branch name="s3inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1376" y="576" type="branch" />
            <wire x2="1392" y1="576" y2="576" x1="1376" />
        </branch>
        <instance x="816" y="176" name="XLXI_17" orien="R0" />
        <branch name="s1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="608" y="224" type="branch" />
            <wire x2="816" y1="224" y2="224" x1="608" />
        </branch>
        <instance x="816" y="256" name="XLXI_18" orien="R0" />
        <instance x="816" y="352" name="XLXI_19" orien="R0" />
        <branch name="s2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="320" type="branch" />
            <wire x2="816" y1="320" y2="320" x1="624" />
        </branch>
        <branch name="s3">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="448" type="branch" />
            <wire x2="832" y1="448" y2="448" x1="624" />
        </branch>
        <instance x="832" y="480" name="XLXI_20" orien="R0" />
        <branch name="s0inv">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1056" y="144" type="branch" />
            <wire x2="1056" y1="144" y2="144" x1="1040" />
        </branch>
        <branch name="s1inv">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1056" y="224" type="branch" />
            <wire x2="1056" y1="224" y2="224" x1="1040" />
        </branch>
        <branch name="s2inv">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="320" type="branch" />
            <wire x2="1072" y1="320" y2="320" x1="1040" />
        </branch>
        <branch name="s3inv">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1088" y="448" type="branch" />
            <wire x2="1088" y1="448" y2="448" x1="1056" />
        </branch>
        <rect width="612" x="532" y="60" height="564" />
        <instance x="2880" y="432" name="XLXI_12" orien="R0" />
        <instance x="2528" y="320" name="XLXI_13" orien="R0" />
        <instance x="2528" y="608" name="XLXI_14" orien="R0" />
        <branch name="s0inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2512" y="64" type="branch" />
            <wire x2="2528" y1="64" y2="64" x1="2512" />
        </branch>
        <branch name="s1inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2512" y="128" type="branch" />
            <wire x2="2528" y1="128" y2="128" x1="2512" />
        </branch>
        <branch name="s2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2512" y="192" type="branch" />
            <wire x2="2528" y1="192" y2="192" x1="2512" />
        </branch>
        <branch name="s3inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2512" y="256" type="branch" />
            <wire x2="2528" y1="256" y2="256" x1="2512" />
        </branch>
        <branch name="s0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2512" y="352" type="branch" />
            <wire x2="2528" y1="352" y2="352" x1="2512" />
        </branch>
        <branch name="s1inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2512" y="416" type="branch" />
            <wire x2="2528" y1="416" y2="416" x1="2512" />
        </branch>
        <branch name="s2inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2512" y="480" type="branch" />
            <wire x2="2528" y1="480" y2="480" x1="2512" />
        </branch>
        <branch name="s3inv">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2512" y="544" type="branch" />
            <wire x2="2528" y1="544" y2="544" x1="2512" />
        </branch>
        <branch name="XLXN_142">
            <wire x2="1664" y1="208" y2="208" x1="1648" />
            <wire x2="1664" y1="208" y2="304" x1="1664" />
            <wire x2="1680" y1="304" y2="304" x1="1664" />
        </branch>
        <branch name="XLXN_143">
            <wire x2="1664" y1="480" y2="480" x1="1648" />
            <wire x2="1664" y1="368" y2="480" x1="1664" />
            <wire x2="1680" y1="368" y2="368" x1="1664" />
        </branch>
        <branch name="fA">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1968" y="336" type="branch" />
            <wire x2="1968" y1="336" y2="336" x1="1936" />
        </branch>
        <branch name="XLXN_145">
            <wire x2="2832" y1="160" y2="160" x1="2784" />
            <wire x2="2832" y1="160" y2="304" x1="2832" />
            <wire x2="2880" y1="304" y2="304" x1="2832" />
        </branch>
        <branch name="XLXN_146">
            <wire x2="2832" y1="448" y2="448" x1="2784" />
            <wire x2="2832" y1="368" y2="448" x1="2832" />
            <wire x2="2880" y1="368" y2="368" x1="2832" />
        </branch>
        <branch name="fB">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3152" y="336" type="branch" />
            <wire x2="3152" y1="336" y2="336" x1="3136" />
        </branch>
        <rect width="772" x="1244" y="48" height="604" />
        <rect width="904" x="2316" y="36" height="608" />
        <branch name="fB,fB,fB,fB">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="1152" type="branch" />
            <wire x2="640" y1="1152" y2="1152" x1="624" />
        </branch>
        <branch name="fA,fA,fA,fA">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="1216" type="branch" />
            <wire x2="640" y1="1216" y2="1216" x1="624" />
        </branch>
        <branch name="x(15:12)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="1088" type="branch" />
            <wire x2="640" y1="1088" y2="1088" x1="624" />
        </branch>
        <branch name="x(11:8)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="1024" type="branch" />
            <wire x2="640" y1="1024" y2="1024" x1="624" />
        </branch>
        <branch name="x(7:4)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="960" type="branch" />
            <wire x2="640" y1="960" y2="960" x1="624" />
        </branch>
        <branch name="x(3:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="896" type="branch" />
            <wire x2="640" y1="896" y2="896" x1="624" />
        </branch>
        <branch name="ONE_,ONE_,ONE_,ONE_">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="1280" type="branch" />
            <wire x2="640" y1="1280" y2="1280" x1="624" />
        </branch>
        <branch name="x(15:0)">
            <wire x2="416" y1="448" y2="448" x1="160" />
        </branch>
        <iomarker fontsize="28" x="160" y="448" name="x(15:0)" orien="R180" />
        <rect width="452" x="8" y="88" height="648" />
        <branch name="GND_">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="416" y="592" type="branch" />
            <wire x2="416" y1="592" y2="592" x1="288" />
        </branch>
        <instance x="224" y="720" name="XLXI_24" orien="R0" />
        <instance x="64" y="592" name="XLXI_25" orien="R0" />
        <branch name="ONE_">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="128" y="672" type="branch" />
            <wire x2="128" y1="592" y2="672" x1="128" />
        </branch>
        <branch name="y(3:0)">
            <wire x2="1008" y1="992" y2="992" x1="960" />
        </branch>
        <iomarker fontsize="28" x="1008" y="992" name="y(3:0)" orien="R0" />
        <text style="fontsize:28;fontname:Arial" x="1640" y="112">encoder</text>
        <text style="fontsize:28;fontname:Arial" x="2832" y="80">encoder</text>
    </sheet>
</drawing>