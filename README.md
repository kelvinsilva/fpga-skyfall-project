# README #

Role: Developer

Description: Implement a simple video game using FPGA Spartan 3E and Basys2 board.

Design and Implement VGA Controller, build custom counters, and all logic circuitry required for functioning video game from the ground up

Technology: FPGA, Xilinx ISE Suite, Combinational circuit design, Sequential circuit design


### What is this repository for? ###

FPGA Simple videogame. VGA Controller custom programmed, use with a 640 monitor.


### How do I get set up? ###

Use Xilinx ISE Suite to synthesize, and Adept to load the file onto FPGA (Spartan 3E and Basys 2 Board). Connect to a 640 vga monitor.



### Contribution guidelines ###

Kelvin Silva, CMPE 100, University of California Santa Cruz, Spring 2016


### Who do I talk to? ###
Kelvin Silva, kelvinsilva747@gmail.com